package atrums.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.BDDOpenbravo.BDDOadUserEmail;
import atrums.modelo.BDDOpenbravo.BDDOcTax;
import atrums.modelo.BDDOpenbravo.BDDOcTaxcategory;
import atrums.modelo.BDDOpenbravo.BDDOmInoutLine;
import atrums.modelo.BDDOpenbravo.BDDOmProduct;
import atrums.modelo.BDDOpenbravo.BDDOmProductCategory;
import atrums.modelo.SRI.SRIDocumentoAutorizado;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServEmail;
import atrums.modelo.Servicio.ServRespuestaControl;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacion;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;
import atrums.persistencia.ServiceInsertarFEControlDocumentos;

public class MonitorearGuiasRemisionMigracion implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearGuiasRemisionMigracion.class);
	private File docFile = null;
	private String correoCliente = null;
	private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearGuiasRemisionMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpenbravo = null;
		String adicional = "";
		this.docFile = null;
		this.correoCliente = null;
		
		try{
			ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
			ConfService confService = new ConfService();
			
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			conHome = home.getConneccion(this.dataSourceOpenbravo);
			
			if(conHome != null && this.variablesServicio.isExtDocumentoGiaMig()){
				if(home.getBDDDocumentoBaseMigrar("GR" ,conHome)){
					documentoBase = home.getDocumentoBase();
					
					/**Codigo para evitar demaciado demanda de recurso**/
					
					if(this.variablesServicio.getDocumentGiaMig().equals(documentoBase.getNrodocumento())){
						int auxConteo = this.variablesServicio.getRepeticionGiaMig();
						auxConteo = auxConteo + 1;
						this.variablesServicio.setRepeticionGiaMig(auxConteo);
						
						if(auxConteo == 5){
							this.variablesServicio.setContinuarGiaMig(false);
							this.variablesServicio.setRepeticionGiaMig(0);
						}
					}else{
						this.variablesServicio.setDocumentGiaMig(documentoBase.getNrodocumento());
						this.variablesServicio.setRepeticionGiaMig(0);
					}
					
					/***/
					
					log.info("Migrando documento guia de remisión: " + documentoBase.getNrodocumento());
					home.saveLogServicio(conHome, "Migrando documento guia de remisión al openbravo: " + documentoBase.getNrodocumento());
					
					BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
					OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
							empresaPrincipal, 
							confService);
					conOpenbravo = openbravo.getConneccion(dataSourceOpenbravo);
					openbravo.setDocumentoBase(documentoBase);
					
					if(openbravo.getEmpresaPrincipal(conOpenbravo)){
						openbravo.getEmpresaPrincipal().setAmbiente("1");
						if(openbravo.existeDocumentoImporFCNC(conOpenbravo)){
							if(openbravo.getBDDOadClient(conOpenbravo)){
								if(openbravo.getBDDOadOrg(conOpenbravo)){
									if(openbravo.getBDDOcDoctype(conOpenbravo)){
										ServiceFacturaNotaRetenPorDocumento documento = new ServiceFacturaNotaRetenPorDocumento(confService, 
												documentoBase.getNrodocumento(), 
												documentoBase.getNroestablecimiento(), 
												documentoBase.getNroemision(), 
												documentoBase.getTipodocumento());
										documento.setFecha(documentoBase.getFechadocumento());
										
										List<ServDocumentoLinea> lineas = documento.callDocumentos();
										
										if(lineas.size() > 0){
											boolean continuar = true;
											
											if(openbravo.existeDato("SELECT count(*) AS total FROM c_bpartner as cb WHERE cb.taxid = '" + lineas.get(0).getDestinatario() + "';", conOpenbravo)){
												ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getDestinatario());
												ServCliente cliente = serviceCliente.callCliente();
												if(cliente.getIdentificacion() != null){
													
													if(cliente.getEmail().getEmail() == null){
														ServEmail auxEmail = new ServEmail();
														auxEmail.setIdentificacion(cliente.getIdentificacion());
														auxEmail.setEmail("N/A");
														cliente.setEmail(auxEmail);
														adicional = ", no hay correo electronico";
													}else{
														this.correoCliente = cliente.getEmail().getEmail();
														adicional = ", correo electronico: " + this.correoCliente + " ";
													}
													
													if(cliente.getDireccion().getDireccion() == null){
														ServDireccion auxDireccion = new ServDireccion();
														auxDireccion.setIdentificacion(cliente.getIdentificacion());
														auxDireccion.setDireccion("N/A");
														auxDireccion.setDireccion2("N/A");
														cliente.setDireccion(auxDireccion);
													}
													
													openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
													openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
													openbravo.getOcbPartner().setTipo(cliente.getTipo());
													openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
													openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
													
													
													if(openbravo.existeDato("SELECT count(*) AS total FROM c_bp_group as cb WHERE upper(cb.name) LIKE '%CLIENTES COMERCIALES%';", conOpenbravo)){
														if(!openbravo.saveBDDOcbGroup(conOpenbravo)){
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}
													
													if(openbravo.saveBDDOcbPartner(conOpenbravo) && continuar){
														openbravo.getOadUserEmail().setEmail(cliente.getEmail().getEmail());
														if(openbravo.saveBDDOadUser(conOpenbravo)){
															openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
															if(openbravo.saveBDDOcLocation(conOpenbravo)){
																if(openbravo.saveBDDOcbPartnerLocation(conOpenbravo)){
																	openbravo.setOadUserEmail(new BDDOadUserEmail());
																	if(openbravo.saveBDDOadUserPortal(conOpenbravo)){
																		if(openbravo.existeDato("SELECT count(*) AS total FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%'", conOpenbravo)){
																			if(openbravo.saveBDDOadRole(conOpenbravo)){
																				if(openbravo.saveBDDOadRoleOrgaccess(conOpenbravo)){
																					if(!openbravo.saveBDDOadWindowAccess(conOpenbravo)){
																						conOpenbravo.rollback(); log.info("Rollback Proceso");
																						continuar = false;
																					}
																				}else{
																					conOpenbravo.rollback(); log.info("Rollback Proceso");
																					continuar = false;
																				}
																			}else{
																				conOpenbravo.rollback(); log.info("Rollback Proceso");
																				continuar = false;
																			}
																		}
																		
																		if(!openbravo.saveBDDOadUserRoles(conOpenbravo)){
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																		
																	}else{
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}else{
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	continuar = false;
																}
															}else{
																conOpenbravo.rollback(); log.info("Rollback Proceso");
																continuar = false;
															}
														}else{
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}else{
														conOpenbravo.rollback(); log.info("Rollback Proceso");
														continuar = false;
													}
												}
											}else{
												openbravo.getOcbPartner().setIdentificacion(lineas.get(0).getDestinatario());
												
												ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getDestinatario());
												ServCliente cliente = serviceCliente.callCliente();
												if(cliente.getIdentificacion() != null){
													
													if(cliente.getEmail().getEmail() == null){
														ServEmail auxEmail = new ServEmail();
														auxEmail.setIdentificacion(cliente.getIdentificacion());
														auxEmail.setEmail("N/A");
														cliente.setEmail(auxEmail);
														adicional = ", no hay correo electronico";
													}else{
														this.correoCliente = cliente.getEmail().getEmail();
														adicional = ", correo electronico: " + this.correoCliente + " ";
													}
													
													if(cliente.getDireccion().getDireccion() == null){
														ServDireccion auxDireccion = new ServDireccion();
														auxDireccion.setIdentificacion(cliente.getIdentificacion());
														auxDireccion.setDireccion("N/A");
														auxDireccion.setDireccion2("N/A");
														cliente.setDireccion(auxDireccion);
													}
													
													openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
													openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
													openbravo.getOcbPartner().setTipo(cliente.getTipo());
													openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
													openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
												}
												
												openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
												openbravo.setOadUserEmail(new BDDOadUserEmail());
												
												openbravo.getOcbPartner().setIdentificacion(lineas.get(0).getDestinatario());
												if(openbravo.getBDDOcbPartner(conOpenbravo)){
													if(!openbravo.getBDDOadUser(conOpenbravo)){
														conOpenbravo.rollback(); log.info("Rollback Proceso");
														continuar = false;
													}else{
														if(cliente.getIdentificacion() != null){
															
															if(cliente.getEmail().getEmail() == null){
																ServEmail auxEmail = new ServEmail();
																auxEmail.setIdentificacion(cliente.getIdentificacion());
																auxEmail.setEmail("N/A");
																cliente.setEmail(auxEmail);
																adicional = ", no hay correo electronico";
															}else{
																this.correoCliente = cliente.getEmail().getEmail();
																adicional = ", correo electronico: " + this.correoCliente + " ";
															}
														}
													}
												}else{
													conOpenbravo.rollback(); log.info("Rollback Proceso");
													continuar = false;
												}
											}
											
											if(openbravo.existeDato("SELECT count(*) AS total FROM c_bpartner as cb WHERE cb.taxid = '" + lineas.get(0).getTransportita() + "';", conOpenbravo)){
												openbravo.getOcbTransp().setNombreComercial(lineas.get(0).getRazonSocialTransp());
												openbravo.getOcbTransp().setRazonSocial(lineas.get(0).getRazonSocialTransp());
												
												if(lineas.get(0).getTransportita().equals("9999999999999")){
													openbravo.getOcbTransp().setTipo("07");
												}else if(lineas.get(0).getTransportita().length() == 13 && lineas.get(0).getTransportita().substring(10, lineas.get(0).getTransportita().length()).equals("001")){
													openbravo.getOcbTransp().setTipo("04");
												}else if(lineas.get(0).getTransportita().length() == 10 && auxiliares.isNumeric(lineas.get(0).getTransportita())){
													openbravo.getOcbTransp().setTipo("05");
												}else{
													openbravo.getOcbTransp().setTipo("06");
												}
												
												openbravo.getOcbTransp().setIdentificacion(lineas.get(0).getTransportita());
												openbravo.getOcbTransp().setEmail("N/D");
												
												if(!openbravo.saveBDDOcbPartnerTrans(conOpenbravo) && continuar){
													conOpenbravo.rollback(); log.info("Rollback Proceso");
													continuar = false;
												}
											}else{
												openbravo.getOcbTransp().setIdentificacion(lineas.get(0).getTransportita());
												if(!openbravo.getBDDOcbPartnerTrans(conOpenbravo)){
													conOpenbravo.rollback(); log.info("Rollback Proceso");
													continuar = false;
												}
											}
											
											if(openbravo.getOcbPartner().getId() != null && openbravo.getOcbTransp().getId() != null && continuar){
												openbravo.getOcLocationPar().setDireccion(lineas.get(0).getDirPartida());
												openbravo.getOcLocationDes().setDireccion(lineas.get(0).getDirDestinatario());
												
												if(!openbravo.saveBDDOcLocationPar(conOpenbravo)){
													conOpenbravo.rollback(); log.info("Rollback Proceso");
													continuar = false;
												}
												
												if(!openbravo.saveBDDOcLocationDes(conOpenbravo)){
													conOpenbravo.rollback(); log.info("Rollback Proceso");
													continuar = false;
												}
												
												if(openbravo.getOcLocationPar().getId() != null && 
														openbravo.getOcLocationDes().getId() != null && 
														continuar){
													
													openbravo.getOmInout().setFechaEmision(documentoBase.getFechadocumento());
													openbravo.getOmInout().setNroDocumento(documentoBase.getNrodocumento());
													openbravo.getOmInout().setClaveAcceso(documentoBase.getClaveacceso());
													openbravo.getOmInout().setFechaAutorizacion(documentoBase.getFechaautorizacion());
													openbravo.getOmInout().setNumeroAutorizacion(documentoBase.getNroautorizacion());
													openbravo.getOmInout().setEstablecimiento(documentoBase.getNroestablecimiento());
													openbravo.getOmInout().setEmision(documentoBase.getNroemision());
													openbravo.getOmInout().setFechaIni(lineas.get(0).getFechaIniTransp());
													openbravo.getOmInout().setFechaFin(lineas.get(0).getFechaFinTransp());
													openbravo.getOmInout().setPlaca(lineas.get(0).getPlaca());
													openbravo.getOmInout().setMotivo(lineas.get(0).getMotivoTras());
													
													try{
														SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
														ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
																empresaPrincipal.getAmbiente(), 
																documentoBase.getClaveacceso());
														autorizado = serviceAutorizacion.CallAutorizado();
														
														if(autorizado.getDocXML() != null){
															openbravo.getOmInout().setGuia(autorizado.getDocXML());
															this.docFile = autorizado.getDocFile();
														}
													} catch (Exception ex){}
													
													if(openbravo.saveBDDOmInout(conOpenbravo) && continuar){
														int linea = 10;
														for(int i=0; i<lineas.size(); i++){
															if(continuar){
																openbravo.setOmProductCategory(new BDDOmProductCategory());
																if(openbravo.existeDato("SELECT count(*) AS total FROM m_product_category AS mp WHERE upper(mp.name) LIKE '%" + openbravo.getOmProductCategory().getName().toUpperCase() + "%'", conOpenbravo)){
																	if(!openbravo.saveBDDmProductCategory(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}
																
																openbravo.setOcTaxcategory(new BDDOcTaxcategory());
																openbravo.getOcTaxcategory().setCodigo("2");
																openbravo.getOcTaxcategory().setNombre("IVA 12");
																
																if(openbravo.existeDato("SELECT count(*) AS total FROM c_taxcategory as ct WHERE upper(ct.name) like '" + openbravo.getOcTaxcategory().getNombre() + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOcTaxcategory(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}
																
																openbravo.setOcTax(new BDDOcTax());
																openbravo.getOcTax().setRate("12");
																
																if(openbravo.existeDato("SELECT count(*) AS total FROM c_tax WHERE upper(name) like '" + openbravo.getOcTaxcategory().getNombre() + "' AND rate = '" + openbravo.getOcTax().getRate() + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOcTax(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}
																
																openbravo.setOmProduct(new BDDOmProduct());
																String codProducto = ""; 
																
																if(lineas.get(i).getProducto().replace(" ", "").toLowerCase().length() > 25){
																	codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto().substring(0, 25));
																}else{
																	codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto());
																}
																
																openbravo.getOmProduct().setCodigo(codProducto);
																openbravo.getOmProduct().setName(lineas.get(i).getProducto().replace("'", "").replace(",", ""));
																if(openbravo.existeDato("SELECT count(*) AS total FROM m_product AS mp WHERE mp.value = '" + openbravo.getOmProduct().getCodigo() + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOmProduct(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}else if(!openbravo.updateBDDOmProduct(conOpenbravo)){
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	continuar = false;
																}
																
																openbravo.setOmInoutLine(new BDDOmInoutLine());
																openbravo.getOmInoutLine().setLinea(String.valueOf(linea));
																openbravo.getOmInoutLine().setCantidad(lineas.get(i).getCantidad());
																openbravo.getOmInoutLine().setDescripcion(lineas.get(i).getDescripcion());
																
																if(!openbravo.saveBDDOmInoutLine(conOpenbravo)){
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	continuar = false;
																}else{
																	linea = linea + 10;
																}
															}
														}

														if(continuar){
															ServiceInsertarFEControlDocumentos controlDocumentos = new ServiceInsertarFEControlDocumentos(
																	confService, 
																	documentoBase.getNrodocumento(), 
																	documentoBase.getNroestablecimiento(), 
																	documentoBase.getNroemision(), 
																	documentoBase.getNroautorizacion(), 
																	documentoBase.getFechaautorizacion(), 
																	documentoBase.getFechadocumento(), 
																	"2", 
																	openbravo.getOcbPartner().getIdentificacion(), 
																	openbravo.getOcbPartner().getIdentificacion(), 
																	(documentoBase.getTipodocumento().equals("GR") ? "GR" : "GR"));
															
															ServRespuestaControl respuestaControl = controlDocumentos.callRespuesta();
															
															if(respuestaControl.isRespuesta()){
																conOpenbravo.commit();
																log.info("Datos migrados");
																
																documentoBase.setEstado("MIG");
																documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes" + adicional);
																documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																if(home.actualizarEstado(conHome)){
																	conHome.commit();
																}else{
																	conHome.rollback(); log.info("Rollback Proceso");
																}
																
																if(openbravo.getOcbPartner().getIdentificacion().indexOf("9999999999") == -1){
																	try{
																		if(confService.isEnviarCorreo() && this.correoCliente != null){
																			EnviarEmail email = new EnviarEmail(conOpenbravo, openbravo, confService, this.docFile);
																			
																			documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																			documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																			
																			if(email.enviar(this.correoCliente, 
																					documentoBase.getClaveacceso(), 
																					openbravo.getOcInvoice().getId(), 
																					documentoBase.getTipodocumento(),
																					documentoBase)){
																				adicional = adicional + ", Correo enviado al: " + this.correoCliente;
																			}else{
																				adicional = adicional + ", Correo no enviado al: " + this.correoCliente;
																			}
																		}else{
																			if(!confService.isEnviarCorreo()){
																				adicional = adicional + ", no esta activo la opción de enviar email";
																			}else if(this.correoCliente == null){
																				adicional = adicional + ", no hay correo para enviar email";
																			}
																		}
																	}catch (Exception ex){}
																}
																
																documentoBase.setEstado("MIG");
																documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes" + adicional);
																documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																if(home.actualizarEstado(conHome)){
																	home.saveLogServicio(conHome, "Documento guia de despacho migrado al openbravo: " + documentoBase.getNrodocumento() + " - " + adicional);
																	conHome.commit();
																}else{
																	conHome.rollback(); log.info("Rollback Proceso");
																}
															}else{
																conOpenbravo.rollback(); log.info("Rollback Proceso");
																log.info("Datos no migrados ver log");
																home.saveLogServicio(conHome, "Documento guia de despacho no migrado al openbravo: " + documentoBase.getNrodocumento());
															}
														}else{
															if(home.actualizarEstado(conHome)){
																home.saveLogServicio(conHome, "Documento guia de despacho no migrado al openbravo: " + adicional);
																conHome.commit();
															}else{
																conHome.rollback(); log.info("Rollback Proceso");
															}
														}
													}else{
														if(home.actualizarEstado(conHome)){
															conHome.commit();
														}else{
															conHome.rollback(); log.info("Rollback Proceso");
														}
														conOpenbravo.rollback(); log.info("Rollback Proceso");
														continuar = false;
													}
												}else{
													documentoBase.setMensaje("ERROR / No hay lineas para el documento: " + documentoBase.getNrodocumento());
													home.setDocumentoBase(documentoBase);
													if(home.actualizarEstado(conHome)){
														conHome.commit();
													}else{
														conHome.rollback(); log.info("Rollback Proceso");
													}
												}
											}
										}else{
											documentoBase.setMensaje("ERROR / No hay lineas para el documento: " + documentoBase.getNrodocumento());
											home.setDocumentoBase(documentoBase);
											if(home.actualizarEstado(conHome)){
												conHome.commit();
											}else{
												conHome.rollback(); log.info("Rollback Proceso");
											}
										}
									}else{
										log.info("Esperando documento...");
										documentoBase.setMensaje("ERROR / No hay un documento guia para la sucursal con establecimiento: " + documentoBase.getNroestablecimiento() + ", emision: " + documentoBase.getNroemision());
										home.setDocumentoBase(documentoBase);
										if(home.actualizarEstado(conHome)){
											conHome.commit();
										}else{
											conHome.rollback(); log.info("Rollback Proceso");
										}
									}
								}else{
									log.info("Esperando sucursal...");
									documentoBase.setMensaje("ERROR / No hay una sucursal para este documento, establecimiento: " + documentoBase.getNroestablecimiento() + ", emision: " + documentoBase.getNroemision());
									home.setDocumentoBase(documentoBase);
									if(home.actualizarEstado(conHome)){
										conHome.commit();
									}else{
										conHome.rollback(); log.info("Rollback Proceso");
									}
								}
							}else{
								log.info("Esperando empresa...");
								documentoBase.setMensaje("ERROR / No hay una empresa para este documento");
								try { if (conOpenbravo != null) conOpenbravo.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
								try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
								this.variablesServicio.setExtDocumentoGiaMig(false);
							}
						}else{
							documentoBase.setEstado("MIG");
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								conHome.commit();
							}else{
								conHome.rollback(); log.info("Rollback Proceso");
							}
						}
					}
				}else{
					log.info("Esperando documento...");
					try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoGiaMig(false);
				}
			}else{
				log.info("Esperando conexión openbravo...");
				this.variablesServicio.setExtDocumentoGiaMig(false);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpenbravo != null) conOpenbravo.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
			try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
		}
	}
}
