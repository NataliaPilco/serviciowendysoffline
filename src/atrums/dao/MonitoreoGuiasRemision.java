package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.servicios.ServicioMonitorearGuiasRemisionMigracion;
import atrums.dao.servicios.ServicioMonitorearGuiasRemisionSRI;
import atrums.dao.servicios.ServicioMonitorearGuiasRemisionWeb;
import atrums.modelo.VariablesServicio;

public class MonitoreoGuiasRemision implements Runnable{
	static final Logger log = Logger.getLogger(MonitoreoGuiasRemision.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;

	public MonitoreoGuiasRemision(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Iniciando Servicio de monitoreo Guias de Remisión");
		
		try {Thread.sleep(30);} catch (InterruptedException e) {}
		ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
        Runnable runnableWeb = new ServicioMonitorearGuiasRemisionWeb(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceWeb.execute(runnableWeb);
		
        try {Thread.sleep(60033);} catch (InterruptedException e) {}
		ExecutorService exServiceSRI = Executors.newFixedThreadPool(1);
		Runnable runnableServiceSRI = new ServicioMonitorearGuiasRemisionSRI(
				this.dataSourceOpenbravo, 
				this.variablesServicio);
		exServiceSRI.execute(runnableServiceSRI);
		
		try {Thread.sleep(60036);} catch (InterruptedException e) {}
		ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
		Runnable runnableMigracion = new ServicioMonitorearGuiasRemisionMigracion(
				this.dataSourceOpenbravo, 
				this.variablesServicio);
		exServiceMigr.execute(runnableMigracion);
		
		exServiceMigr.shutdown();
		exServiceSRI.shutdown();
		exServiceWeb.shutdown();
		
		while (!exServiceMigr.isTerminated()){}
		while (!exServiceSRI.isTerminated()){}
		while (!exServiceWeb.isTerminated()){}
	}
}
