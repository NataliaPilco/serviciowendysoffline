package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.servicios.ServicioMonitorearFacturasMigracion;
import atrums.dao.servicios.ServicioMonitorearFacturasSRI;
import atrums.dao.servicios.ServicioMonitorearFacturasWeb;
import atrums.modelo.VariablesServicio;

public class MonitorearFacturas implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearFacturas.class);
	//private DataSource dataSourceOpenbravo;
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearFacturas(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Iniciando Servicio de monitoreo Facturas");
		
		try {Thread.sleep(3);} catch (InterruptedException e) {}
		ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
        Runnable runnableWeb = new ServicioMonitorearFacturasWeb(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceWeb.execute(runnableWeb);
		
        try {Thread.sleep(60006);} catch (InterruptedException e) {}
        ExecutorService exServiceSRI = Executors.newFixedThreadPool(1);
        Runnable runnableServiceSRI = new ServicioMonitorearFacturasSRI(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceSRI.execute(runnableServiceSRI);
        
        try {Thread.sleep(60009);} catch (InterruptedException e) {}
        ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
        Runnable runnableMigracion = new ServicioMonitorearFacturasMigracion(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceMigr.execute(runnableMigracion);
        
        exServiceMigr.shutdown();
        exServiceSRI.shutdown();
        exServiceWeb.shutdown();
	}	
}