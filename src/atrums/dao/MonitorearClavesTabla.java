package atrums.dao;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceClaveFecha;
import atrums.persistencia.ServiceClavePorFechaPagina;
import atrums.persistencia.ServiceNroPaginasClaves;

public class MonitorearClavesTabla implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearClavesTabla.class);
    private ConfService confService;
    private DataSource dataSourceOpenbravo;
    
    public MonitorearClavesTabla(DataSource dataSourceOpenbravo){
        this.confService = new ConfService();
        this.dataSourceOpenbravo = dataSourceOpenbravo;
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Monitoreando Claves de Facturas");
		
        OperacionesBDDHome home = new OperacionesBDDHome(null);
        
        try{
    		ServiceClaveFecha serviceFecha = new ServiceClaveFecha(confService);
            List<String> auxFechas = serviceFecha.callFechas();
            serviceFecha = null;
            
            Collections.sort(auxFechas);
            Collections.reverse(auxFechas);
            
            List<String> auxFechas2 = new ArrayList<String>(); 
            
            for(int k=0;k<4;k++){
            	auxFechas2.add(auxFechas.get(k));
            }
            
            auxFechas = auxFechas2;
            
            for(String fecha: auxFechas){
            	ServiceNroPaginasClaves serviceNroPaginasClaves = new ServiceNroPaginasClaves(confService, fecha);
            	int numeroPaginas = serviceNroPaginasClaves.callDocumentos();
            	log.info("Monitoreando Fecha Clave: " + fecha);
            	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
            	
            	for(int i=1; i <= numeroPaginas; i++){
            		ServiceClavePorFechaPagina serviceClavePorFechaPagina = new ServiceClavePorFechaPagina(confService, fecha, String.valueOf(i));
            		List<String> auxClaves = serviceClavePorFechaPagina.callClaves();
            		
            		Connection connection = null;
            		
            		try{
            			connection = home.getConneccion(this.dataSourceOpenbravo);
                		
                		if(connection != null){
                			
                			if(!connection.isClosed()){
                				home.saveLogServicio(connection, "Monitoreando Claves de Acceso");
                			}else{
                				log.warn("La conexion esta cerrada");
                			}
                			
                			/*String clavesBuscar = null;
                			
                			for(String clave: auxClaves){
                				if(clavesBuscar == null){
                					clavesBuscar = clave;
                				}else{
                					clavesBuscar = clavesBuscar + "','" + clave;
                				}
                			}
                			
                			if(!connection.isClosed()){
                				home.clavesFechas(auxClaves, clavesBuscar, connection);
                			}else{
                				auxClaves.clear();
                			}*/
                			
                			log.info("Nro de claves: " + auxClaves.size());
                			
                			for(String clave: auxClaves){
                				DateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH);
                    			Date fechaDoc = format.parse(clave.substring(0,8));
                    			SimpleDateFormat sdfFormato = new SimpleDateFormat("yyyy-MM-dd");
                            	
                            	BDDDocumentoBase documentoBase = new BDDDocumentoBase(
                            			clave.substring(30,39), 
                            			clave.substring(24, 27), 
                            			clave.substring(27, 30),
                            			(clave.substring(8,10).equals("01") ? "FCP":"") + 
                            			(clave.substring(8,10).equals("04") ? "NCP":"") + 
                            			(clave.substring(8,10).equals("07") ? "RTP":""), 
                            			sdfFormato.format(fechaDoc));
                            	home.setDocumentoBase(documentoBase);
                            	
                            	if(home.guardarDocumento(connection)){
                        			documentoBase.setClaveacceso(clave);
                        			documentoBase.setEstado("AUT");
                        			home.setDocumentoBase(documentoBase);
                        			if(home.actualizarEstado(connection)){
                        				connection.commit();
                        			}else{
                        				connection.rollback(); log.info("Rollback Proceso");
                        			}
                        		}else{
                        			connection.rollback(); log.info("Rollback Proceso");
                        		}
                            	
                            	documentoBase = null;
                    		}
                			
                			auxClaves.clear();
                			auxClaves = null;
                			try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
                		}
        			} catch (Exception ex) {
        				// TODO Auto-generated catch block
        				log.error(ex.getMessage());
        			} finally {
        				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
        			}
            	}
            }
            
            auxFechas.clear();
            auxFechas = null;
            auxFechas2.clear(); 
            auxFechas2 = null;
        } catch (Exception ex){
        	log.error(ex.getMessage(), ex);
		}
        
        home = null;
	}
}