package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearNotasCreditoFechas;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearNotasCreditoWeb implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearNotasCreditoWeb.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;

	public ServicioMonitorearNotasCreditoWeb(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Monitoreando documentos notas de credito web service");
				while(this.variablesServicio.isEstadoNotas()){
					if(this.variablesServicio.isSemaforoUno()){
	        			this.variablesServicio.setSemaforoUno(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
	            		
	            		ExecutorService executor = Executors.newFixedThreadPool(1);
	    		    	Runnable runnable = new MonitorearNotasCreditoFechas(this.dataSourceOpenbravo);
	    		    	Future<?> future = executor.submit(runnable);
	            		
						try{
					        future.get(20, TimeUnit.MINUTES); 
						}catch(Exception ex){
							future.cancel(true);
							log.error(ex.getMessage(), ex);
						}
						
						executor.shutdownNow();
				        
						this.variablesServicio.setSemaforoUno(true);
	    				log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
						
	    				runnable = null;
	    				executor = null;
	    				
	    				try {log.info("Esperando semaforo...");Thread.sleep(600000);} catch (InterruptedException e) {}
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoUno());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30030);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoNotas(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
