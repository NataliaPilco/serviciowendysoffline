package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearRetencionesSRI;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearRetencionesSRI implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearRetencionesSRI.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearRetencionesSRI(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Generando documentos XML retencion");
				while(this.variablesServicio.isEstadoRetenciones()){
					if(this.variablesServicio.isSemaforoDos_A()){
	        			this.variablesServicio.setSemaforoDos_A(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
	            		
	            		if(this.variablesServicio.isContinuarRetSRI()){
	            			ExecutorService exService = Executors.newFixedThreadPool(1);
	    			        Runnable runnableService = new MonitorearRetencionesSRI(
	    			        		this.dataSourceOpenbravo, 
	    			        		this.variablesServicio);
	    			        Future<?> future = exService.submit(runnableService);
							
							try{
						        future.get(10, TimeUnit.MINUTES); 
							}catch(Exception ex){
								future.cancel(true);
								log.error(ex.getMessage(), ex);
							}
							
							exService.shutdownNow();
							
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
							
							runnableService = null;
							exService = null;
						}else{
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
		    				
							this.variablesServicio.setContinuarRetSRI(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
				        
	            		if(!this.variablesServicio.isExtDocumentoRetSRI()){
				        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        	this.variablesServicio.setExtDocumentoRetSRI(true);
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_A());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30070);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoRetenciones(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
