package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearGuiasRemisionMigracion;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearGuiasRemisionMigracion implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearGuiasRemisionMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearGuiasRemisionMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Migrando documentos guia openbravo");
				while(this.variablesServicio.isEstadoGuiasRemision()){
					if(this.variablesServicio.isSemaforoDos_M()){
	        			this.variablesServicio.setSemaforoDos_M(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
	            		
	            		if(this.variablesServicio.isContinuarGiaMig()){
	            			ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
							Runnable runnableMigracion = new MonitorearGuiasRemisionMigracion(
									this.dataSourceOpenbravo, 
									this.variablesServicio);
							Future<?> future = exServiceMigr.submit(runnableMigracion);
					        
							try{
						        future.get(10, TimeUnit.MINUTES); 
							}catch(Exception ex){
								future.cancel(true);
								log.error(ex.getMessage(), ex);
							}
							
							exServiceMigr.shutdownNow();
					        
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
							
		    				runnableMigracion = null;
		    				exServiceMigr = null;
	            		}else{
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
		    				
							this.variablesServicio.setContinuarGiaMig(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
	            		
	            		if(!this.variablesServicio.isExtDocumentoGiaMig()){
	            			this.variablesServicio.setExtDocumentoGiaMig(true);
				        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_M());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30110);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoGuiasRemision(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
