package atrums.dao.servicios;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearFacturasMigracion;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearFacturasMigracion implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearFacturasMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearFacturasMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Migrando documentos factura openbravo");
				while(this.variablesServicio.isEstadoFacturas()){
					if(this.variablesServicio.isSemaforoDos_M()){
	        			this.variablesServicio.setSemaforoDos_M(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
	            		
	            		if(this.variablesServicio.isContinuarFacMig()){
	            			ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
					        Runnable runnableMigracion = new MonitorearFacturasMigracion(
					        		this.dataSourceOpenbravo, 
					        		this.variablesServicio);
					        Future<?> future = exServiceMigr.submit(runnableMigracion);
					        
							try{
						        future.get(5, TimeUnit.MINUTES); 
							}catch (TimeoutException ex) {
								boolean c = future.cancel(true);
								log.info("Timeout " + c);
								log.error(ex.getMessage(), ex);
					        }catch(InterruptedException ex){
								log.error(ex.getMessage(), ex);
							}catch(ExecutionException ex){
								log.error(ex.getMessage(), ex);
							}
							
							exServiceMigr.shutdownNow();
					        
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
							
		    				runnableMigracion = null;
		    				exServiceMigr = null;
	            		}else{
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
		    				
							this.variablesServicio.setContinuarFacMig(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
	            		
	            		if(!this.variablesServicio.isExtDocumentoFacMig()){
				        	this.variablesServicio.setExtDocumentoFacMig(true);
				        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_M());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30020);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoFacturas(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
