package atrums.dao.servicios;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearFacturasFechas;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearFacturasWeb implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearFacturasWeb.class);
	private DataSource dataSourceOpenbravo;

	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearFacturasWeb(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Monitoreando documentos factura web service");
				while(this.variablesServicio.isEstadoFacturas()){
					if(this.variablesServicio.isSemaforoUno()){
	        			this.variablesServicio.setSemaforoUno(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
	            		
	            		ExecutorService executor = Executors.newFixedThreadPool(1);
	    		    	Runnable runnable = new MonitorearFacturasFechas(this.dataSourceOpenbravo);
	    		    	Future<?> future = executor.submit(runnable);
	    		    	
						try{
					        future.get(30, TimeUnit.MINUTES); 
						}catch (TimeoutException ex) {
							boolean c = future.cancel(true);
							log.info("Timeout " + c);
							log.error(ex.getMessage(), ex);
				        }catch(InterruptedException ex){
							log.error(ex.getMessage(), ex);
						}catch(ExecutionException ex){
							log.error(ex.getMessage(), ex);
						}
						
						executor.shutdownNow();
				        
						this.variablesServicio.setSemaforoUno(true);
	    				log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
						
	    				runnable = null;
	    				executor = null;
	    				
	    				try {log.info("Esperando semaforo...");Thread.sleep(600000);} catch (InterruptedException e) {}
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoUno());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30000);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoFacturas(false);
				log.warn(ex.getMessage());
			}
	}
}
