package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.SRI.SRIDocumentoAutorizado;
import atrums.modelo.SRI.SRIDocumentoRecibido;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacion;
import atrums.persistencia.ServiceRecibido;

public class MonitorearFacturasSRI implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearFacturasSRI.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearFacturasSRI(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpen = null;
		
		try {
			ConfService confService = new ConfService();
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			conHome = home.getConneccion(dataSourceOpenbravo);
			
			if(conHome != null && this.variablesServicio.isExtDocumentoFacSRI()){
				if(home.getBDDDocumentoBaseProcesar("FC','FV" ,conHome)){
					documentoBase = home.getDocumentoBase();
					
					/**Codigo para evitar demaciado demanda de recurso**/
					
					if(this.variablesServicio.getDocumentFacSRI().equals(documentoBase.getNrodocumento())){
						int auxConteo = this.variablesServicio.getRepeticionFacSRI();
						auxConteo = auxConteo + 1;
						this.variablesServicio.setRepeticionFacSRI(auxConteo);
						
						if(auxConteo == 5){
							this.variablesServicio.setContinuarFacSRI(false);
							this.variablesServicio.setRepeticionFacSRI(0);
						}
					}else{
						this.variablesServicio.setDocumentFacSRI(documentoBase.getNrodocumento());
						this.variablesServicio.setRepeticionFacSRI(0);
					}
					
					/***/
					
					log.info("Procesando Documento: " + documentoBase.getNrodocumento());
					home.saveLogServicio(conHome, "Procesando documento factura: " + documentoBase.getNrodocumento());
					if(documentoBase.getEstado().equals("N") || 
							documentoBase.getEstado().equals("DEV") || 
							documentoBase.getEstado().equals("NOA") || 
							documentoBase.getEstado().equals("NOR")){
						
						ExecutorService exGenerar = Executors.newCachedThreadPool();
						GenerarDocumentosFactura documento = new GenerarDocumentosFactura(documentoBase, dataSourceOpenbravo);
						Future<?> future = exGenerar.submit(documento);
						
						try{
					        future.get(5, TimeUnit.MINUTES); 
						}catch(Exception ex){
							future.cancel(true);
							log.error(ex.getMessage(), ex);
						}
						exGenerar.shutdownNow();
						
						if(documento.getFileString() != null){
							log.info("Autorizando documento factura: " + documentoBase.getNrodocumento());
							home.saveLogServicio(conHome, "Autorizando documento factura: " + documentoBase.getNrodocumento());
							
							SRIDocumentoAutorizado autorizadoPre = new SRIDocumentoAutorizado();
							SRIDocumentoRecibido recibido = new SRIDocumentoRecibido();
							
							ServiceAutorizacion serviceAutorizacionPre = new ServiceAutorizacion(confService, 
									documento.getEmpresaPrincipal().getAmbiente(), 
									documento.getClaveAcceso());
							autorizadoPre = serviceAutorizacionPre.CallAutorizado();
							
							if(!autorizadoPre.getEstadoespecifico().equals("AUT")){
								ServiceRecibido serviceRecibido = new ServiceRecibido(confService, 
										documento.getEmpresaPrincipal().getAmbiente(), documento.getFileString());
								recibido = serviceRecibido.CallRecibido();
							}else{
								recibido.setEstado("RECIBIDO");
								recibido.setEstadoespecifico("REC");
							}
							
							if(recibido.getEstadoespecifico() != null){
								
								if(recibido.getMensaje() != null){
									if(recibido.getMensaje().indexOf("CLAVE ACCESO REGISTRADA") != -1){
										recibido.setEstadoespecifico("REC");
										recibido.setMensaje("");
									}
								}
								
								documentoBase.setEstado(recibido.getEstadoespecifico());
								documentoBase.setClaveacceso(documento.getClaveAcceso());
								documentoBase.setMensaje(recibido.getMensaje());
								if(recibido.getInformacion() != null){
									documentoBase.setMensaje(recibido.getMensaje() + recibido.getInformacion().replace("'", ""));
								}
							}
							
							home.setDocumentoBase(documentoBase);
							
							if(home.actualizarEstado(conHome)){
								conHome.commit();
								if(recibido.getEstadoespecifico().equals("REC")){
									SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
									ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
											documento.getEmpresaPrincipal().getAmbiente(), 
											documentoBase.getClaveacceso());
									autorizado = serviceAutorizacion.CallAutorizado();
									
									if(autorizado.getEstadoespecifico() != null){
										documentoBase.setEstado(autorizado.getEstadoespecifico());
										documentoBase.setFechaautorizacion(autorizado.getFechaAutorizacion());
										documentoBase.setNroautorizacion(autorizado.getNumeroAutorizacion());
										
										if(autorizado.getInformacion() != null){
											documentoBase.setMensaje(autorizado.getMensaje() + autorizado.getInformacion().replace("'", ""));
										}else{
											documentoBase.setMensaje(autorizado.getMensaje());
										}
									}
									
									home.setDocumentoBase(documentoBase);
									if(home.actualizarEstado(conHome)){
										log.info("Documento factura autorizado: " + documentoBase.getNrodocumento());
										home.saveLogServicio(conHome, "Documento factura autorizado: " + documentoBase.getNrodocumento());
										conHome.commit();
									}else{
										conHome.rollback(); log.info("Rollback Proceso");
									}
								}else{
									conHome.rollback(); log.info("Rollback Proceso");
								}
							}else{
								conHome.rollback(); log.info("Rollback Proceso");
							}
						}else{
							documentoBase.setEstado("DEV");
							documentoBase.setMensaje(documento.getMensaje());
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								home.saveLogServicio(conHome, "Documento factura no procesado: " + documentoBase.getNrodocumento() + " - " + documento.getMensaje());
								conHome.commit();
							}else{
								conHome.rollback(); log.info("Rollback Proceso");
							}
						}
					}else if(documentoBase.getEstado().equals("REC")){
						SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
						ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
						BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
						OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
								empresaPrincipal, 
								confService);
						conOpen = openbravo.getConneccion(dataSourceOpenbravo);
						
						if(openbravo.getEmpresaPrincipal(conOpen)){
							ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
									openbravo.getEmpresaPrincipal().getAmbiente(), 
									documentoBase.getClaveacceso());
							autorizado = serviceAutorizacion.CallAutorizado();
							
							if(autorizado.getEstadoespecifico() != null){
								documentoBase.setEstado(autorizado.getEstadoespecifico());
								documentoBase.setFechaautorizacion(autorizado.getFechaAutorizacion());
								documentoBase.setNroautorizacion(autorizado.getNumeroAutorizacion());
								documentoBase.setMensaje(autorizado.getMensaje());
							}
							
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								conHome.commit();
							}else{
								conHome.rollback(); log.info("Rollback Proceso");
							}
						}
					}else{
						log.info("Esperando documentos del SRI...");
						try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); conHome = null;} catch (Exception ex) {};
						this.variablesServicio.setExtDocumentoFacSRI(false);
					}
				}else{
					log.info("Esperando documentos...");
					try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); conHome = null;} catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoFacSRI(false);
				}
			}else{
				log.info("Esperando conexión...");
				this.variablesServicio.setExtDocumentoFacSRI(false);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpen != null) conOpen.close();  log.info("Close BDD o Statement"); conOpen = null;} catch (Exception ex) {};
			try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); conHome = null;} catch (Exception ex) {};
		}
	}
}
