package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.servicios.ServicioMonitorearClavesFacturasMigracion;
import atrums.dao.servicios.ServicioMonitorearClavesFacturasWeb;
import atrums.dao.servicios.ServicioMonitorearClavesNotaCreditoMigracion;
import atrums.modelo.VariablesServicio;

public class MonitorearClaves implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearClaves.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearClaves(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {Thread.sleep(39);} catch (InterruptedException e) {}
		ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
        Runnable runnableWeb = new ServicioMonitorearClavesFacturasWeb(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceWeb.execute(runnableWeb);
		
        try {Thread.sleep(60042);} catch (InterruptedException e) {}
		ExecutorService exServiceMigrF = Executors.newFixedThreadPool(1);
        Runnable runnableMigracionF = new ServicioMonitorearClavesFacturasMigracion(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceMigrF.execute(runnableMigracionF);
		
        try {Thread.sleep(60045);} catch (InterruptedException e) {}
        ExecutorService exServiceMigrN = Executors.newFixedThreadPool(1);
        Runnable runnableMigracionN = new ServicioMonitorearClavesNotaCreditoMigracion(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceMigrN.execute(runnableMigracionN);
		
        exServiceMigrN.shutdown();
        exServiceMigrF.shutdown();
        exServiceWeb.shutdown();
        
        while (!exServiceMigrN.isTerminated()){}
        while (!exServiceMigrF.isTerminated()){}
        while (!exServiceWeb.isTerminated()){}
	}
}
