package atrums.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.Servicio.ServDetalleRentecion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;
import atrums.persistencia.ServiceRetencionDetallesPorFecha;

public class GenerarDocumentosRetencion implements Runnable{
	static final Logger log = Logger.getLogger(GenerarDocumentosRetencion.class);
	private ConfService confService;
    private ConfBDDOpenbravo confBDDOpenbravo;
    private BDDDocumentoBase documentoBase;
    private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
    private BDDEmpresaPrincipal empresaPrincipal;
    private String fileString = null;
    private String claveAcceso = null;
    private String mensaje = null;
    private DecimalFormat format2 = new DecimalFormat("0.00");
    private DataSource dataSourceOpenbravo;
    private Double porcentajeRetencion;
	
    public GenerarDocumentosRetencion(BDDDocumentoBase documentoBase, DataSource dataSourceOpenbravo) {
    	this.confService = new ConfService();
    	this.confBDDOpenbravo = new ConfBDDOpenbravo();
    	this.documentoBase = documentoBase;
    	this.dataSourceOpenbravo = dataSourceOpenbravo;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conOpen = null;
		
		try {
			log.info("Procesando documento: " + documentoBase.getNrodocumento());
			ServiceFacturaNotaRetenPorDocumento documento = new ServiceFacturaNotaRetenPorDocumento(confService, 
					documentoBase.getNrodocumento(), 
					documentoBase.getNroestablecimiento(), 
					documentoBase.getNroemision(), 
					documentoBase.getTipodocumento());
			documento.setFecha(documentoBase.getFechadocumento());
			
			log.info("Consultando Lineas del documento: " + documentoBase.getNrodocumento());
			List<ServDocumentoLinea> lineas = documento.callDocumentos();
			
			log.info("Consultando Empresa para el documento: " + documentoBase.getNrodocumento());
			this.empresaPrincipal = new BDDEmpresaPrincipal();
			OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(this.confBDDOpenbravo, this.empresaPrincipal, this.confService);
			
			log.info("Conectanto a la bdd para procesar documento: " + documentoBase.getNrodocumento());
			conOpen = openbravo.getConneccion(dataSourceOpenbravo);
			
			log.info("Extrayendo datos de la bdd para el documento: " + documentoBase.getNrodocumento());
			if(openbravo.getEmpresaPrincipal(conOpen)){
				empresaPrincipal = openbravo.getEmpresaPrincipal();
				if(lineas.size() > 0){
					try {
						File file = File.createTempFile("documento", ".xml", null);
						file.deleteOnExit();
						
						Document document = DocumentHelper.createDocument();
						OutputFormat outputFormat = OutputFormat.createPrettyPrint();
						
						String tipoComprobante = "";
						
						Element elmret = document.addElement("comprobanteRetencion");
					    elmret.addAttribute("id", "comprobante");
					    elmret.addAttribute("version", "1.0.0");
					    tipoComprobante = "07";
					    
					    final Element elminftri = elmret.addElement("infoTributaria");
					    
					    elminftri.addElement("ambiente").addText(empresaPrincipal.getAmbiente());
						elminftri.addElement("tipoEmision").addText(empresaPrincipal.getTipoEmision());
						elminftri.addElement("razonSocial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getRazonSocial()));
						elminftri.addElement("nombreComercial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getNombreComercial()));
						elminftri.addElement("ruc").addText(empresaPrincipal.getRuc());
					    
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
						Date fechaDoc = format.parse(documentoBase.getFechadocumento());
						//Date fechaDoc = format.parse("2016-06-01");
						SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
						SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
						SimpleDateFormat sdfFormatoPerido = new SimpleDateFormat("MM/yyyy");
						
						String secuencial = documentoBase.getNrodocumento();
						for (int i = 0; i < (9 - documentoBase.getNrodocumento().length()); i++) {
							secuencial = "0" + secuencial;
						}
						
						String codNumerico = empresaPrincipal.getCodNumerico();
						
						if(empresaPrincipal.getCodNumerico() != null){
							for (int i = 0; i < (8 - empresaPrincipal.getCodNumerico().length()); i++) {
								codNumerico = "0" + codNumerico;
							}
						}
						
						documentoBase.getNrodocumento();
						documentoBase.getNroestablecimiento();
						documentoBase.getNroemision();
						
						String numDoc = lineas.get(0).getNroComprobanteVenta();
						String numEst = "";
						String numEmi = "";
						
						if(numDoc.length() <= 17){
				            numEst = numDoc.substring(0, 3);
				            numEmi = numDoc.substring(numDoc.indexOf("-") + 1, numDoc.indexOf("-") + 4);
				            numDoc = numDoc.substring(numDoc.indexOf("-", numDoc.indexOf("-") + 1) + 1, numDoc.length());
				            String auxSecuencial = numDoc;
				            
				            for (int i = 0; i < (9 - numDoc.length()); i++) {
				            	auxSecuencial = "0" + auxSecuencial;
				            }
				            
				            numDoc = auxSecuencial;
				        }
						
						if (!numDoc.equals(documentoBase.getNrodocumento())) {
							numDoc = documentoBase.getNrodocumento();
							numEst = documentoBase.getNroestablecimiento();
							numEmi = documentoBase.getNroemision();
						}
						
						String claveAcceso = this.auxiliares.generarclaveacceso(
								sdfFormatoClave.format(fechaDoc), 
								tipoComprobante, 
								empresaPrincipal.getRuc(), 
								empresaPrincipal.getAmbiente(), 
								numEst + numEmi, 
								numDoc, 
								codNumerico, 
								empresaPrincipal.getTipoEmision());
						
						elminftri.addElement("claveAcceso").addText(claveAcceso);
						elminftri.addElement("codDoc").addText(tipoComprobante);
						elminftri.addElement("estab").addText(numEst);
						elminftri.addElement("ptoEmi").addText(numEmi);
						elminftri.addElement("secuencial").addText(numDoc);
						elminftri.addElement("dirMatriz").addText(empresaPrincipal.getDireccion());
						
						Element elmcomret = elmret.addElement("infoCompRetencion");
						elmcomret.addElement("fechaEmision").addText(sdfFormato.format(fechaDoc));
						elmcomret.addElement("dirEstablecimiento").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getDireccion()));
						
						if(empresaPrincipal.getNumResolucion() != null){
							String numResolucion = empresaPrincipal.getNumResolucion();
							
							for (int i = 0; i < (3 - empresaPrincipal.getNumResolucion().length()); i++) {
								numResolucion = "0" + numResolucion;
							}
							
							elmcomret.addElement("contribuyenteEspecial").addText(numResolucion);
						}
						
						if(empresaPrincipal.isObliContabi()){
							elmcomret.addElement("obligadoContabilidad").addText("SI");
						}else{
							elmcomret.addElement("obligadoContabilidad").addText("NO");
						}
						
						if(lineas.get(0).getRucProveedor().equals("9999999999999")){
							elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("07");
						}else if(lineas.get(0).getRucProveedor().length() == 13 && lineas.get(0).getRucProveedor().substring(10, lineas.get(0).getRucProveedor().length()).equals("001")){
							elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("04");
						}else if(lineas.get(0).getRucProveedor().length() == 10 && auxiliares.isNumeric(lineas.get(0).getRucProveedor())){
							elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("05");
						}else{
							elmcomret.addElement("tipoIdentificacionSujetoRetenido").addText("06");
						}
						
						if(lineas.get(0).getRucProveedor().equals("9999999999999")){
							elmcomret.addElement("razonSocialSujetoRetenido").addText("CONSUMIDOR FINAL");
						}else{
							elmcomret.addElement("razonSocialSujetoRetenido").addText(lineas.get(0).getProveedor());
						}
						
						elmcomret.addElement("identificacionSujetoRetenido").addText(lineas.get(0).getRucProveedor());
						
						elmcomret.addElement("periodoFiscal").addText(sdfFormatoPerido.format(fechaDoc));
						
						Element elmimps = elmret.addElement("impuestos");
						
						ServiceRetencionDetallesPorFecha retencionDetallesPorFecha = new ServiceRetencionDetallesPorFecha(confService, 
								lineas.get(0).getSucursal(), 
								documentoBase.getNrodocumento(), 
								documentoBase.getFechadocumento()
								);
						
						List<ServDetalleRentecion> lineasRetencion = retencionDetallesPorFecha.callDocumentos();
						
						if(lineasRetencion.size() > 0){
							for(ServDetalleRentecion detalleRentecion: lineasRetencion){
								Element elmtolimp = elmimps.addElement("impuesto");
								elmtolimp.addElement("codigo").addText((detalleRentecion.getTipo().equals("V") ? "2" : "") + 
										(detalleRentecion.getTipo().equals("N") ? "1" : ""));
								
								String codigoRetencion;
								
								if(detalleRentecion.getCodigoRetencion().indexOf(".") != -1 || 
										detalleRentecion.getCodigoRetencion().indexOf(",") != -1){
									codigoRetencion = String.valueOf(Double.valueOf(detalleRentecion.getCodigoRetencion()).intValue());
								}else{
									codigoRetencion = detalleRentecion.getCodigoRetencion();
								}
								
								if(detalleRentecion.getTipo().equals("V")){
									elmtolimp.addElement("codigoRetencion").addText(codigoRetencion);
									/*if(codigoRetencion.equals("10")){
										elmtolimp.addElement("codigoRetencion").addText("9");
									} else if(codigoRetencion.equals("20")){
										elmtolimp.addElement("codigoRetencion").addText("10");
									} else if(codigoRetencion.equals("30")){
										elmtolimp.addElement("codigoRetencion").addText("1");
									} else if(codigoRetencion.equals("70")){
										elmtolimp.addElement("codigoRetencion").addText("2");
									} else if(codigoRetencion.equals("100")){
										elmtolimp.addElement("codigoRetencion").addText("3");
									} else {
										elmtolimp.addElement("codigoRetencion").addText(codigoRetencion);
									}*/
								}else{
									elmtolimp.addElement("codigoRetencion").addText(String.valueOf(codigoRetencion));
								}
								
								double porcentaje = (Double.valueOf(detalleRentecion.getValorRetencion()) * 100) / (Double.valueOf(detalleRentecion.getBaseImponibleReten()));
								
								
								elmtolimp.addElement("baseImponible").addText(format2.format(Double.valueOf(detalleRentecion.getBaseImponibleReten())).replace(",", "."));
								if (codigoRetencion.equals(new String("309"))||codigoRetencion.equals(new String("312"))||codigoRetencion.equals(new String("319"))||
										codigoRetencion.equals(new String("322"))||codigoRetencion.equals(new String("3440"))) {
									
									porcentajeRetencion = Double.valueOf(Math.round(porcentaje * 100.0) / 100.0).doubleValue();
									
									if (!porcentajeRetencion.equals(2.75) && codigoRetencion.equals(new String("3440"))){
										porcentajeRetencion = 2.75;
									}
									if (!porcentajeRetencion.equals(1.75) && codigoRetencion.equals(new String("322"))){
										porcentajeRetencion = 1.75;
									}
									if (!porcentajeRetencion.equals(1.75) && codigoRetencion.equals(new String("312"))){
										porcentajeRetencion = 1.75;
									}
									if (!porcentajeRetencion.equals(1.75) && codigoRetencion.equals(new String("309"))){
										porcentajeRetencion = 1.75;
									}
									elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(porcentajeRetencion));
								}else {
									
									if (codigoRetencion.equals(new String("1"))){
										elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(30));
									}else {
										if (codigoRetencion.equals(new String("2"))){
											elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(70));
										}else {
											if (codigoRetencion.equals(new String("3"))){
												elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(100));
											}else {
												if (codigoRetencion.equals(new String("9"))){
													elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(10));
												}else{
													if (codigoRetencion.equals(new String("10"))){
														elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(20));
													}else {
														elmtolimp.addElement("porcentajeRetener").addText(String.valueOf(Double.valueOf(Math.round(porcentaje)).doubleValue()));
													}	
												}
											}
										}
											
									}
									
								}
									
					            elmtolimp.addElement("valorRetenido").addText(format2.format(Double.valueOf(detalleRentecion.getValorRetencion())).replace(",", "."));
					            elmtolimp.addElement("codDocSustento").addText("01");
					            
					            if((lineas.get(0).getNroFactura().replaceAll("-", "")).length() == 15){
					            	elmtolimp.addElement("numDocSustento").addText(lineas.get(0).getNroFactura().replaceAll("-", ""));
					            }
							}
							
						}
						
						final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"), outputFormat);
						writer.write(document);
						writer.flush();
						writer.close();  log.info("Close BDD o Statement");
						
						File fileLlave = new File(confService.getDireccionFirma());
						if(fileLlave.exists()){
							log.info("Firmando XML del documento: " + documentoBase.getNrodocumento());
							file = auxiliares.firmarDocumento(file, confService);
							String fileString = null;
							
							if(file != null){
								byte[] bytes = auxiliares.filetobyte(file);
								fileString = new String(bytes, "UTF-8");
							}
							
							this.fileString = fileString;
							this.claveAcceso = claveAcceso;
						}else{
							this.fileString = null;
							this.mensaje = "ERROR / No hay la llave publica para firmar el documento";
						}
					} catch (ParseException ex) {
						// TODO Auto-generated catch block
						log.error(ex.getMessage());
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						log.error(ex.getMessage());
					}
				}else{
					this.mensaje = "ERROR / No hay lineas para este documento";
				}
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (conOpen != null) conOpen.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
		}
		
		log.info("Proceso finalizado del documento: " + documentoBase.getNrodocumento());
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public String getFileString() {
		return fileString;
	}

	public void setFileString(String fileString) {
		this.fileString = fileString;
	}

	public BDDEmpresaPrincipal getEmpresaPrincipal() {
		return empresaPrincipal;
	}

	public void setEmpresaPrincipal(BDDEmpresaPrincipal empresaPrincipal) {
		this.empresaPrincipal = empresaPrincipal;
	}
}
