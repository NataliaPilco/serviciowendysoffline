package atrums.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.BDDOpenbravo.BDDOadUserEmail;
import atrums.modelo.BDDOpenbravo.BDDOcInvoiceLine;
import atrums.modelo.BDDOpenbravo.BDDOcTax;
import atrums.modelo.BDDOpenbravo.BDDOcTaxcategory;
import atrums.modelo.BDDOpenbravo.BDDOmProduct;
import atrums.modelo.BDDOpenbravo.BDDOmProductCategory;
import atrums.modelo.SRI.SRIDocumentoAutorizado;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServEmail;
import atrums.modelo.Servicio.ServPago;
import atrums.modelo.Servicio.ServRespuestaControl;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacion;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;
import atrums.persistencia.ServiceInsertarFEControlDocumentos;

public class MonitorearFacturasMigracion implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearFacturasMigracion.class);
	private File docFile = null;
	private String correoCliente = null;
	private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearFacturasMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpenbravo = null;
		String adicional = "";
		this.docFile = null;
		this.correoCliente = null;
		
		try{
			ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
			ConfService confService = new ConfService();
			
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			
			log.info("Conectando a la bdd para procesar el documento");
			conHome = home.getConneccion(this.dataSourceOpenbravo);
			
			if(conHome != null && this.variablesServicio.isExtDocumentoFacMig()){
				if(home.getBDDDocumentoBaseMigrar("FC','FV" ,conHome)){
					documentoBase = home.getDocumentoBase();
					
					/**Codigo para evitar demaciado demanda de recurso**/
					
					if(this.variablesServicio.getDocumentFacMig().equals(documentoBase.getNrodocumento())){
						int auxConteo = this.variablesServicio.getRepeticionFacMig();
						auxConteo = auxConteo + 1;
						this.variablesServicio.setRepeticionFacMig(auxConteo);
						
						if(auxConteo == 5){
							this.variablesServicio.setContinuarFacMig(false);
							this.variablesServicio.setRepeticionFacMig(0);
						}
					}else{
						this.variablesServicio.setDocumentFacMig(documentoBase.getNrodocumento());
						this.variablesServicio.setRepeticionFacMig(0);
					}
					
					/***/
					
					log.info("Migrando documento factura: " + documentoBase.getNrodocumento());
					home.saveLogServicio(conHome, "Migrando documento factura al openbravo: " + documentoBase.getNrodocumento());
					
					log.info("Consultando Empresa para el documento: " + documentoBase.getNrodocumento());
					BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
					OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
							empresaPrincipal, 
							confService);
					
					log.info("Conectando a la bdd para procesar el documento: " + documentoBase.getNrodocumento());
					conOpenbravo = openbravo.getConneccion(this.dataSourceOpenbravo);
					openbravo.setDocumentoBase(documentoBase);
					
					log.info("Verificando si la conexion esta cerrada para el documento: " + documentoBase.getNrodocumento());
					if(!conOpenbravo.isClosed()){
						if(openbravo.getEmpresaPrincipal(conOpenbravo)){
							if(openbravo.existeDocumentoImporFCNC(conOpenbravo)){
								if(openbravo.getBDDOadClient(conOpenbravo)){
									if(openbravo.getBDDOadOrg(conOpenbravo)){
										if(openbravo.getBDDOcDoctype(conOpenbravo)){
											ServiceFacturaNotaRetenPorDocumento facturaPorDocumento = new ServiceFacturaNotaRetenPorDocumento(confService, 
													documentoBase.getNrodocumento(), 
													documentoBase.getNroestablecimiento(), 
													documentoBase.getNroemision(), 
													documentoBase.getTipodocumento());
											facturaPorDocumento.setFecha(documentoBase.getFechadocumento());
											
											List<ServDocumentoLinea> lineas = facturaPorDocumento.callDocumentos();
											
											if(lineas.size() > 0){
												boolean continuar = true;
												
												if(openbravo.existeDato("SELECT count(*) AS total FROM c_bpartner as cb WHERE cb.taxid = '" + lineas.get(0).getCliente() + "';", conOpenbravo)){
													ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getCliente());
													ServCliente cliente = serviceCliente.callCliente();
													if(cliente.getIdentificacion() != null){
														
														if(cliente.getEmail().getEmail() == null){
															ServEmail auxEmail = new ServEmail();
															auxEmail.setIdentificacion(cliente.getIdentificacion());
															auxEmail.setEmail("N/A");
															cliente.setEmail(auxEmail);
															adicional = ", no hay correo electronico";
														}else{
															this.correoCliente = cliente.getEmail().getEmail();
															adicional = ", correo electronico: " + this.correoCliente + " ";
														}
														
														if(cliente.getDireccion().getDireccion() == null){
															ServDireccion auxDireccion = new ServDireccion();
															auxDireccion.setIdentificacion(cliente.getIdentificacion());
															auxDireccion.setDireccion("N/A");
															auxDireccion.setDireccion2("N/A");
															cliente.setDireccion(auxDireccion);
														}
														
														openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
														openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
														openbravo.getOcbPartner().setTipo(cliente.getTipo());
														openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
														openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
														
														
														if(openbravo.existeDato("SELECT count(*) AS total FROM c_bp_group as cb WHERE upper(cb.name) LIKE '%CLIENTES COMERCIALES%';", conOpenbravo)){
															if(!openbravo.saveBDDOcbGroup(conOpenbravo)){
																conOpenbravo.rollback(); log.info("Rollback Proceso");
																continuar = false;
															}
														}
														
														if(openbravo.saveBDDOcbPartner(conOpenbravo) && continuar){
															openbravo.getOadUserEmail().setEmail(cliente.getEmail().getEmail());
															if(openbravo.saveBDDOadUser(conOpenbravo)){
																openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
																if(openbravo.saveBDDOcLocation(conOpenbravo)){
																	if(openbravo.saveBDDOcbPartnerLocation(conOpenbravo)){
																		openbravo.setOadUserEmail(new BDDOadUserEmail());
																		if(openbravo.saveBDDOadUserPortal(conOpenbravo)){
																			if(openbravo.existeDato("SELECT count(*) AS total FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%'", conOpenbravo)){
																				if(openbravo.saveBDDOadRole(conOpenbravo)){
																					if(openbravo.saveBDDOadRoleOrgaccess(conOpenbravo)){
																						if(!openbravo.saveBDDOadWindowAccess(conOpenbravo)){
																							conOpenbravo.rollback(); log.info("Rollback Proceso");
																							continuar = false;
																						}
																					}else{
																						conOpenbravo.rollback(); log.info("Rollback Proceso");
																						continuar = false;
																					}
																				}else{
																					conOpenbravo.rollback(); log.info("Rollback Proceso");
																					continuar = false;
																				}
																			}
																			
																			if(!openbravo.saveBDDOadUserRoles(conOpenbravo)){
																				conOpenbravo.rollback(); log.info("Rollback Proceso");
																				continuar = false;
																			}
																			
																		}else{
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																	}else{
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																}else{
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	continuar = false;
																}
															}else{
																conOpenbravo.rollback(); log.info("Rollback Proceso");
																continuar = false;
															}
														}else{
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}
												}else{
													openbravo.getOcbPartner().setIdentificacion(lineas.get(0).getCliente());
													
													ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getCliente());
													ServCliente cliente = serviceCliente.callCliente();
													if(cliente.getIdentificacion() != null){
														
														if(cliente.getEmail().getEmail() == null){
															ServEmail auxEmail = new ServEmail();
															auxEmail.setIdentificacion(cliente.getIdentificacion());
															auxEmail.setEmail("N/A");
															cliente.setEmail(auxEmail);
															adicional = ", no hay correo electronico";
														}else{
															this.correoCliente = cliente.getEmail().getEmail();
															adicional = ", correo electronico: " + this.correoCliente + " ";
														}
														
														if(cliente.getDireccion().getDireccion() == null){
															ServDireccion auxDireccion = new ServDireccion();
															auxDireccion.setIdentificacion(cliente.getIdentificacion());
															auxDireccion.setDireccion("N/A");
															auxDireccion.setDireccion2("N/A");
															cliente.setDireccion(auxDireccion);
														}
														
														openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
														openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
														openbravo.getOcbPartner().setTipo(cliente.getTipo());
														openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
														openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
													}
													
													openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
													openbravo.setOadUserEmail(new BDDOadUserEmail());
													
													openbravo.getOcbPartner().setIdentificacion(lineas.get(0).getCliente());
													if(openbravo.getBDDOcbPartner(conOpenbravo)){
														if(!openbravo.getBDDOadUser(conOpenbravo)){
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}else{
															if(cliente.getIdentificacion() != null){
																
																if(cliente.getEmail().getEmail() == null){
																	ServEmail auxEmail = new ServEmail();
																	auxEmail.setIdentificacion(cliente.getIdentificacion());
																	auxEmail.setEmail("N/A");
																	cliente.setEmail(auxEmail);
																	adicional = ", no hay correo electronico";
																}else{
																	this.correoCliente = cliente.getEmail().getEmail();
																	adicional = ", correo electronico: " + this.correoCliente + " ";
																}
															}
														}
													}else{
														conOpenbravo.rollback(); log.info("Rollback Proceso");
														continuar = false;
													}
												}
												
												if(openbravo.getOcbPartner().getId() != null && continuar){
													openbravo.getOcInvoice().setNroDocumento(documentoBase.getNrodocumento());
													openbravo.getOcInvoice().setFechaEmision(documentoBase.getFechadocumento());
													openbravo.getOcInvoice().setFechaContable(lineas.get(0).getFechaContable());
													openbravo.getOcInvoice().setNumeroAutorizacion(documentoBase.getNroautorizacion());
													openbravo.getOcInvoice().setFechaAutorizacion(documentoBase.getFechaautorizacion());
													openbravo.getOcInvoice().setNroEstablecimiento(documentoBase.getNroestablecimiento());
													openbravo.getOcInvoice().setNroEmision(documentoBase.getNroemision());
													openbravo.getOcInvoice().setEstado("AUTORIZADO");
													openbravo.getOcInvoice().setClaveAcceso(documentoBase.getClaveacceso());
													openbravo.getOcInvoice().setTipoDocumento(documentoBase.getTipodocumento());
													openbravo.getOcInvoice().setDescripcion("");
													
													if(!lineas.get(0).getPagos().isEmpty()){
														String auxMetodo = lineas.get(0).getPagos().get(0).getMetodo();
														
														if(auxMetodo.equals("01")){
															openbravo.getOcInvoice().setMetodoPago("SIN UTILIZACION DEL SISTEMA FINANCIERO");
														}else if(auxMetodo.equals("15")){
															openbravo.getOcInvoice().setMetodoPago("COMPENSACIÓN DE DEUDAS");
														}else if(auxMetodo.equals("16")){
															openbravo.getOcInvoice().setMetodoPago("TARJETA DE DÉBITO");
														}else if(auxMetodo.equals("17")){
															openbravo.getOcInvoice().setMetodoPago("DINERO ELECTRÓNICO");
														}else if(auxMetodo.equals("18")){
															openbravo.getOcInvoice().setMetodoPago("TARJETA PREPAGO");
														}else if(auxMetodo.equals("19")){
															openbravo.getOcInvoice().setMetodoPago("TARJETA DE CRÉDITO");
														}else if(auxMetodo.equals("20")){
															openbravo.getOcInvoice().setMetodoPago("TARJETA DE CRÉDITO");
														}else if(auxMetodo.equals("21")){
															openbravo.getOcInvoice().setMetodoPago("ENDOSO DE TÍTULOS");
														}else{
															openbravo.getOcInvoice().setMetodoPago("SIN UTILIZACION DEL SISTEMA FINANCIERO");
														}
													}else{
														openbravo.getOcInvoice().setMetodoPago("SIN UTILIZACION DEL SISTEMA FINANCIERO");
													}
													
													
													try{
														SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
														ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
																empresaPrincipal.getAmbiente(), 
																documentoBase.getClaveacceso());
														autorizado = serviceAutorizacion.CallAutorizado();
														
														if(autorizado.getDocXML() != null){
															openbravo.getOcInvoice().setFactura(autorizado.getDocXML());
															this.docFile = autorizado.getDocFile();
														}
													} catch (Exception ex){}
													
													if(openbravo.existeDato("SELECT count(*) AS total FROM c_paymentterm AS cp WHERE upper(cp.name) like '%CONTADO%'", conOpenbravo)){
														if(!openbravo.saveBDDOcPaymentterm(conOpenbravo)){
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}
													
													if(openbravo.existeDato("SELECT count(*) AS total FROM m_pricelist AS mp WHERE upper(mp.name) LIKE '%VENTA%'", conOpenbravo)){
														if(!openbravo.saveBDDOmPricelist(conOpenbravo)){
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}
													
													if(continuar){
														if(openbravo.saveBDDOcInvoice(conOpenbravo)){
															int linea = 10;
															for(int i=0; i<lineas.size(); i++){
																
																if(continuar){
																	openbravo.setOmProductCategory(new BDDOmProductCategory());
																	if(openbravo.existeDato("SELECT count(*) AS total FROM m_product_category AS mp WHERE upper(mp.name) LIKE '%" + openbravo.getOmProductCategory().getName().toUpperCase() + "%'", conOpenbravo)){
																		if(!openbravo.saveBDDmProductCategory(conOpenbravo)){
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOcTaxcategory(new BDDOcTaxcategory());
																	
																	if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 0){
																		openbravo.getOcTaxcategory().setCodigo("0");
																	}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 12){
																		openbravo.getOcTaxcategory().setCodigo("2");
																	}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 14){
																		openbravo.getOcTaxcategory().setCodigo("3");
																	}
																	
																	openbravo.getOcTaxcategory().setNombre("IVA " + String.valueOf(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue())));
																	
																	if(openbravo.existeDato("SELECT count(*) AS total FROM c_taxcategory as ct WHERE upper(ct.name) like '" + openbravo.getOcTaxcategory().getNombre() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOcTaxcategory(conOpenbravo)){
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOcTax(new BDDOcTax());
																	openbravo.getOcTax().setRate(lineas.get(i).getImpuesto());
																	
																	if(openbravo.existeDato("SELECT count(*) AS total FROM c_tax WHERE upper(name) like '" + openbravo.getOcTaxcategory().getNombre() + "' AND rate = '" + openbravo.getOcTax().getRate() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOcTax(conOpenbravo)){
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOmProduct(new BDDOmProduct());
																	String codProducto = ""; 
																	
																	if(lineas.get(i).getProducto().replace(" ", "").toLowerCase().length() > 25){
																		codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto().substring(0, 25));
																	}else{
																		codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto());
																	}
																	
																	openbravo.getOmProduct().setCodigo(codProducto);
																	openbravo.getOmProduct().setName(lineas.get(i).getProducto().replace("'", "").replace(",", ""));
																	if(openbravo.existeDato("SELECT count(*) AS total FROM m_product AS mp WHERE mp.value = '" + openbravo.getOmProduct().getCodigo() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOmProduct(conOpenbravo)){
																			conOpenbravo.rollback(); log.info("Rollback Proceso");
																			continuar = false;
																		}
																	}else if(!openbravo.updateBDDOmProduct(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}
																	
																	double auxTotalLinea = (Double.valueOf(lineas.get(i).getPrecio()) - Double.valueOf(lineas.get(i).getDescuento()));
														            auxTotalLinea = (Double.valueOf(lineas.get(i).getCantidad()) * auxTotalLinea);
														            double auxImpuestoVal = Double.valueOf(lineas.get(i).getImpuesto());
														            double auxValorImpues = (auxTotalLinea * auxImpuestoVal) / 100;
														            
														            double auxValorImpuestoReal = Double.valueOf(lineas.get(i).getValorImpuesto());
														            
														            if(auxValorImpuestoReal <= auxValorImpues){
														            	auxValorImpues = auxValorImpuestoReal;
														            }
																	
																	//DecimalFormat format2 = new DecimalFormat("0.00");
																	DecimalFormat format4 = new DecimalFormat("0.0000");
																	openbravo.setOcInvoiceLine(new BDDOcInvoiceLine());
																	openbravo.getOcInvoiceLine().setLinea(String.valueOf(linea));
																	openbravo.getOcInvoiceLine().setCantidad(lineas.get(i).getCantidad());
																	openbravo.getOcInvoiceLine().setPrecioUnitario(format4.format(Double.valueOf(lineas.get(i).getPrecio())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setPrecioTotal(format4.format(auxTotalLinea).replace(",", "."));//Double.valueOf(lineas.get(i).getTotalLinea())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setDescuento(format4.format(Double.valueOf(lineas.get(i).getDescuento())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setValor(format4.format(auxValorImpues).replace(",", "."));//Double.valueOf(lineas.get(i).getValorImpuesto())).replace(",", "."));
																	
																	if(!openbravo.saveBDDOcInvoiceLine(conOpenbravo)){
																		conOpenbravo.rollback(); log.info("Rollback Proceso");
																		continuar = false;
																	}else{
																		linea = linea + 10;
																	}
																}
															}
															
															if(continuar){
																DecimalFormat format2 = new DecimalFormat("0.00");
																String total = format2.format(Double.valueOf(lineas.get(0).getTotalFac())).replace(",", ".");
																
																if(!openbravo.procesarFactura(total, conOpenbravo)){
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	continuar = false;
																	adicional = "No se pudo procesar la factura ";
																}else{
																	String idPaymentSchedule = openbravo.buscarPaymentSchedule(conOpenbravo);
																	openbravo.iniciarPago(idPaymentSchedule, conOpenbravo);
																	
																	if(idPaymentSchedule != null && !lineas.get(0).getPagos().isEmpty()){
																		float totalPagos = 0;
																		String totalpago = "";
																		
																		for(ServPago auxPago: lineas.get(0).getPagos()){
																			String idpayment = UUID.randomUUID().toString().replace("-", "").toUpperCase();
																			totalPagos = totalPagos + Float.valueOf(auxPago.getMonto());
																			if(continuar){
																				String auxMetodoPago = "";
																				
																				if(auxPago.getMetodo().equals("01")){
																					auxMetodoPago = "SIN UTILIZACION DEL SISTEMA FINANCIERO";
																				}else if(auxPago.getMetodo().equals("15")){
																					auxMetodoPago = "COMPENSACIÓN DE DEUDAS";
																				}else if(auxPago.getMetodo().equals("16")){
																					auxMetodoPago = "TARJETA DE DÉBITO";
																				}else if(auxPago.getMetodo().equals("17")){
																					auxMetodoPago = "DINERO ELECTRÓNICO";
																				}else if(auxPago.getMetodo().equals("18")){
																					auxMetodoPago = "TARJETA PREPAGO";
																				}else if(auxPago.getMetodo().equals("19")){
																					auxMetodoPago = "TARJETA DE CRÉDITO";
																				}else if(auxPago.getMetodo().equals("20")){
																					auxMetodoPago = "TARJETA DE CRÉDITO";
																				}else if(auxPago.getMetodo().equals("21")){
																					auxMetodoPago = "ENDOSO DE TÍTULOS";
																				}else{
																					auxMetodoPago = "SIN UTILIZACION DEL SISTEMA FINANCIERO";
																				}
																				
																				String auxMontoPago = format2.format(Double.valueOf(auxPago.getMonto())).replace(",", ".");
																				
																				if(openbravo.savePago(idpayment, auxMetodoPago, auxMontoPago, conOpenbravo)){
																					String idpaymentline =  UUID.randomUUID().toString().replace("-", "").toUpperCase();
																					if(openbravo.savePagolinea(idpayment, idpaymentline, auxMontoPago, conOpenbravo)){
																						if(openbravo.savePaymentScheduledetail(idPaymentSchedule, idpaymentline, auxMontoPago, conOpenbravo)){
																							if(!openbravo.procesarPago(idpayment, true, conOpenbravo)){
																								conOpenbravo.rollback(); log.info("Rollback Proceso");
																								continuar = false;
																								adicional = "No se guardo el pago ";
																							}else{
																								totalpago = String.valueOf(totalPagos);
																							}
																						}else{
																							conOpenbravo.rollback(); log.info("Rollback Proceso");
																							continuar = false;
																							adicional = "No se guardo el pago ";
																						}
																					}else{
																						conOpenbravo.rollback(); log.info("Rollback Proceso");
																						continuar = false;
																						adicional = "No se guardo el pago ";
																					}
																				}else{
																					conOpenbravo.rollback(); log.info("Rollback Proceso");
																					continuar = false;
																					adicional = "No se guardo el pago ";
																				}
																			}
																		}
																		
																		if(continuar){
																			
																			log.info("Procesando en el Openbravo el documento: " + documentoBase.getNrodocumento());
																			if(!openbravo.terminarFactura("Y", totalpago, conOpenbravo)){
																				conOpenbravo.rollback(); log.info("Rollback Proceso");
																				continuar = false;
																				adicional = "No se guardo el pago ";
																			}
																		}
																	}
																}
															}
															
															if(continuar){
																log.info("Enviando a Wendys informacion del documento: " + documentoBase.getNrodocumento());
																ServiceInsertarFEControlDocumentos controlDocumentos = new ServiceInsertarFEControlDocumentos(
																		confService, 
																		documentoBase.getNrodocumento(), 
																		documentoBase.getNroestablecimiento(), 
																		documentoBase.getNroemision(), 
																		documentoBase.getNroautorizacion(), 
																		documentoBase.getFechaautorizacion(), 
																		documentoBase.getFechadocumento(), 
																		"2", 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		(documentoBase.getTipodocumento().equals("FC") ? "FV" : "FV"));
																
																ServRespuestaControl respuestaControl = controlDocumentos.callRespuesta();
																
																if(respuestaControl.isRespuesta()){
																	conOpenbravo.commit();
																	log.info("Datos migrados");
																	
																	documentoBase.setEstado("MIG");
																	documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes" + adicional);
																	documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																	documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																	if(home.actualizarEstado(conHome)){
																		conHome.commit();
																	}else{
																		conHome.rollback(); log.info("Rollback Proceso");
																	}
																	
																	if(openbravo.getOcbPartner().getIdentificacion().indexOf("9999999999") == -1){
																		try{
																			if(confService.isEnviarCorreo() && this.correoCliente != null){
																				EnviarEmail email = new EnviarEmail(conOpenbravo, openbravo, confService, this.docFile);
																				
																				documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																				documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																				
																				if(email.enviar(this.correoCliente, 
																						documentoBase.getClaveacceso(), 
																						openbravo.getOcInvoice().getId(), 
																						documentoBase.getTipodocumento(),
																						documentoBase)){
																					adicional = adicional + ", Correo enviado al: " + this.correoCliente;
																				}else{
																					adicional = adicional + ", Correo no enviado al: " + this.correoCliente;
																				}
																			}else{
																				if(!confService.isEnviarCorreo()){
																					adicional = adicional + ", no esta activo la opción de enviar email";
																				}else if(this.correoCliente == null){
																					adicional = adicional + ", no hay correo para enviar email";
																				}
																			}
																		}catch (Exception ex){}
																	}
																	
																	documentoBase.setEstado("MIG");
																	documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes" + adicional);
																	documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																	documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																	if(home.actualizarEstado(conHome)){
																		home.saveLogServicio(conHome, "Documento factura migrado al openbravo: " + documentoBase.getNrodocumento() + " - " + adicional);
																		conHome.commit();
																	}else{
																		conHome.rollback(); log.info("Rollback Proceso");
																	}
																}else{
																	conOpenbravo.rollback(); log.info("Rollback Proceso");
																	log.info("Datos no migrados ver log");
																	home.saveLogServicio(conHome, "Documento factura no migrado al openbravo: " + documentoBase.getNrodocumento());
																}
															}else{
																if(home.actualizarEstado(conHome)){
																	home.saveLogServicio(conHome, "Documento factura no migrado al openbravo: " + adicional);
																	conHome.commit();
																}else{
																	conHome.rollback(); log.info("Rollback Proceso");
																}
															}
														}else{
															if(home.actualizarEstado(conHome)){
																conHome.commit();
															}else{
																conHome.rollback(); log.info("Rollback Proceso");
															}
															conOpenbravo.rollback(); log.info("Rollback Proceso");
															continuar = false;
														}
													}
												}
											}else{
												documentoBase.setMensaje("ERROR / No hay lineas para el documento: " + documentoBase.getNrodocumento());
												home.setDocumentoBase(documentoBase);
												if(home.actualizarEstado(conHome)){
													conHome.commit();
												}else{
													conHome.rollback(); log.info("Rollback Proceso");
												}
											}
										}else{
											log.info("Esperando documento...");
											documentoBase.setMensaje("ERROR / No hay un documento fatura para la sucursal con establecimiento: " + documentoBase.getNroestablecimiento() + ", emision: " + documentoBase.getNroemision());
											home.setDocumentoBase(documentoBase);
											if(home.actualizarEstado(conHome)){
												conHome.commit();
											}else{
												conHome.rollback(); log.info("Rollback Proceso");
											}
										}
									}else{
										log.info("Esperando sucursal...");
										documentoBase.setMensaje("ERROR / No hay una sucursal para este documento, establecimiento: " + documentoBase.getNroestablecimiento() + ", emision: " + documentoBase.getNroemision());
										home.setDocumentoBase(documentoBase);
										if(home.actualizarEstado(conHome)){
											conHome.commit();
										}else{
											conHome.rollback(); log.info("Rollback Proceso");
										}
									}
								}else{
									log.info("Esperando empresa...");
									documentoBase.setMensaje("ERROR / No hay una empresa para este documento");
									try { if (conOpenbravo != null) conOpenbravo.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
									try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
									this.variablesServicio.setExtDocumentoFacMig(false);
								}
							}else{
								documentoBase.setEstado("MIG");
								home.setDocumentoBase(documentoBase);
								if(home.actualizarEstado(conHome)){
									conHome.commit();
								}else{
									conHome.rollback(); log.info("Rollback Proceso");
								}
							}
						}else{
							log.info("Esperando empresa...");
							try { if (conOpenbravo != null) conOpenbravo.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
							try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
							this.variablesServicio.setExtDocumentoFacMig(false);
						}
					}
				}else{
					log.info("Esperando documento...");
					try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoFacMig(false);
				}
			}else{
				log.info("Esperando conexión openbravo...");
				this.variablesServicio.setExtDocumentoFacMig(false);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpenbravo != null) conOpenbravo.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
			try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
		}
		
		log.info("Finalizando proceso de migración");
	}
}