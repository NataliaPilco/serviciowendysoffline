package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.servicios.ServicioMonitorearRetencionesMigracion;
import atrums.dao.servicios.ServicioMonitorearRetencionesSRI;
import atrums.dao.servicios.ServicioMonitorearRetencionesWeb;
import atrums.modelo.VariablesServicio;

public class MonitorearRetenciones implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearRetenciones.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearRetenciones(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Iniciando Servicio de monitoreo Retenciones");
		
		try {Thread.sleep(21);} catch (InterruptedException e) {}
		ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
        Runnable runnableWeb = new ServicioMonitorearRetencionesWeb(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceWeb.execute(runnableWeb);
		
        try {Thread.sleep(60024);} catch (InterruptedException e) {}
		ExecutorService exServiceSRI = Executors.newFixedThreadPool(1);
        Runnable runnableServiceSRI = new ServicioMonitorearRetencionesSRI(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceSRI.execute(runnableServiceSRI);
        
        try {Thread.sleep(60027);} catch (InterruptedException e) {}
        ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
        Runnable runnableMigracion = new ServicioMonitorearRetencionesMigracion(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceMigr.execute(runnableMigracion);
		
        exServiceMigr.shutdown();
        exServiceSRI.shutdown();
        exServiceWeb.shutdown();
        
        while (!exServiceMigr.isTerminated()){}
        while (!exServiceSRI.isTerminated()){}
        while (!exServiceWeb.isTerminated()){}
	}
}
