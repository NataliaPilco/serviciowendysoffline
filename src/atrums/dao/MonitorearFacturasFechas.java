package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceFacturaFecha;
import atrums.persistencia.ServiceFacturaPorFechaPagina;
import atrums.persistencia.ServiceNroPaginasFacturas;

import org.apache.log4j.Logger;

public class MonitorearFacturasFechas implements Runnable{
    static final Logger log = Logger.getLogger(MonitorearFacturasFechas.class);
    private ConfService confService;
    private DataSource dataSourceOpenbravo;
    
    public MonitorearFacturasFechas(DataSource dataSourceOpenbravo){
        this.confService = new ConfService();
        this.dataSourceOpenbravo = dataSourceOpenbravo;
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Monitoreando Fechas de Facturas");
		OperacionesBDDHome home = new OperacionesBDDHome();
    	
    	try{
    		ServiceFacturaFecha serviceFecha = new ServiceFacturaFecha(this.confService);
            List<String> auxFechas = serviceFecha.callFechas();
            serviceFecha = null;
            
            Collections.sort(auxFechas);
            Collections.reverse(auxFechas);
            
            if(auxFechas.size() > 10){
            	List<String> auxFechas2 = auxFechas;
	        	auxFechas = new ArrayList<String>();
	        	for(int i=0;i<7;i++){
	        		auxFechas.add(auxFechas2.get(i));
	        	}
	        	
	        	auxFechas2 = null;
            }
            
            log.info("Fechas totales a monitorear: " + auxFechas.size());
            for(String fecha: auxFechas){
            	/**Codigo Auxiliar**/
            	ServiceFacturaFecha serviceFecha2 = new ServiceFacturaFecha(this.confService);
                List<String> auxFechas2 = serviceFecha2.callFechas();
                Collections.sort(auxFechas2);
                Collections.reverse(auxFechas2);
                String fechainicial = fecha;
                
                if(!auxFechas2.isEmpty()){
                	fechainicial = auxFechas2.get(0);
                	auxFechas2 = null;
                }
                
                	Connection connection = null;
                	
                	try{
                		log.info("Conectando a la bdd");
                		connection = home.getConneccion(dataSourceOpenbravo);
                		
                		log.info("Verificando si existe conexion a la bdd");
                		if(connection != null){
                			
                    		/**Codigo Auxiliar**/
                        	ServiceNroPaginasFacturas serviceNroPaginasFacturas = new ServiceNroPaginasFacturas(this.confService, fecha);
                        	int numeroPaginas = serviceNroPaginasFacturas.callDocumentos();
                        	log.info("Monitoreando Fecha Factura: " + fecha);
                        	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
                            
                        	List<ServDocumentoNroNroEsNroEm> auxDoc = new ArrayList<ServDocumentoNroNroEsNroEm>();
                        	
                        	for(int i=1; i <= numeroPaginas; i++){
                        		ServiceFacturaPorFechaPagina serviceFacturaPorFechaPagina = new ServiceFacturaPorFechaPagina(this.confService, fecha, String.valueOf(i));
                        		List<ServDocumentoNroNroEsNroEm> auxDoc1 = serviceFacturaPorFechaPagina.callDocumentos();
                        		
                        		if(!auxDoc1.isEmpty()){
                        			auxDoc.addAll(auxDoc1);
                        		}
                        		auxDoc1.clear();
                        	}
                        	
                        	/**Codigo Auxiliar**/
                        	ServiceNroPaginasFacturas serviceNroPaginasFacturas2 = new ServiceNroPaginasFacturas(this.confService, fechainicial);
                        	int numeroPaginas2 = serviceNroPaginasFacturas2.callDocumentos();
                        	
                        	if (fecha != fechainicial) {
                        		log.info("Paginas de la Fecha 2: " + fechainicial + " - " + numeroPaginas2);
                        		
                        		for(int k=1; k <= numeroPaginas2; k++){
                            		ServiceFacturaPorFechaPagina serviceFacturaPorFechaPagina2 = new ServiceFacturaPorFechaPagina(confService, fechainicial, String.valueOf(k));
                            		List<ServDocumentoNroNroEsNroEm> auxDoc2 = serviceFacturaPorFechaPagina2.callDocumentos();
                            		
                            		if(!auxDoc2.isEmpty()){
                            			auxDoc.addAll(auxDoc2);
                            		}
                            		auxDoc2.clear();
                            	}            	
                        	}
                        	
                        	log.info("Documento por pagina: " + auxDoc.size());
                    		
                        	/**Codigo Auxiliar**/
                            for(ServDocumentoNroNroEsNroEm datodoc: auxDoc){
                            	BDDDocumentoBase documentoBase = new BDDDocumentoBase(
                            			datodoc.getNroDocumento(), 
                            			datodoc.getNroEstablecimiento(), 
                            			datodoc.getNroEmision(), 
                            			datodoc.getTipoDoc(), 
                            			datodoc.getFechadocumento());
                        		home.setDocumentoBase(documentoBase);
                        		
                        		try{
                            		if(home.guardarDocumento(connection)){
                            			home.saveLogServicio(connection, "Preparando documento factura de la fecha: " + fecha 
                            					+ " - documento: " + documentoBase.getNrodocumento() 
                            					+ " - ptoemision: " + documentoBase.getNroemision()
                            					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
                            			connection.commit();
                            		}else{
                            			connection.rollback(); log.info("Rollback Proceso");
                            		}
                            	} catch (SQLException ex) {
                            		log.error(ex.getMessage());
                            	}
                            }
                            
                            auxDoc.clear();
                            auxDoc = null;
                    	}
					} finally {
        				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
        			}
            	}
            auxFechas = null;
    	} catch (Exception ex){
    		log.warn(ex.getMessage(), ex);
		}
	}
}