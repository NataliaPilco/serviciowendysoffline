package atrums.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServEmail;
import atrums.modelo.Servicio.ServImpuestos;
import atrums.modelo.Servicio.ServPago;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;

public class GenerarDocumentosFactura implements Runnable{
	static final Logger log = Logger.getLogger(GenerarDocumentosFactura.class);
	private ConfService confService;
    private ConfBDDOpenbravo confBDDOpenbravo;
    private BDDDocumentoBase documentoBase;
    private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
    private BDDEmpresaPrincipal empresaPrincipal;
    private String fileString = null;
    private String claveAcceso = null;
    private String mensaje = null;
    private DecimalFormat format2 = new DecimalFormat("0.00");
    private DecimalFormat format4 = new DecimalFormat("0.000000");
    private String auxMensajeError = null;
    private DataSource dataSourceOpenbravo;
    
    public GenerarDocumentosFactura(BDDDocumentoBase documentoBase, DataSource dataSourceOpenbravo) {
    	this.confService = new ConfService();
    	this.confBDDOpenbravo = new ConfBDDOpenbravo();
    	this.documentoBase = documentoBase;
    	this.dataSourceOpenbravo = dataSourceOpenbravo;
	}
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conOpen = null;
		double auxTotal = 0;
		double subtotal = 0;
		
		try {
			log.info("Procesando documento: " + documentoBase.getNrodocumento());
			ServiceFacturaNotaRetenPorDocumento documento = new ServiceFacturaNotaRetenPorDocumento(confService, 
					documentoBase.getNrodocumento(), 
					documentoBase.getNroestablecimiento(), 
					documentoBase.getNroemision(), 
					documentoBase.getTipodocumento());
			documento.setFecha(documentoBase.getFechadocumento());
			
			log.info("Consultando Lineas del documento: " + documentoBase.getNrodocumento());
			List<ServDocumentoLinea> lineas = documento.callDocumentos();
			
			log.info("Consultando Empresa para el documento: " + documentoBase.getNrodocumento());
			this.empresaPrincipal = new BDDEmpresaPrincipal();
			OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(this.confBDDOpenbravo, this.empresaPrincipal, this.confService);
			
			log.info("Conectando a la bdd para procesar el documento: " + documentoBase.getNrodocumento());
			conOpen = openbravo.getConneccion(dataSourceOpenbravo);
			
			log.info("Extrayendo datos de la bdd para el documento: " + documentoBase.getNrodocumento());
			if(conOpen != null){
				if(openbravo.getEmpresaPrincipal(conOpen)){
					empresaPrincipal = openbravo.getEmpresaPrincipal();
					if(lineas.size() > 0){
						
						ServCliente cliente = null;
						
						if(lineas.get(0).getCliente() != null){
							ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getCliente());
							cliente = serviceCliente.callCliente();
						}else{
							cliente = new ServCliente(); 
						}
						
						for(int k=0; k<lineas.size(); k++){
							if(lineas.get(k).getMensajeError() != null){
								auxMensajeError = lineas.get(k).getMensajeError();
							}
						}
						
						if(auxMensajeError == null){
							if(cliente.getIdentificacion() != null){
								
								if(cliente.getEmail().getEmail() == null){
									ServEmail auxEmail = new ServEmail();
									auxEmail.setIdentificacion(cliente.getIdentificacion());
									auxEmail.setEmail("N/A");
									cliente.setEmail(auxEmail);
								}
								
								if(cliente.getDireccion().getDireccion() == null){
									ServDireccion auxDireccion = new ServDireccion();
									auxDireccion.setIdentificacion(cliente.getIdentificacion());
									auxDireccion.setDireccion("N/A");
									auxDireccion.setDireccion2("N/A");
									cliente.setDireccion(auxDireccion);
								}
								
								if(cliente.getEmail().getEmail() != null && cliente.getDireccion().getDireccion() != null){
									try {
										File file = File.createTempFile("documento", ".xml", null);
										file.deleteOnExit();
										
										Document document = DocumentHelper.createDocument();
										OutputFormat outputFormat = OutputFormat.createPrettyPrint();
										Element elmfac = null;
										
										String tipoComprobante = "";
										
										elmfac = document.addElement("factura");
										elmfac.addAttribute("id", "comprobante");
										elmfac.addAttribute("version", "1.1.0");
										tipoComprobante = "01";
										
										final Element elminftri = elmfac.addElement("infoTributaria");
										
										elminftri.addElement("ambiente").addText(empresaPrincipal.getAmbiente());
										elminftri.addElement("tipoEmision").addText(empresaPrincipal.getTipoEmision());
										elminftri.addElement("razonSocial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getRazonSocial()));
										elminftri.addElement("nombreComercial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getNombreComercial()));
										elminftri.addElement("ruc").addText(empresaPrincipal.getRuc());
										
										DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
										Date fechaDoc = format.parse(documentoBase.getFechadocumento());
										SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
										SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
										
										String secuencial = documentoBase.getNrodocumento();
										for (int i = 0; i < (9 - documentoBase.getNrodocumento().length()); i++) {
											secuencial = "0" + secuencial;
										}
										
										String codNumerico = empresaPrincipal.getCodNumerico();
										
										if(empresaPrincipal.getCodNumerico() != null){
											for (int i = 0; i < (8 - empresaPrincipal.getCodNumerico().length()); i++) {
												codNumerico = "0" + codNumerico;
											}
										}
										
										String claveAcceso = this.auxiliares.generarclaveacceso(
												sdfFormatoClave.format(fechaDoc), 
												tipoComprobante, 
												empresaPrincipal.getRuc(), 
												empresaPrincipal.getAmbiente(), 
												documentoBase.getNroestablecimiento() + documentoBase.getNroemision(), 
												secuencial, 
												codNumerico, 
												empresaPrincipal.getTipoEmision());
										
										elminftri.addElement("claveAcceso").addText(claveAcceso);
										elminftri.addElement("codDoc").addText(tipoComprobante);
										elminftri.addElement("estab").addText(documentoBase.getNroestablecimiento());
										elminftri.addElement("ptoEmi").addText(documentoBase.getNroemision());
										elminftri.addElement("secuencial").addText(secuencial);
										elminftri.addElement("dirMatriz").addText(empresaPrincipal.getDireccion());
										
										Element elminffac = null;
										elminffac = elmfac.addElement("infoFactura");
										
										elminffac.addElement("fechaEmision").addText(sdfFormato.format(fechaDoc));
										elminffac.addElement("dirEstablecimiento").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getDireccion()));
										
										if(empresaPrincipal.getNumResolucion() != null){
											String numResolucion = empresaPrincipal.getNumResolucion();
											
											for (int i = 0; i < (3 - empresaPrincipal.getNumResolucion().length()); i++) {
												numResolucion = "0" + numResolucion;
											}
											
											elminffac.addElement("contribuyenteEspecial").addText(numResolucion);
										}
										
										if(empresaPrincipal.isObliContabi()){
											elminffac.addElement("obligadoContabilidad").addText("SI");
										}else{
											elminffac.addElement("obligadoContabilidad").addText("NO");
										}
										
										if(lineas.get(0).getCliente().equals("9999999999999")){
											elminffac.addElement("tipoIdentificacionComprador").addText("07");
										}else if(lineas.get(0).getCliente().length() == 13 && lineas.get(0).getCliente().substring(10, lineas.get(0).getCliente().length()).equals("001")){
											elminffac.addElement("tipoIdentificacionComprador").addText("04");
										}else if(lineas.get(0).getCliente().length() == 10 && auxiliares.isNumeric(lineas.get(0).getCliente())){
											elminffac.addElement("tipoIdentificacionComprador").addText("05");
										}else{
											elminffac.addElement("tipoIdentificacionComprador").addText("06");
										}
										
										if(lineas.get(0).getCliente().equals("9999999999999")){
											elminffac.addElement("razonSocialComprador").addText("CONSUMIDOR FINAL");
										}else{
											elminffac.addElement("razonSocialComprador").addText(lineas.get(0).getRazonSocial());
										}
										
										elminffac.addElement("identificacionComprador").addText(lineas.get(0).getCliente());
										
										Element totalSinImpuestos = elminffac.addElement("totalSinImpuestos");
										
										Element elmDescTot = null;
										
										elmDescTot = elminffac.addElement("totalDescuento");
										
										Element elmtolcimp = null;
										elmtolcimp = elminffac.addElement("totalConImpuestos");
										
										List<ServImpuestos> impuestos = new ArrayList<ServImpuestos>();
										
										for(int i=0;i<lineas.size();i++){
											boolean encontrado = true;
											
											double auxTotalLinea = (Double.valueOf(lineas.get(i).getPrecio()) - Double.valueOf(lineas.get(i).getDescuento()));
								            auxTotalLinea = (Double.valueOf(lineas.get(i).getCantidad()) * auxTotalLinea);
								            double auxImpuestoVal = Double.valueOf(lineas.get(i).getImpuesto());
								            double auxValorImpues = (auxTotalLinea * auxImpuestoVal) / 100;
								            
								            double auxValorImpuestoReal = Double.valueOf(lineas.get(i).getValorImpuesto());
								            
								            if(auxValorImpuestoReal <= auxValorImpues){
								            	auxValorImpues = auxValorImpuestoReal;
								            }
											
											for(int j=0;j<impuestos.size();j++){
												
												String auxPorcentaje = "";
												if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 0){
													auxPorcentaje = "0";
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 12){
													auxPorcentaje = "2";
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 14){
													auxPorcentaje = "3";
												}
												
												if(impuestos.get(j).getCodigoImpuesto().equals("2") && 
														impuestos.get(j).getPorcentajeImpuesto().equals(auxPorcentaje)){
													encontrado = false;
													impuestos.get(j).setBaseImponible(impuestos.get(j).getBaseImponible() + 
															Double.valueOf(format4.format(auxTotalLinea).replace(",", "."))//Double.valueOf(lineas.get(i).getBaseImponible())).replace(",", "."))
															);
													impuestos.get(j).setValor(impuestos.get(j).getValor() + 
															Double.valueOf(format4.format(auxValorImpues).replace(",", "."))//Double.valueOf(lineas.get(i).getValorImpuesto())).replace(",", "."))
															);
												}
											}
											
											if(encontrado){
												ServImpuestos auxImp = new ServImpuestos();
												auxImp.setCodigoImpuesto("2");
												
												if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 0){
													auxImp.setPorcentajeImpuesto("0");
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 12){
													auxImp.setPorcentajeImpuesto("2");
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 14){
													auxImp.setPorcentajeImpuesto("3");
												}
												
												auxImp.setBaseImponible(auxImp.getBaseImponible() + 
														Double.valueOf(format4.format(auxTotalLinea).replace(",", "."))//Double.valueOf(lineas.get(i).getBaseImponible())).replace(",", "."))
														);
												auxImp.setValor(auxImp.getValor() + 
														Double.valueOf(format4.format(auxValorImpues).replace(",", "."))//format2.format(Double.valueOf(lineas.get(i).getValorImpuesto())).replace(",", "."))
														);
												
												impuestos.add(auxImp);
											}
										}
										
										for(int k=0;k<impuestos.size();k++){
											Element elmtolimp = null;
											elmtolimp = elmtolcimp.addElement("totalImpuesto");
											
											elmtolimp.addElement("codigo").addText(impuestos.get(k).getCodigoImpuesto());
											elmtolimp.addElement("codigoPorcentaje").addText(impuestos.get(k).getPorcentajeImpuesto());
											
											elmtolimp.addElement("baseImponible").addText(format2.format(impuestos.get(k).getBaseImponible()).replace(",", "."));
								            elmtolimp.addElement("valor").addText(format2.format(impuestos.get(k).getValor()).replace(",", "."));
										}
										
										elminffac.addElement("propina").addText("0.00");
										elminffac.addElement("importeTotal").addText(format2.format(Double.valueOf(lineas.get(0).getTotalFac())).replace(",", "."));
										auxTotal = Double.valueOf(lineas.get(0).getTotalFac());
										
										elminffac.addElement("moneda").addText("DOLAR");
										
										if(!lineas.get(0).getPagos().isEmpty()){
											Element elmpagos = null;
											elmpagos = elminffac.addElement("pagos");
											
											for(ServPago auxpago: lineas.get(0).getPagos()){
												Element elmpago = null;
												
												elmpago = elmpagos.addElement("pago");
												
												elmpago.addElement("formaPago").addText(auxpago.getMetodo());
												elmpago.addElement("total").addText(format2.format(Double.valueOf(auxpago.getMonto())).replace(",", "."));
												elmpago.addElement("plazo").addText("0");
												elmpago.addElement("unidadTiempo").addText("dias");
											}
											
										}else{
											log.warn("No hay metodo de pago en la factura");
										}
										
										if(auxMensajeError == null){
											Element elmdetfac = null;
											elmdetfac = elmfac.addElement("detalles");
											
											double descuento = 0;
											
											for(int i=0;i<lineas.size();i++){
												Element elmdeta = null;
												elmdeta = elmdetfac.addElement("detalle");
												
												String codProducto = ""; 
												if(lineas.get(i).getProducto().replace(" ", "").toLowerCase().length() > 25){
													codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto().substring(0, 25));
												}else{
													codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto());
												}
												
												elmdeta.addElement("codigoPrincipal").addText(codProducto);
												
												elmdeta.addElement("codigoAuxiliar").addText(codProducto);
												
												elmdeta.addElement("descripcion").addText(lineas.get(i).getProducto());
												elmdeta.addElement("cantidad").addText(lineas.get(i).getCantidad());
												elmdeta.addElement("precioUnitario").addText(format4.format(Double.valueOf(lineas.get(i).getPrecio())).replace(",", "."));
									            elmdeta.addElement("descuento").addText(format2.format(Double.valueOf(lineas.get(i).getDescuento())).replace(",", "."));
									            
									            double auxTotalLinea = (Double.valueOf(lineas.get(i).getPrecio()) - Double.valueOf(lineas.get(i).getDescuento()));
									            auxTotalLinea = (Double.valueOf(lineas.get(i).getCantidad()) * auxTotalLinea);
									            double auxImpuestoVal = Double.valueOf(lineas.get(i).getImpuesto());
									            double auxValorImpues = (auxTotalLinea * auxImpuestoVal) / 100;
									            
									            log.info("total linea: " + auxTotalLinea);
									            log.info("total impuesto: " + auxValorImpues);
									            
									            descuento = descuento + Double.valueOf(format2.format(Double.valueOf(lineas.get(i).getDescuento())).replace(",", "."));
									            subtotal = subtotal +  Double.valueOf(format4.format(auxTotalLinea).replace(",", ".")); //subtotal + Double.valueOf(format2.format(Double.valueOf(lineas.get(i).getTotalLinea())).replace(",", "."));
									            elmdeta.addElement("precioTotalSinImpuesto").addText(format2.format(auxTotalLinea).replace(",", ".")); //format2.format(Double.valueOf(lineas.get(i).getTotalLinea())).replace(",", "."));
									            
									            Element elmdetimps = null;
									            elmdetimps = elmdeta.addElement("impuestos");
									            Element elmdetimp = elmdetimps.addElement("impuesto");
									            elmdetimp.addElement("codigo").addText("2");
									            
									            if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 0){
									            	elmdetimp.addElement("codigoPorcentaje").addText("0");
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 12){
													elmdetimp.addElement("codigoPorcentaje").addText("2");
												}else if(Integer.valueOf(Double.valueOf(lineas.get(i).getImpuesto()).intValue()) == 14){
													elmdetimp.addElement("codigoPorcentaje").addText("3");
												}
									            
									            elmdetimp.addElement("tarifa").addText(format2.format(Double.valueOf(lineas.get(i).getImpuesto())).replace(",", "."));
									            elmdetimp.addElement("baseImponible").addText(format2.format(Double.valueOf(lineas.get(i).getBaseImponible())).replace(",", "."));
									            
									            double auxValorImpuestoReal = Double.valueOf(lineas.get(i).getValorImpuesto());
									            
									            if(auxValorImpuestoReal <= auxValorImpues){
									            	auxValorImpues = auxValorImpuestoReal;
									            }
									            
									            elmdetimp.addElement("valor").addText(format2.format(auxValorImpues).replace(",", "."));//format2.format(Double.valueOf(lineas.get(i).getValorImpuesto())).replace(",", "."));
											}
											
											totalSinImpuestos.addText(format2.format(subtotal).replace(",", "."));
											
											elmDescTot.setText(format2.format(descuento).replace(",", "."));
											
											if(confService.getEmail() != null || confService.getWebConsulta() != null){
												Element elminfAdc = elmfac.addElement("infoAdicional");
												
												if(confService.getEmail() != null){
													elminfAdc.addElement("campoAdicional").addAttribute("nombre", "EMAiL").addText(confService.getEmail());
												}
												
												if(confService.getWebConsulta() != null){
													elminfAdc.addElement("campoAdicional").addAttribute("nombre", "PORTAL-CONSULTA").addText(confService.getWebConsulta());
												}
											}
											
											final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"), outputFormat);
											writer.write(document);
											writer.flush();
											writer.close();  log.info("Close BDD o Statement");
											
											File fileLlave = new File(confService.getDireccionFirma());
											
											log.info("subtotal: " + subtotal);
											log.info("descuento: " + descuento);
											log.info("total: " + auxTotal);
											
											if(subtotal > auxTotal){
												this.fileString = null;
												this.mensaje = "ERROR / El total de las lineas no esta deacuerdo con el total de la factura";
											}else if(fileLlave.exists()){
												file = auxiliares.firmarDocumento(file, confService);
												String fileString = null;
												
												if(file != null){
													byte[] bytes = auxiliares.filetobyte(file);
													fileString = new String(bytes, "UTF-8");
												}
												
												this.fileString = fileString;
												this.claveAcceso = claveAcceso;
												lineas = null;
											}else{
												this.fileString = null;
												this.mensaje = "ERROR / No hay la llave publica para firmar el documento";
												lineas = null;
											}
										}else{
											this.fileString = null;
											this.mensaje = "ERROR INTERNO WEB SERVICE / " + auxMensajeError;
											lineas = null;
										}
									} catch (ParseException ex) {
										// TODO Auto-generated catch block
										log.error(ex.getMessage());	
									} catch (IOException ex) {
										// TODO Auto-generated catch block
										log.error(ex.getMessage());
									}
								}else{
									this.mensaje = "ERROR INTERNO WEB SERVICE / El cliente no tiene dirección o correo electrónico";
									lineas = null;
								}
							}else{
								this.mensaje = "ERROR INTERNO WEB SERVICE / No hay un cliente para esta factura, " + (lineas.get(0).getCliente() == null? "específicamente la factura no tiene cliente" : ("cliente: " + lineas.get(0).getCliente()));
								lineas = null;
							}
						}else{
							this.mensaje = "ERROR INTERNO WEB SERVICE / " + auxMensajeError;
							lineas = null;
						}
					}else{
						this.mensaje = "ERROR / No hay lineas para este documento";
						lineas = null;
					}
				}
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (conOpen != null) conOpen.close();  log.info("Close BDD o Statement"); } catch (Exception ex) {};
		}
		
		log.info("Proceso finalizado del documento: " + documentoBase.getNrodocumento());
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public String getFileString() {
		return fileString;
	}

	public void setFileString(String fileString) {
		this.fileString = fileString;
	}

	public BDDEmpresaPrincipal getEmpresaPrincipal() {
		return empresaPrincipal;
	}

	public void setEmpresaPrincipal(BDDEmpresaPrincipal empresaPrincipal) {
		this.empresaPrincipal = empresaPrincipal;
	}
}
