package atrums.dao;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDOpenbravo;

public class EnviarEmail {
	static final Logger log = Logger.getLogger(EnviarEmail.class);
	private OperacionesBDDOpenbravo openbravo;
	private Connection connection;
	private ConfService service;
	private File xml;
	private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
	
	public EnviarEmail(Connection connection, 
			OperacionesBDDOpenbravo openbravo, 
			ConfService service, 
			File xml) {
		this.connection = connection;
		this.openbravo = openbravo;
		this.service = service;
		this.xml = xml;
	}
	
	public boolean enviar(String correo, 
			String claveAcceso, 
			String idDocumento, 
			String tipoDoc, 
			BDDDocumentoBase documentoBase){
		if(openbravo.getBDDOcPocConfiguration(connection)){
			
			try {
				Properties props = new Properties();
				 
				props.setProperty("mail.smtp.host", this.openbravo.getOcPocConfiguration().getSmtpserver());
				props.setProperty("mail.smtp.port", this.openbravo.getOcPocConfiguration().getSmtpport());
				props.setProperty("mail.smtp.auth", this.openbravo.getOcPocConfiguration().isIssmtpautorization() ? "true" : "false");
				props.setProperty("mail.smtp.starttls.enable", "true");
				
				Session session = Session.getDefaultInstance(props, null);

	            BodyPart texto = new MimeBodyPart();
	            
	            String cuerpoCorreo = this.service.getCuerpoCorreo();
	        	
	            cuerpoCorreo = cuerpoCorreo.replace("user", documentoBase.getUsuario());
	            cuerpoCorreo = cuerpoCorreo.replace("pass", documentoBase.getPassword());
	            
	            texto.setText(cuerpoCorreo);
	            BodyPart adjunto = new MimeBodyPart();
	            
	            adjunto.setDataHandler(
	                new DataHandler(new FileDataSource(this.xml.getPath())));
	            adjunto.setFileName("documento - " + claveAcceso + ".xml");
	            
	            BodyPart adjunto2 = new MimeBodyPart();
	            
	            String baseDesign = "./..//REPORTES/"; 
	            String reporte = (tipoDoc.equals("FV") ? "Rpt_Factura.jrxml" : "") + 
	            (tipoDoc.equals("FC") ? "Rpt_Factura.jrxml" : "") + 
	            (tipoDoc.equals("NC") ? "Rpt_NotaCredito.jrxml" : "") + 
	            (tipoDoc.equals("RT") ? "Rpt_Retenciones.jrxml" : "") + 
	            (tipoDoc.equals("GR") ? "Rpt_Guia.jrxml" : "");
	            
	            String dirBaseDesign = baseDesign + reporte; 
	            
	            URL url = this.getClass().getClassLoader().getResource(dirBaseDesign);
	    		baseDesign = url.getPath().replace(reporte, "");
	    		
	    		if(baseDesign.indexOf(":") != -1){
	    			baseDesign = baseDesign.replaceFirst("/", "");
	    		}else if(baseDesign.indexOf("//") != -1){
	    			baseDesign = baseDesign.replaceFirst("//", "/");
	    		}
	            
	            File pdf = auxiliares.generarPDF(baseDesign, reporte, idDocumento, claveAcceso, connection);

	            adjunto2.setDataHandler(
		                new DataHandler(new FileDataSource(pdf.getPath())));
		            adjunto2.setFileName("documento - " + claveAcceso + ".pdf");
	            
	            MimeMultipart multiParte = new MimeMultipart();
	            multiParte.addBodyPart(texto);
	            multiParte.addBodyPart(adjunto);
	            multiParte.addBodyPart(adjunto2);
	            
	            MimeMessage message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(this.openbravo.getOcPocConfiguration().getSmtpserveracount()));
	            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correo));
	            message.setSubject(MimeUtility.encodeText(this.service.getAsunto(),"UTF-8","B"));
	            message.setContent(multiParte);
	            
	            Transport t = session.getTransport("smtp");
	            
				//conectando al email
				t.connect(this.openbravo.getOcPocConfiguration().getSmtpserveracount(), auxiliares.dencryptSh1(this.openbravo.getOcPocConfiguration().getSmtppassword()));
	            
				//enviando email
				t.sendMessage(message, message.getAllRecipients());
				
				//cerrando conexion
	            t.close();  log.info("Close BDD o Statement");
				
				return true;
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				log.error(ex.getMessage());
				return false;
			}
		}else{
			return false;
		}
	}
}
