package atrums.modelo.documento;

public class DatosInfo {
	private String identificacionComprador = null;
	private String fechaEmision = null;
	private String detalle = null;
	private String razonSocial = null;
	private String tipo = null;

	public DatosInfo() {
		super();
	}
	
	public DatosInfo(String identificacionComprador, 
			String fechaEmision, 
			String detalle, 
			String razonSocial, 
			String tipo){
		this.identificacionComprador = identificacionComprador;
		this.fechaEmision = fechaEmision;
		this.detalle = detalle;
		this.razonSocial = razonSocial;
		this.tipo = tipo;
	}
	
	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getIdentificacionComprador() {
		return identificacionComprador;
	}

	public void setIdentificacionComprador(String identificacionComprador) {
		this.identificacionComprador = identificacionComprador;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
}
