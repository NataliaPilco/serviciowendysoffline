package atrums.modelo.documento;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class InfoTributaria {
	private static Logger log = Logger.getLogger(InfoTributaria.class);
	private String ambiente;
	private String tipoEmision;
	private String razonSocial;
	private String nombreComercial;
	private String ruc;
	private String claveAcceso;
	private String codDoc;
	private String estab;
	private String ptoEmi;
	private String secuencial;
	private String dirMatriz;
	
	public InfoTributaria(String Documento){
		try {
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource source = new InputSource();
			source.setCharacterStream(new StringReader(Documento));
			Document doc = documentBuilder.parse(source);
			NodeList nodes = doc.getElementsByTagName("infoTributaria");
			
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				
				if(element.getElementsByTagName("ambiente").getLength() > 0){
					this.ambiente = element.getElementsByTagName("ambiente").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("tipoEmision").getLength() > 0){
					this.tipoEmision = element.getElementsByTagName("tipoEmision").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("razonSocial").getLength() > 0){
					this.razonSocial = element.getElementsByTagName("razonSocial").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("nombreComercial").getLength() > 0){
					this.nombreComercial = element.getElementsByTagName("nombreComercial").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("ruc").getLength() > 0){
					this.ruc= element.getElementsByTagName("ruc").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("claveAcceso").getLength() > 0){
					this.claveAcceso = element.getElementsByTagName("claveAcceso").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("codDoc").getLength() > 0){
					this.codDoc = element.getElementsByTagName("codDoc").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("estab").getLength() > 0){
					this.estab = element.getElementsByTagName("estab").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("ptoEmi").getLength() > 0){
					this.ptoEmi = element.getElementsByTagName("ptoEmi").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("secuencial").getLength() > 0){
					this.secuencial = element.getElementsByTagName("secuencial").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("dirMatriz").getLength() > 0){
					this.dirMatriz = element.getElementsByTagName("dirMatriz").item(0).getFirstChild().getNodeValue();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}
	
	public String getAmbiente() {
		return ambiente;
	}
	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}
	public String getTipoEmision() {
		return tipoEmision;
	}
	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getClaveAcceso() {
		return claveAcceso;
	}
	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}
	public String getCodDoc() {
		return codDoc;
	}
	public void setCodDoc(String codDoc) {
		this.codDoc = codDoc;
	}
	public String getEstab() {
		return estab;
	}
	public void setEstab(String estab) {
		this.estab = estab;
	}
	public String getPtoEmi() {
		return ptoEmi;
	}
	public void setPtoEmi(String ptoEmi) {
		this.ptoEmi = ptoEmi;
	}
	public String getSecuencial() {
		return secuencial;
	}
	public void setSecuencial(String secuencial) {
		this.secuencial = secuencial;
	}
	public String getDirMatriz() {
		return dirMatriz;
	}
	public void setDirMatriz(String dirMatriz) {
		this.dirMatriz = dirMatriz;
	}
}