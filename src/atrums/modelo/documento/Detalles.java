package atrums.modelo.documento;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class Detalles {
	private static Logger log = Logger.getLogger(Detalles.class);
	private List<Detalle> detalles;
	
	public Detalles(String Documento){
		try {
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource source = new InputSource();
			source.setCharacterStream(new StringReader(Documento));
			Document doc = documentBuilder.parse(source);
			NodeList nodes = doc.getElementsByTagName("detalles");
			List<Detalle> auxDetalles = new ArrayList<Detalle>();
			
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				if(element.getElementsByTagName("detalle").getLength() > 0){
					NodeList nodess = element.getElementsByTagName("detalle");
					for (int k = 0; k < nodess.getLength(); k++) {
						Detalle detalle = new Detalle((Element) nodess.item(k));
						auxDetalles.add(detalle);
					}
				}
			}
			this.detalles = auxDetalles;
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}

	public List<Detalle> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<Detalle> detalles) {
		this.detalles = detalles;
	}
}