/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class ConfService {
    static final Logger log = Logger.getLogger(ConfService.class);
    private static InputStream confService = null;
    private Properties propiedad = new Properties();
    
    private boolean activar;
    
    private String ipService;
    private String puertoService;
    private String wSDL;
    private String consultarFechasFactura;
    private String consultarResumenFacturaPorFecha;
    private String consultarFacturaPorDocumento;
    private String consultarResumenFacturasPaginadasPorFecha;
    private String consultarResumenTotalFacturaPorFecha;
    
    private String consultarFechasNotaCredito;
    private String consultarResumenNotaCreditoPorFecha;
    private String consultarNotaCreditoPorDocumento;
    private String consultarResumenNotaCreditoPaginadasPorFecha;
    private String consultarResumenTotalNotaCreditoPorFecha;
    
    private String consultarFechasGuiaRemision;
    private String consultarGuiaRemisionPorDocumento;
    private String consultarResumenGuiasRemisionPaginadasPorFecha; 
    private String consultarResumenTotalGuiaRemisionPorFecha;
    
    private String consultarFechasRetencion;
    private String consultarResumenRetencionPorFecha;
    private String consultarRetencionPorDocumento;
    private String consultarDetalleRetenciones;
    
    private String consultarClientePorIdentificacion;
    private String consultarDireccionClientePorIdentificacion;
    private String consultarCorreoClientePorIdentificacion;
    
    private String insertarFEControlDocumentos;
    
    private boolean enviarCorreo;
    private String asunto;
    private String cuerpoCorreo;
    
    private String consultarClavesAccesoFacturas;
    private String consultarFechasClave;
    private String consultarResumenTotalClavesPorFecha;
    private String consultarResumenClavesPaginadasPorFecha;
    
	private String razonSocial;
    private String nombreComercial;
    private String ruc;
    private String direccion;
    private String ambiente;
    private boolean usarDatos = false;
    private String codNumerico;
    private String numResolucion;
    private boolean obliContabi = false;
    private String email;
    private String webConsulta;
    
    private String direccionFirma;
    private String passwordFirma;
    
    private String pruebasRt;
    private String pruebasAt;
    private String produccionRt;
    private String produccionAt;
    private String nameSpaceRt;
    private String nameSpaceAt;
    
    public ConfService(){
        try {
            confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/ConfServiceWeb.xml");
            propiedad.loadFromXML(confService);
            
            this.activar = propiedad.getProperty("Activar").equals("Y") ? true : false;
            
            //Datos del Servicio
            this.ipService = propiedad.getProperty("IpService");
            this.puertoService = propiedad.getProperty("PuertoService");
            this.wSDL = propiedad.getProperty("WSDL");
            
            //Configuracion de Servicios//
            this.consultarFechasFactura = propiedad.getProperty("ConsultarFechasFactura");
            this.consultarResumenFacturaPorFecha = propiedad.getProperty("ConsultarResumenFacturaPorFecha");
            this.consultarFacturaPorDocumento = propiedad.getProperty("ConsultarFacturaPorDocumento");
            this.consultarResumenFacturasPaginadasPorFecha = propiedad.getProperty("ConsultarResumenFacturasPaginadasPorFecha");
            this.consultarResumenTotalFacturaPorFecha = propiedad.getProperty("ConsultarResumenTotalFacturaPorFecha");
            
            this.consultarFechasNotaCredito = propiedad.getProperty("ConsultarFechasNotaCredito");
            this.consultarResumenNotaCreditoPorFecha = propiedad.getProperty("ConsultarResumenNotaCreditoPorFecha");
            this.consultarNotaCreditoPorDocumento = propiedad.getProperty("ConsultarNotaCreditoPorDocumento");
            this.consultarResumenNotaCreditoPaginadasPorFecha = propiedad.getProperty("ConsultarResumenNotaCreditoPaginadasPorFecha");
            this.consultarResumenTotalNotaCreditoPorFecha = propiedad.getProperty("ConsultarResumenTotalNotaCreditoPorFecha");
            
            this.consultarFechasGuiaRemision = propiedad.getProperty("ConsultarFechasGuiaRemision");
            this.consultarGuiaRemisionPorDocumento = propiedad.getProperty("ConsultarGuiaRemisionPorDocumento");
            this.consultarResumenGuiasRemisionPaginadasPorFecha = propiedad.getProperty("ConsultarResumenGuiaRemisionPaginadasPorFecha");
            this.consultarResumenTotalGuiaRemisionPorFecha = propiedad.getProperty("ConsultarResumenTotalGuiaRemisionPorFecha");
            
            this.consultarFechasRetencion = propiedad.getProperty("ConsultarFechasRetencion");
            this.consultarResumenRetencionPorFecha = propiedad.getProperty("ConsultarResumenRetencionPorFecha");
            this.consultarRetencionPorDocumento = propiedad.getProperty("ConsultarRetencionPorDocumento");
            this.consultarDetalleRetenciones = propiedad.getProperty("ConsultarDetalleRetenciones");
            
            this.consultarClientePorIdentificacion = propiedad.getProperty("ConsultarClientePorIdentificacion");
            this.consultarDireccionClientePorIdentificacion = propiedad.getProperty("ConsultarDireccionClientePorIdentificacion");
            this.consultarCorreoClientePorIdentificacion = propiedad.getProperty("ConsultarCorreoClientePorIdentificacion");
            
            this.consultarClavesAccesoFacturas = propiedad.getProperty("ConsultarClavesAccesoFacturas");
            this.consultarFechasClave = propiedad.getProperty("ConsultarFechasClave");
            this.consultarResumenTotalClavesPorFecha = propiedad.getProperty("ConsultarResumenTotalClavesPorFecha");
            this.consultarResumenClavesPaginadasPorFecha = propiedad.getProperty("ConsultarResumenClavesPaginadasPorFecha");
            
            this.enviarCorreo = propiedad.getProperty("EnviarCorreo").equals("Y") ? true : false;
            this.asunto = propiedad.getProperty("Asunto");
            this.cuerpoCorreo = propiedad.getProperty("CuerpoCorreo");
            
            this.insertarFEControlDocumentos = propiedad.getProperty("InsertarFEControlDocumentos");
            
            //Datos de ambiente
            this.razonSocial = propiedad.getProperty("RazonSocial");
            this.nombreComercial = propiedad.getProperty("NombreComercial");
            this.ruc = propiedad.getProperty("Ruc");
            this.direccion = propiedad.getProperty("Direccion");
            this.ambiente = propiedad.getProperty("Ambiente");
            this.usarDatos = propiedad.getProperty("UsarDatos").equals("Y") ? true : false;
            this.codNumerico = propiedad.getProperty("CodNumerico");
            this.numResolucion = propiedad.getProperty("NumResolucion");
            this.obliContabi = propiedad.getProperty("ObliContabi").equals("Y") ? true : false;
            
            this.email = propiedad.getProperty("Email").equals("") ? null : propiedad.getProperty("Email");
            this.webConsulta = propiedad.getProperty("WebConsulta").equals("") ? null : propiedad.getProperty("WebConsulta");
            
            this.direccionFirma = propiedad.getProperty("DireccionFirma").equals("") ? null : propiedad.getProperty("DireccionFirma");
            this.passwordFirma = propiedad.getProperty("PasswordFirma").equals("") ? null : propiedad.getProperty("PasswordFirma");
            

        	this.pruebasRt = propiedad.getProperty("PruebasRt");
        	this.pruebasAt = propiedad.getProperty("PruebasAt");
        	this.produccionRt = propiedad.getProperty("ProduccionRt");
        	this.produccionAt = propiedad.getProperty("ProduccionAt");
        	this.nameSpaceRt = propiedad.getProperty("NameSpaceRt");
        	this.nameSpaceAt = propiedad.getProperty("NameSpaceAt");
        	
        	confService.close();  log.info("Close BDD o Statement");
        } catch (IOException ex) {
            log.error(ex.getStackTrace());
        }
    }

	public String getConsultarResumenGuiasRemisionPaginadasPorFecha() {
		return consultarResumenGuiasRemisionPaginadasPorFecha;
	}

	public void setConsultarResumenGuiasRemisionPaginadasPorFecha(
			String consultarResumenGuiasRemisionPaginadasPorFecha) {
		this.consultarResumenGuiasRemisionPaginadasPorFecha = consultarResumenGuiasRemisionPaginadasPorFecha;
	}

	public String getConsultarGuiaRemisionPorDocumento() {
		return consultarGuiaRemisionPorDocumento;
	}



	public void setConsultarGuiaRemisionPorDocumento(
			String consultarGuiaRemisionPorDocumento) {
		this.consultarGuiaRemisionPorDocumento = consultarGuiaRemisionPorDocumento;
	}

	public String getConsultarResumenTotalGuiaRemisionPorFecha() {
		return consultarResumenTotalGuiaRemisionPorFecha;
	}

	public void setConsultarResumenTotalGuiaRemisionPorFecha(
			String consultarResumenTotalGuiaRemisionPorFecha) {
		this.consultarResumenTotalGuiaRemisionPorFecha = consultarResumenTotalGuiaRemisionPorFecha;
	}

	public String getConsultarFechasGuiaRemision() {
		return consultarFechasGuiaRemision;
	}

	public void setConsultarFechasGuiaRemision(String consultarFechasGuiaRemision) {
		this.consultarFechasGuiaRemision = consultarFechasGuiaRemision;
	}

	public String getConsultarResumenClavesPaginadasPorFecha() {
		return consultarResumenClavesPaginadasPorFecha;
	}

	public void setConsultarResumenClavesPaginadasPorFecha(
			String consultarResumenClavesPaginadasPorFecha) {
		this.consultarResumenClavesPaginadasPorFecha = consultarResumenClavesPaginadasPorFecha;
	}

	public String getConsultarResumenTotalClavesPorFecha() {
		return consultarResumenTotalClavesPorFecha;
	}

	public void setConsultarResumenTotalClavesPorFecha(
			String consultarResumenTotalClavesPorFecha) {
		this.consultarResumenTotalClavesPorFecha = consultarResumenTotalClavesPorFecha;
	}

	public String getConsultarFechasClave() {
		return consultarFechasClave;
	}

	public void setConsultarFechasClave(String consultarFechasClave) {
		this.consultarFechasClave = consultarFechasClave;
	}

	public String getConsultarResumenFacturasPaginadasPorFecha() {
		return consultarResumenFacturasPaginadasPorFecha;
	}

	public void setConsultarResumenFacturasPaginadasPorFecha(
			String consultarResumenFacturasPaginadasPorFecha) {
		this.consultarResumenFacturasPaginadasPorFecha = consultarResumenFacturasPaginadasPorFecha;
	}

	public String getConsultarResumenTotalFacturaPorFecha() {
		return consultarResumenTotalFacturaPorFecha;
	}

	public void setConsultarResumenTotalFacturaPorFecha(
			String consultarResumenTotalFacturaPorFecha) {
		this.consultarResumenTotalFacturaPorFecha = consultarResumenTotalFacturaPorFecha;
	}

	public String getConsultarResumenNotaCreditoPaginadasPorFecha() {
		return consultarResumenNotaCreditoPaginadasPorFecha;
	}

	public void setConsultarResumenNotaCreditoPaginadasPorFecha(
			String consultarResumenNotaCreditoPaginadasPorFecha) {
		this.consultarResumenNotaCreditoPaginadasPorFecha = consultarResumenNotaCreditoPaginadasPorFecha;
	}

	public String getConsultarResumenTotalNotaCreditoPorFecha() {
		return consultarResumenTotalNotaCreditoPorFecha;
	}

	public void setConsultarResumenTotalNotaCreditoPorFecha(
			String consultarResumenTotalNotaCreditoPorFecha) {
		this.consultarResumenTotalNotaCreditoPorFecha = consultarResumenTotalNotaCreditoPorFecha;
	}

	public boolean isEnviarCorreo() {
		return enviarCorreo;
	}

	public void setEnviarCorreo(boolean enviarCorreo) {
		this.enviarCorreo = enviarCorreo;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getCuerpoCorreo() {
		return cuerpoCorreo;
	}

	public void setCuerpoCorreo(String cuerpoCorreo) {
		this.cuerpoCorreo = cuerpoCorreo;
	}

	public String getConsultarClavesAccesoFacturas() {
		return consultarClavesAccesoFacturas;
	}

	public void setConsultarClavesAccesoFacturas(
			String consultarClavesAccesoFacturas) {
		this.consultarClavesAccesoFacturas = consultarClavesAccesoFacturas;
	}

	public String getConsultarDetalleRetenciones() {
		return consultarDetalleRetenciones;
	}

	public void setConsultarDetalleRetenciones(String consultarDetalleRetenciones) {
		this.consultarDetalleRetenciones = consultarDetalleRetenciones;
	}

	public String getConsultarRetencionPorDocumento() {
		return consultarRetencionPorDocumento;
	}

	public void setConsultarRetencionPorDocumento(
			String consultarRetencionPorDocumento) {
		this.consultarRetencionPorDocumento = consultarRetencionPorDocumento;
	}

	public String getConsultarResumenRetencionPorFecha() {
		return consultarResumenRetencionPorFecha;
	}

	public void setConsultarResumenRetencionPorFecha(
			String consultarResumenRetencionPorFecha) {
		this.consultarResumenRetencionPorFecha = consultarResumenRetencionPorFecha;
	}

	public String getConsultarNotaCreditoPorDocumento() {
		return consultarNotaCreditoPorDocumento;
	}

	public void setConsultarNotaCreditoPorDocumento(
			String consultarNotaCreditoPorDocumento) {
		this.consultarNotaCreditoPorDocumento = consultarNotaCreditoPorDocumento;
	}

	public String getConsultarFechasRetencion() {
		return consultarFechasRetencion;
	}

	public void setConsultarFechasRetencion(String consultarFechasRetencion) {
		this.consultarFechasRetencion = consultarFechasRetencion;
	}

	public String getConsultarResumenNotaCreditoPorFecha() {
		return consultarResumenNotaCreditoPorFecha;
	}

	public void setConsultarResumenNotaCreditoPorFecha(
			String consultarResumenNotaCreditoPorFecha) {
		this.consultarResumenNotaCreditoPorFecha = consultarResumenNotaCreditoPorFecha;
	}

	public String getConsultarFechasNotaCredito() {
		return consultarFechasNotaCredito;
	}

	public void setConsultarFechasNotaCredito(String consultarFechasNotaCredito) {
		this.consultarFechasNotaCredito = consultarFechasNotaCredito;
	}

	public boolean isActivar() {
		return activar;
	}

	public void setActivar(boolean activar) {
		this.activar = activar;
	}

	public String getInsertarFEControlDocumentos() {
		return insertarFEControlDocumentos;
	}

	public void setInsertarFEControlDocumentos(String insertarFEControlDocumentos) {
		this.insertarFEControlDocumentos = insertarFEControlDocumentos;
	}

	public String getConsultarCorreoClientePorIdentificacion() {
		return consultarCorreoClientePorIdentificacion;
	}

	public void setConsultarCorreoClientePorIdentificacion(
			String consultarCorreoClientePorIdentificacion) {
		this.consultarCorreoClientePorIdentificacion = consultarCorreoClientePorIdentificacion;
	}

	public String getConsultarDireccionClientePorIdentificacion() {
		return consultarDireccionClientePorIdentificacion;
	}

	public void setConsultarDireccionClientePorIdentificacion(
			String consultarDireccionClientePorIdentificacion) {
		this.consultarDireccionClientePorIdentificacion = consultarDireccionClientePorIdentificacion;
	}
    
    public String getConsultarClientePorIdentificacion() {
		return consultarClientePorIdentificacion;
	}

	public void setConsultarClientePorIdentificacion(
			String consultarClientePorIdentificacion) {
		this.consultarClientePorIdentificacion = consultarClientePorIdentificacion;
	}

	public String getNameSpaceRt() {
		return nameSpaceRt;
	}

	public void setNameSpaceRt(String nameSpaceRt) {
		this.nameSpaceRt = nameSpaceRt;
	}

	public String getNameSpaceAt() {
		return nameSpaceAt;
	}

	public void setNameSpaceAt(String nameSpaceAt) {
		this.nameSpaceAt = nameSpaceAt;
	}

	public String getPruebasRt() {
		return pruebasRt;
	}

	public void setPruebasRt(String pruebasRt) {
		this.pruebasRt = pruebasRt;
	}

	public String getPruebasAt() {
		return pruebasAt;
	}

	public void setPruebasAt(String pruebasAt) {
		this.pruebasAt = pruebasAt;
	}

	public String getProduccionRt() {
		return produccionRt;
	}

	public void setProduccionRt(String produccionRt) {
		this.produccionRt = produccionRt;
	}

	public String getProduccionAt() {
		return produccionAt;
	}

	public void setProduccionAt(String produccionAt) {
		this.produccionAt = produccionAt;
	}

	public String getDireccionFirma() {
		return direccionFirma;
	}

	public void setDireccionFirma(String direccionFirma) {
		this.direccionFirma = direccionFirma;
	}

	public String getPasswordFirma() {
		return passwordFirma;
	}

	public void setPasswordFirma(String passwordFirma) {
		this.passwordFirma = passwordFirma;
	}

	public String getWebConsulta() {
		return webConsulta;
	}

	public void setWebConsulta(String webConsulta) {
		this.webConsulta = webConsulta;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isObliContabi() {
		return obliContabi;
	}

	public void setObliContabi(boolean obliContabi) {
		this.obliContabi = obliContabi;
	}

	public String getNumResolucion() {
		return numResolucion;
	}

	public void setNumResolucion(String numResolucion) {
		this.numResolucion = numResolucion;
	}

	public String getCodNumerico() {
		return codNumerico;
	}

	public void setCodNumerico(String codNumerico) {
		this.codNumerico = codNumerico;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public boolean isUsarDatos() {
		return usarDatos;
	}

	public void setUsarDatos(boolean usarDatos) {
		this.usarDatos = usarDatos;
	}

	public String getConsultarFacturaPorDocumento() {
		return consultarFacturaPorDocumento;
	}

	public void setConsultarFacturaPorDocumento(String consultarFacturaPorDocumento) {
		this.consultarFacturaPorDocumento = consultarFacturaPorDocumento;
	}

	public String getConsultarResumenFacturaPorFecha() {
		return consultarResumenFacturaPorFecha;
	}

	public void setConsultarResumenFacturaPorFecha(String consultarResumenFacturaPorFecha) {
		this.consultarResumenFacturaPorFecha = consultarResumenFacturaPorFecha;
	}

    public String getIpService() {
        return ipService;
    }

    public void setIpService(String ipService) {
        this.ipService = ipService;
    }

    public String getPuertoService() {
        return puertoService;
    }

    public void setPuertoService(String puertoService) {
        this.puertoService = puertoService;
    }

    public String getConsultarFechasFactura() {
        return consultarFechasFactura;
    }

    public void setConsultarFechasFactura(String consultarFechasFactura) {
        this.consultarFechasFactura = consultarFechasFactura;
    }

    public String getwSDL() {
        return wSDL;
    }

    public void setwSDL(String wSDL) {
        this.wSDL = wSDL;
    }
}
