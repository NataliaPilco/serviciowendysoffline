package atrums.modelo.facesBean;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.modelo.Servicio.ServDocumentoSucNumFNumDocProve;
import atrums.persistencia.OperacionConexion;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceClavePorFechaPagina;
import atrums.persistencia.ServiceFacturaPorFechaPagina;
import atrums.persistencia.ServiceNotaCreditoPorFechaPagina;
import atrums.persistencia.ServiceNroPaginasClaves;
import atrums.persistencia.ServiceNroPaginasFacturas;
import atrums.persistencia.ServiceNroPaginasNotas;
import atrums.persistencia.ServiceRetencionPorFecha;

public class LoginBean {
	static final Logger log = Logger.getLogger(LoginBean.class);
	private DataSource dataSourceOpenbravo = OperacionConexion.getDataSource();
	private ConfService confService;
	
	private String nombre;
	private String clave;
	
	private Date fechafactura;
	private Date fechafacturaclave;
	private Date fechanota;
	private Date fecharetencion;
	
	private boolean logeado = false;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public boolean isLogeado() {
		return logeado;
	}
	public void setLogeado(boolean logeado) {
		this.logeado = logeado;
	}
	public Date getFechafactura() {
		return fechafactura;
	}
	public void setFechafactura(Date fechafactura) {
		this.fechafactura = fechafactura;
	}
	public Date getFechafacturaclave() {
		return fechafacturaclave;
	}
	public void setFechafacturaclave(Date fechafacturaclave) {
		this.fechafacturaclave = fechafacturaclave;
	}
	public Date getFechanota() {
		return fechanota;
	}
	public void setFechanota(Date fechanota) {
		this.fechanota = fechanota;
	}
	public Date getFecharetencion() {
		return fecharetencion;
	}
	public void setFecharetencion(Date fecharetencion) {
		this.fecharetencion = fecharetencion;
	}
	
	public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Fecha Seleccionada", format.format(event.getObject())));
    }
	
	public void login(ActionEvent actionEvent) {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage msg = null;
		OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
		String auxClave = "";
		
		try {
			auxClave = auxiliares.encryptSh1(this.clave);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);;
		}
		
		Connection conHome = null;
		
		try {
			OperacionesBDDHome home = new OperacionesBDDHome();
			conHome = home.getConneccion(dataSourceOpenbravo);
			
			this.logeado = home.loginUser(this.nombre, auxClave, conHome);
			
			this.logeado = true;
			
			if (this.logeado) {
				msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenid@", this.nombre);
			} else {
				msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Credenciales no válidas");
			}
		} finally {
			try { if (conHome != null) conHome.close();  log.info("Close BDD o Statement"); conHome = null;} catch (Exception ex) {};
		}
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
		context.addCallbackParam("estaLogeado", logeado);
		
		if (logeado)
			context.addCallbackParam("view", "operaciones.xhtml");
	} 
	
	public void logout() {
		RequestContext context = RequestContext.getCurrentInstance();
		FacesMessage msg = null;
		
		msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Salir", "Cerrando Sesión");
		FacesContext.getCurrentInstance().addMessage(null, msg);
		
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		session.invalidate();
		logeado = false;
		
		context.addCallbackParam("view", "index.xhtml");
	}
	
	public void monitorarFactura() {
		FacesMessage msg = null;
		OperacionesBDDHome home = new OperacionesBDDHome();
		this.confService = new ConfService();
		
		if (this.fechafactura != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			String fecha = df.format(this.fechafactura);
			
			Connection connection = null;
			
			try {
				log.info("Conectando a la bdd");
        		connection = home.getConneccion(this.dataSourceOpenbravo);
        		
        		log.info("Verificando si existe conexion a la bdd");
        		if(connection != null){
        			ServiceNroPaginasFacturas serviceNroPaginasFacturas = new ServiceNroPaginasFacturas(this.confService, fecha);
                	int numeroPaginas = serviceNroPaginasFacturas.callDocumentos();
                	log.info("Monitoreando Fecha Factura: " + fecha);
                	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
                    
                	List<ServDocumentoNroNroEsNroEm> auxDoc = new ArrayList<ServDocumentoNroNroEsNroEm>();
                	
                	for(int i=1; i <= numeroPaginas; i++){
                		ServiceFacturaPorFechaPagina serviceFacturaPorFechaPagina = new ServiceFacturaPorFechaPagina(this.confService, fecha, String.valueOf(i));
                		List<ServDocumentoNroNroEsNroEm> auxDoc1 = serviceFacturaPorFechaPagina.callDocumentos();
                		
                		if(!auxDoc1.isEmpty()){
                			auxDoc.addAll(auxDoc1);
                		}
                		auxDoc1.clear();
                	}
                	
                	log.info("Documento por pagina: " + auxDoc.size());
            		
                	/**Codigo Auxiliar**/
                    for(ServDocumentoNroNroEsNroEm datodoc: auxDoc){
                    	BDDDocumentoBase documentoBase = new BDDDocumentoBase(
                    			datodoc.getNroDocumento(), 
                    			datodoc.getNroEstablecimiento(), 
                    			datodoc.getNroEmision(), 
                    			datodoc.getTipoDoc(), 
                    			datodoc.getFechadocumento());
                		home.setDocumentoBase(documentoBase);
                		
                		try{
                    		if(home.guardarDocumento(connection)){
                    			home.saveLogServicio(connection, "Preparando documento factura de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
                    			connection.commit();
                    		}else{
                    			connection.rollback(); log.info("Rollback Proceso");
                    		}
                    	} catch (SQLException ex) {
                    		log.error(ex.getMessage());
                    	}
                    }
                    
                    auxDoc.clear();
                    auxDoc = null;
        		}
			} finally {
				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
			}
			
			log.info("Re-Monitoreando fecha: " + fecha);
			
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se Re-Monitoreo la fecha: " + fecha);
		} else {
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "No hay fecha para monitorear");
		}
		
		this.fechafactura = null;
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public void monitorarFacturaClave() {
		FacesMessage msg = null;
		OperacionesBDDHome home = new OperacionesBDDHome();
		this.confService = new ConfService();
		
		if (this.fechafacturaclave != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			String fecha = df.format(this.fechafacturaclave);
			
			Connection connection = null;
			
			try {
				log.info("Conectando a la bdd");
        		connection = home.getConneccion(this.dataSourceOpenbravo);
        		
        		log.info("Verificando si existe conexion a la bdd");
        		if(connection != null){
        			ServiceNroPaginasClaves serviceNroPaginasClaves = new ServiceNroPaginasClaves(this.confService, fecha);
                	int numeroPaginas = serviceNroPaginasClaves.callDocumentos();
                	log.info("Monitoreando Fecha Clave: " + fecha);
                	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
                	
                	for(int i=1; i <= numeroPaginas; i++){
                		ServiceClavePorFechaPagina serviceClavePorFechaPagina = new ServiceClavePorFechaPagina(confService, fecha, String.valueOf(i));
                		List<String> auxClaves = serviceClavePorFechaPagina.callClaves();
                		
                		try{
                			log.info("Nro de claves: " + auxClaves.size());
                			
                			for(String clave: auxClaves){
                				DateFormat format = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH);
                    			Date fechaDoc = format.parse(clave.substring(0,8));
                    			SimpleDateFormat sdfFormato = new SimpleDateFormat("yyyy-MM-dd");
                            	
                            	BDDDocumentoBase documentoBase = new BDDDocumentoBase(
                            			clave.substring(30,39), 
                            			clave.substring(24, 27), 
                            			clave.substring(27, 30),
                            			(clave.substring(8,10).equals("01") ? "FCP":"") + 
                            			(clave.substring(8,10).equals("04") ? "NCP":"") + 
                            			(clave.substring(8,10).equals("07") ? "RTP":""), 
                            			sdfFormato.format(fechaDoc));
                            	home.setDocumentoBase(documentoBase);
                            	
                            	if(home.guardarDocumento(connection)){
                        			documentoBase.setClaveacceso(clave);
                        			documentoBase.setEstado("AUT");
                        			home.setDocumentoBase(documentoBase);
                        			if(home.actualizarEstado(connection)){
                        				connection.commit();
                        			}else{
                        				connection.rollback(); log.info("Rollback Proceso");
                        			}
                        		}else{
                        			connection.rollback(); log.info("Rollback Proceso");
                        		}
                            	
                            	documentoBase = null;
                    		}
                			
                			auxClaves.clear();
                			auxClaves = null;
            			} catch (Exception ex) {
            				// TODO Auto-generated catch block
            				log.error(ex.getMessage());
            			} finally {
            				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
            			}
                	}
        		}
			} finally {
				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
			}
			
			log.info("Re-Monitoreando fecha: " + fecha);
			
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se Re-Monitoreo la fecha: " + fecha);
		} else {
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "No hay fecha para monitorear");
		}
		
		this.fechafacturaclave = null;
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public void monitorarNota() {
		FacesMessage msg = null;
		OperacionesBDDHome home = new OperacionesBDDHome();
		this.confService = new ConfService();
		
		if (this.fechanota != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			String fecha = df.format(this.fechanota);
			
			Connection connection = null;
			
			try {
				log.info("Conectando a la bdd");
        		connection = home.getConneccion(this.dataSourceOpenbravo);
        		
        		log.info("Verificando si existe conexion a la bdd");
        		if(connection != null){
        			ServiceNroPaginasNotas serviceNroPaginasNotas = new ServiceNroPaginasNotas(confService, fecha);
    	        	int numeroPaginas = serviceNroPaginasNotas.callDocumentos();
    	        	log.info("Monitoreando Fecha Nota de Credito: " + fecha);
    	        	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
    	        	
    	        	for(int i=1; i <= numeroPaginas; i++){
    	        		ServiceNotaCreditoPorFechaPagina serviceNotaCreditoPorFechaPagina = new ServiceNotaCreditoPorFechaPagina(confService, fecha, String.valueOf(i));
    	        		List<ServDocumentoNroNroEsNroEm> auxDoc = serviceNotaCreditoPorFechaPagina.callDocumentos();
    	        		
    	        		for(ServDocumentoNroNroEsNroEm datodoc: auxDoc){
    	        			BDDDocumentoBase documentoBase = new BDDDocumentoBase(
	                    			datodoc.getNroDocumento(), 
	                    			datodoc.getNroEstablecimiento(), 
	                    			datodoc.getNroEmision(), 
	                    			datodoc.getTipoDoc(), 
	                    			datodoc.getFechadocumento());
	                    	home.setDocumentoBase(documentoBase);
	                    	
	                    	try{
                        		if(home.guardarDocumento(connection)){
                        			home.saveLogServicio(connection, "Preparando documento nota de credito de la fecha: " + fecha 
                        					+ " - documento: " + documentoBase.getNrodocumento() 
                        					+ " - ptoemision: " + documentoBase.getNroemision()
                        					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
                        			log.info("Ingresando documento nota de credito de la fecha: " + fecha 
                        					+ " - documento: " + documentoBase.getNrodocumento() 
                        					+ " - ptoemision: " + documentoBase.getNroemision()
                        					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento());
                        			connection.commit();
                        		}else{
                        			connection.rollback(); log.info("Rollback Proceso");
                        		}
                        	} catch (SQLException ex) {
                        		log.error(ex.getMessage(), ex);
            				}
    	            	}
    	        		
    	        		auxDoc.clear();
    	        		auxDoc = null;
    	        	}                    
        		}
			} finally {
				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
			}
			
			log.info("Re-Monitoreando fecha: " + fecha);
			
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se Re-Monitoreo la fecha: " + fecha);
		} else {
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "No hay fecha para monitorear");
		}
		
		this.fechanota = null;
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	
	public void monitorarRetencion() {
		FacesMessage msg = null;
		OperacionesBDDHome home = new OperacionesBDDHome();
		this.confService = new ConfService();
		
		if (this.fecharetencion != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			String fecha = df.format(this.fecharetencion);
			
			Connection connection = null;
			
			try {
				log.info("Conectando a la bdd");
        		connection = home.getConneccion(this.dataSourceOpenbravo);
        		
        		log.info("Verificando si existe conexion a la bdd");
        		if(connection != null){
        			ServiceRetencionPorFecha serviceRetencionPorFecha = new ServiceRetencionPorFecha(confService, fecha);
    				List<ServDocumentoSucNumFNumDocProve> auxDoc = serviceRetencionPorFecha.callDocumentos();
    				for(ServDocumentoSucNumFNumDocProve datodoc: auxDoc){
    					BDDDocumentoBase documentoBase = new BDDDocumentoBase(
								datodoc.getNroDocumento(), 
				        		datodoc.getSucursal(), 
				        		datodoc.getNroFactura(), 
				        		"RT", 
				        		fecha);
    					home.setDocumentoBase(documentoBase);
    					
    					try{
	                		if(home.guardarDocumento(connection)){
	                			home.saveLogServicio(connection, "Preparando documento retencions de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
	                			log.info("Ingresando documento retención de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento());
	                			connection.commit();
	                		}else{
	                			connection.rollback(); log.info("Rollback Proceso");
	                		}
	                	} catch (SQLException ex) {
	                		log.error(ex.getMessage());
                    		connection = home.getConneccion(dataSourceOpenbravo);
	    				}
    				}
    				
    				auxDoc.clear();
    				auxDoc = null;
        		}
			} finally {
				try { if (connection != null) connection.close();  log.info("Close BDD o Statement"); connection = null;} catch (Exception ex) {};
			}
			
			log.info("Re-Monitoreando fecha: " + fecha);
			
			msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se Re-Monitoreo la fecha: " + fecha);
		} else {
			msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "No hay fecha para monitorear");
		}
		
		this.fecharetencion = null;
		
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
}
