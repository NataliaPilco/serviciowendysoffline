package atrums.modelo.SRI;

import java.io.File;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;

import atrums.persistencia.OperacionesAuxiliares;

public class SRIDocumentoAutorizado {
	static final Logger log = Logger.getLogger(SRIDocumentoAutorizado.class);
	private String estado = null;
	private String estadoespecifico = null;
	private String mensaje = null;
	private String informacion = null;
	private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
	private String numeroAutorizacion = null;
	private String fechaAutorizacion = null;
	private String ambiente = null;
	private String docXML = null;
	private File docFile = null;
	
	public SRIDocumentoAutorizado(){
		super();
	}
	
	public SRIDocumentoAutorizado(SOAPMessage soapMessage){
		try {
			if(soapMessage != null){
				if(soapMessage.getSOAPBody().getElementsByTagName("numeroComprobantes").getLength() > 0){
					if(Integer.valueOf(soapMessage.getSOAPBody().getElementsByTagName("numeroComprobantes").item(0).getFirstChild().getNodeValue()) > 0){
						if(soapMessage.getSOAPBody().getElementsByTagName("estado").getLength() > 0){
							this.estado = soapMessage.getSOAPBody().getElementsByTagName("estado").item(0).getFirstChild().getNodeValue();
							this.estadoespecifico = this.estado.replace(" ", "").substring(0, 3);
						}
						
						if(soapMessage.getSOAPBody().getElementsByTagName("mensaje").getLength() > 0){
							this.mensaje = soapMessage.getSOAPBody().getElementsByTagName("mensaje").item(1).getFirstChild().getNodeValue();
						}
						

						if(soapMessage.getSOAPBody().getElementsByTagName("informacionAdicional").getLength() > 0){
							this.informacion = soapMessage.getSOAPBody().getElementsByTagName("informacionAdicional").item(0).getFirstChild().getNodeValue(); 
						}

						if(soapMessage.getSOAPBody().getElementsByTagName("numeroAutorizacion").getLength() > 0){
							this.numeroAutorizacion = soapMessage.getSOAPBody().getElementsByTagName("numeroAutorizacion").item(0).getFirstChild().getNodeValue();
						}
						
						if(soapMessage.getSOAPBody().getElementsByTagName("fechaAutorizacion").getLength() > 0){
							this.fechaAutorizacion = soapMessage.getSOAPBody().getElementsByTagName("fechaAutorizacion").item(0).getFirstChild().getNodeValue();
						}
						
						if(soapMessage.getSOAPBody().getElementsByTagName("ambiente").getLength() > 0){
							this.ambiente = soapMessage.getSOAPBody().getElementsByTagName("ambiente").item(0).getFirstChild().getNodeValue();
						}
						
						if(this.estado != null && 
								this.numeroAutorizacion != null &&
								this.fechaAutorizacion != null && 
								this.ambiente != null){
							if(soapMessage.getSOAPBody().getElementsByTagName("comprobante").getLength() > 0){
								this.docXML = auxiliares.stringtostring64Au(soapMessage.getSOAPBody().getElementsByTagName("comprobante").item(0).getFirstChild().getNodeValue(),
						                  this.estado,
						                  this.numeroAutorizacion,
						                  this.fechaAutorizacion,
						                  this.ambiente);
								
								this.docFile = auxiliares.stringtoDocumento(soapMessage.getSOAPBody().getElementsByTagName("comprobante").item(0).getFirstChild().getNodeValue(),
						                  this.estado,
						                  this.numeroAutorizacion,
						                  this.fechaAutorizacion,
						                  this.ambiente);
							}
						}
					}else{
						this.estadoespecifico = "N";
						this.mensaje = "INFO / Documento aun no procesado reintentando";
					}
				}else{
					this.estadoespecifico = "N";
					this.mensaje = "INFO / Documento aun no procesado reintentando";
				}
			}else{
				this.estadoespecifico = "N";
			}
		} catch (DOMException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	public File getDocFile() {
		return docFile;
	}

	public void setDocFile(File docFile) {
		this.docFile = docFile;
	}

	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getDocXML() {
		return docXML;
	}

	public void setDocXML(String docXML) {
		this.docXML = docXML;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstadoespecifico() {
		return estadoespecifico;
	}

	public void setEstadoespecifico(String estadoespecifico) {
		this.estadoespecifico = estadoespecifico;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}
}
