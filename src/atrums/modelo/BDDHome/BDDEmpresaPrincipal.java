package atrums.modelo.BDDHome;

import org.apache.log4j.Logger;

public class BDDEmpresaPrincipal {
	static final Logger log = Logger.getLogger(BDDEmpresaPrincipal.class);
	private String id = null;
	private String direccion = null;
	private String ambiente = null;
	private String tipoEmision = null;
	private String razonSocial = null;
	private String nombreComercial = null;
	private String ruc = null;
	private String codNumerico = null;
	private String numResolucion = null;
	private boolean obliContabi = false;
	
	public BDDEmpresaPrincipal(){
		super();
	}
	
	public BDDEmpresaPrincipal(String id, 
			String direccion, 
			String ambiente, 
			String tipoEmision, 
			String razonSocial, 
			String nombreComercial, 
			String ruc, 
			String codNumerico, 
			String numResolucion, 
			boolean obliContabi){
		this.id = id;
		this.direccion = direccion;
		this.ambiente = ambiente;
		this.tipoEmision = tipoEmision;
		this.razonSocial = razonSocial;
		this.nombreComercial = nombreComercial;
		this.ruc = ruc;
		this.codNumerico = codNumerico;
		this.numResolucion = numResolucion;
		this.obliContabi = obliContabi;
	}

	public boolean isObliContabi() {
		return obliContabi;
	}

	public void setObliContabi(boolean obliContabi) {
		this.obliContabi = obliContabi;
	}

	public String getNumResolucion() {
		return numResolucion;
	}

	public void setNumResolucion(String numResolucion) {
		this.numResolucion = numResolucion;
	}

	public String getCodNumerico() {
		return codNumerico;
	}

	public void setCodNumerico(String codNumerico) {
		this.codNumerico = codNumerico;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getTipoEmision() {
		return tipoEmision;
	}

	public void setTipoEmision(String tipoEmision) {
		this.tipoEmision = tipoEmision;
	}

	public String getAmbiente() {
		return ambiente;
	}

	public void setAmbiente(String ambiente) {
		this.ambiente = ambiente;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
