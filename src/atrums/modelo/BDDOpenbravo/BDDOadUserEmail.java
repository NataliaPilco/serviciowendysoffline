package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOadUserEmail {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String email = null;
	
	public BDDOadUserEmail() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
