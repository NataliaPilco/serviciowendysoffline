package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcTaxcategory {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String nombre = null;
	private String codigo = null;
	
	public BDDOcTaxcategory() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
}
