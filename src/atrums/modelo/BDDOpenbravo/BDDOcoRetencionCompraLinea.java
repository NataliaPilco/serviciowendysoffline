package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcoRetencionCompraLinea {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String linea = null;
	private String tipo = null;
	private String baseimponible = null;
	private String valor = null;
	
	public BDDOcoRetencionCompraLinea() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getBaseimponible() {
		return baseimponible;
	}

	public void setBaseimponible(String baseimponible) {
		this.baseimponible = baseimponible;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
}
