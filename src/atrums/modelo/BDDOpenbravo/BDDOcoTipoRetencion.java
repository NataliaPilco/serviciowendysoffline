package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcoTipoRetencion {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String codigo = null;
	private String porcentaje = null;
	private String tipo = null;
	
	public BDDOcoTipoRetencion() {
		super();
	}

	public String getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}