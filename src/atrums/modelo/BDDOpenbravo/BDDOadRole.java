package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOadRole {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	
	public BDDOadRole() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
