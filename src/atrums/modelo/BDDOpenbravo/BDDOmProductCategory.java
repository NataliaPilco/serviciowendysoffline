package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOmProductCategory {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String name = "Productos Wendys";
	
	public BDDOmProductCategory() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
