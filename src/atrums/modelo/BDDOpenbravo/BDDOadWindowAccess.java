package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOadWindowAccess {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	
	public BDDOadWindowAccess() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
