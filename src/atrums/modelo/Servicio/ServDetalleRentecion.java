package atrums.modelo.Servicio;

import org.apache.log4j.Logger;

public class ServDetalleRentecion {
	static final Logger log = Logger.getLogger(ServDetalleRentecion.class);
	private String sucursal = null;
	private String nroRetencion = null;
	private String tipo = null;
	private String baseImponibleReten = null;
	private String codigoRetencion = null;
	private String valorRetencion = null;
	
	public ServDetalleRentecion() {
		super();
	}
	
	public ServDetalleRentecion(String sucursal, 
			String nroRetencion , 
			String tipo, 
			String baseImponibleReten, 
			String codigoRetencion, 
			String valorRetencion){
		this.sucursal = sucursal;
		this.nroRetencion = nroRetencion;
		this.tipo = tipo;
		this.baseImponibleReten = baseImponibleReten;
		this.codigoRetencion = codigoRetencion;
		this.valorRetencion = valorRetencion;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getNroRetencion() {
		return nroRetencion;
	}

	public void setNroRetencion(String nroRetencion) {
		this.nroRetencion = nroRetencion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getBaseImponibleReten() {
		return baseImponibleReten;
	}

	public void setBaseImponibleReten(String baseImponibleReten) {
		this.baseImponibleReten = baseImponibleReten;
	}

	public String getCodigoRetencion() {
		return codigoRetencion;
	}

	public void setCodigoRetencion(String codigoRetencion) {
		this.codigoRetencion = codigoRetencion;
	}

	public String getValorRetencion() {
		return valorRetencion;
	}

	public void setValorRetencion(String valorRetencion) {
		this.valorRetencion = valorRetencion;
	}
}
