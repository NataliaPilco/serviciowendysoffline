package atrums.modelo.Servicio;

import org.apache.log4j.Logger;

public class ServPago {
	static final Logger log = Logger.getLogger(ServPago.class);
	private String metodo = null;
	private String monto = null;
	
	public ServPago() {
		super();
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}
}
