/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public class ServFechas {
    static final Logger log = Logger.getLogger(ServFechas.class);
    private List<String> fechas;
    
    public ServFechas(SOAPMessage soapMes){
    	fechas = new ArrayList<String>();
    	
        try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("FECHA_FACTURA").getLength();
                int tamanioR = soapMes.getSOAPBody().getElementsByTagName("FECHA_EMISION_RETEN").getLength();
                int tamanioG = soapMes.getSOAPBody().getElementsByTagName("FECHA").getLength();
                
                if(tamanio != 0){
                    NodeList datos = soapMes.getSOAPBody().getElementsByTagName("FECHA_FACTURA");
                
                    for(int i=0; i<tamanio; i++){
                        fechas.add(datos.item(i).getFirstChild().getNodeValue());
                    }
                    
                    datos = null;
                }else if(tamanioR != 0){
                    NodeList datos = soapMes.getSOAPBody().getElementsByTagName("FECHA_EMISION_RETEN");
                
                    for(int i=0; i<tamanioR; i++){
                        fechas.add(datos.item(i).getFirstChild().getNodeValue());
                    }
                    
                    datos = null;
                }else if(tamanioG != 0){
                    NodeList datos = soapMes.getSOAPBody().getElementsByTagName("FECHA");
                
                    for(int i=0; i<tamanioG; i++){
                        fechas.add(datos.item(i).getFirstChild().getNodeValue());
                    }
                    
                    datos = null;
                }
                
            }
            
            soapMes = null;
        } catch(DOMException ex){
            log.error(ex.getMessage());
        } catch(SOAPException ex){
            log.error(ex.getMessage());
        }
    }

    public List<String> getFechas() {
        return fechas;
    }

    public void setFechas(List<String> fechas) {
        this.fechas = fechas;
    }   
}