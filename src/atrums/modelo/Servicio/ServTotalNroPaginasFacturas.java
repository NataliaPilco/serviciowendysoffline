package atrums.modelo.Servicio;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public class ServTotalNroPaginasFacturas {
	static final Logger log = Logger.getLogger(ServTotalNroPaginasFacturas.class);
	private int nroPaginas = 0;
	
	public ServTotalNroPaginasFacturas(SOAPMessage soapMes) {
		try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("Column1").getLength();
                
                if(tamanio != 0){
                    NodeList datos = soapMes.getSOAPBody().getElementsByTagName("Column1");
                
                    int intValorTotal = Integer.valueOf(datos.item(0).getFirstChild().getNodeValue());
                    Double total = (double) intValorTotal / (double) 100;
                    total = Math.ceil(total);
                    
                    this.nroPaginas = total.intValue();
                    
                    total = null;
                    datos = null;
                }
            }
            
            soapMes = null;
        } catch(DOMException ex){
            log.error(ex.getMessage());
        } catch(SOAPException ex){
            log.error(ex.getMessage());
        }
	}

	public int getNroPaginas() {
		return nroPaginas;
	}

	public void setNroPaginas(int nroPaginas) {
		this.nroPaginas = nroPaginas;
	}
}
