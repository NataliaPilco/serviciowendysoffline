package atrums.modelo.Servicio;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

public class ServEmail {
	static final Logger log = Logger.getLogger(ServEmail.class);
	private String identificacion = null;
	private String email = null;
	
	public ServEmail() {
		super();
	}

	public ServEmail(SOAPMessage soapMes){
		try{
			if(soapMes != null){
				int tamanio = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").getLength();
				if(tamanio > 0){
					for(int i=0;i<tamanio;i++){
						this.email = "N/A";
						this.identificacion = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").item(i).getFirstChild().getNodeValue();
						this.email = soapMes.getSOAPBody().getElementsByTagName("EMAIL").item(i).getFirstChild().getNodeValue();
						if(soapMes.getSOAPBody().getElementsByTagName("ACTIVO").item(i).getFirstChild().getNodeValue().equals("Y")){}
					}
				}
			}
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}
	
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
