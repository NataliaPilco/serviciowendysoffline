package atrums.modelo.Servicio;

import org.apache.log4j.Logger;

public class ServDocumentoSucNumFNumDocProve {
	static final Logger log = Logger.getLogger(ServDocumentoSucNumFNumDocProve.class);
	private String sucursal;
	private String nroFactura;
	private String nroDocumento;
	private String tipoDoc;
	private String proveedor;
	
    
    public ServDocumentoSucNumFNumDocProve(String sucursal, 
    		String nroFactura, 
    		String nroDocumento, 
    		String tipoDoc, 
    		String proveedor){
        this.sucursal = sucursal;
        this.nroFactura = nroFactura;
        this.nroDocumento = nroDocumento;
        this.tipoDoc = tipoDoc;
        this.proveedor = proveedor;
    }

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
}
