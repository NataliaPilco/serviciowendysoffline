package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public class ServClaves {
	static final Logger log = Logger.getLogger(ServFechas.class);
    private List<String> claves;
    
    public ServClaves(SOAPMessage soapMes){
    	claves = new ArrayList<String>();
        try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("CLAVE_SRI").getLength();
                
                if(tamanio != 0){
                    NodeList datos = soapMes.getSOAPBody().getElementsByTagName("CLAVE_SRI");
                
                    for(int i=0; i<tamanio; i++){
                    	claves.add(datos.item(i).getFirstChild().getNodeValue());
                    }
                    
                    datos = null;
                }
            }
            
            soapMes = null;
        } catch(DOMException ex){
            log.error(ex.getMessage());
        } catch(SOAPException ex){
            log.error(ex.getMessage());
        }
    }

	public List<String> getClaves() {
		return claves;
	}

	public void setClaves(List<String> claves) {
		this.claves = claves;
	}
}
