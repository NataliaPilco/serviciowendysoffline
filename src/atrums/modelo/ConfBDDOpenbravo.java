/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class ConfBDDOpenbravo {
    static final Logger log = Logger.getLogger(ConfBDDOpenbravo.class);
    private static InputStream confService = null;
    private Properties propiedad = new Properties();
    
    private String ipBDD;
    private String puertoBDD;
    private String bDD;
    private String usuarioBDD;
    private String passwordBDD;
    private String empresa;
    
    public ConfBDDOpenbravo(){
        try {
            confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/ConfBDDOpenbravo.xml");
            propiedad.loadFromXML(confService);
            
            //Datos de la Base de Datos Openbravo
            this.ipBDD = propiedad.getProperty("IpBDD");
            this.puertoBDD = propiedad.getProperty("PuertoBDD");
            this.bDD = propiedad.getProperty("BDD");
            this.usuarioBDD = propiedad.getProperty("UsuarioBDD");
            this.passwordBDD = propiedad.getProperty("PasswordBDD");
            
            this.empresa = propiedad.getProperty("Empresa");
        } catch (IOException ex) {
            log.error(ex.getStackTrace());
        }
    }

    public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getIpBDD() {
        return ipBDD;
    }

    public void setIpBDD(String ipBDD) {
        this.ipBDD = ipBDD;
    }

    public String getPuertoBDD() {
        return puertoBDD;
    }

    public void setPuertoBDD(String puertoBDD) {
        this.puertoBDD = puertoBDD;
    }

    public String getbDD() {
        return bDD;
    }

    public void setbDD(String bDD) {
        this.bDD = bDD;
    }

    public String getUsuarioBDD() {
        return usuarioBDD;
    }

    public void setUsuarioBDD(String usuarioBDD) {
        this.usuarioBDD = usuarioBDD;
    }

    public String getPasswordBDD() {
        return passwordBDD;
    }

    public void setPasswordBDD(String passwordBDD) {
        this.passwordBDD = passwordBDD;
    }
}
