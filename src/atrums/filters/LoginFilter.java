package atrums.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import atrums.modelo.facesBean.LoginBean;

/**
 * Servlet Filter implementation class LoginFilter
 */
public class LoginFilter implements Filter {
	static final Logger log = Logger.getLogger(LoginFilter.class);

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		LoginBean loginBean = (LoginBean) req.getSession().getAttribute("loginBean");
		
		String urlStr = req.getRequestURL().toString().toLowerCase();
		
		if (noProteger(urlStr)) {
			chain.doFilter(request, response);
			return;
		}
			  
		if (loginBean == null || !loginBean.isLogeado()) {
			res.sendRedirect(req.getContextPath() + "/index.xhtml");
			return;
		}
		
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}
	
	private boolean noProteger(String urlStr) {
		if (urlStr.endsWith("index.xhtml"))
			return true;
		if (urlStr.indexOf("/javax.faces.resource/") != -1)
			return true;
		return false;
	}
}
