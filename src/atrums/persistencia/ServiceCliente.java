package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServEmail;

public class ServiceCliente {
	static final Logger log = Logger.getLogger(ServiceCliente.class);
    private final ConfService confService;
    private String identificacion;
    
    public ServiceCliente(ConfService confService, String identificacion){
        this.confService = confService;
        this.identificacion = identificacion;
    }
    
    public ServCliente callCliente(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("identificacion", this.identificacion);
    	
        ServicioWendysCall wendysCall = new ServicioWendysCall(this.confService.getwSDL(), 
        		auxParametros, 
        		this.confService.getConsultarClientePorIdentificacion());
        
        ServCliente cliente = new ServCliente(wendysCall.Call(), this.confService);
        
        if(this.identificacion.equals("9999999999999") || this.identificacion.equals("9999999999")){
        	cliente.setIdentificacion("9999999999999");
        	cliente.setRazonSocial("CONSUMIDOR FINAL");
        	cliente.setNombreComercial("CONSUMIDOR FINAL");
        	cliente.setTipo("07");
        	cliente.setDireccion(new ServDireccion());
        	cliente.setEmail(new ServEmail());
        }
        
        return cliente;
    }
}
