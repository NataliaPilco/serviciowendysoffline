package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServClaves;

public class ServiceClavesAccesoFacturas {
	static final Logger log = Logger.getLogger(ServiceClavesAccesoFacturas.class);
    private final ConfService confService;
    
    public ServiceClavesAccesoFacturas(ConfService confService){
        this.confService = confService;
    }
    
    public List<String> callClaves(){
        ServicioWendysCall wendysCall = new ServicioWendysCall(confService.getwSDL(), 
                new Hashtable<String, String>(), 
                confService.getConsultarClavesAccesoFacturas());
        
        ServClaves claves = new ServClaves(wendysCall.Call());
        
        wendysCall = null;
        return claves.getClaves();
    }
}
