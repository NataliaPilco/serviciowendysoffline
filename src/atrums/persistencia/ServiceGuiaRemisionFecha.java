package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServFechas;

public class ServiceGuiaRemisionFecha {
	static final Logger log = Logger.getLogger(ServiceGuiaRemisionFecha.class);
	private final ConfService confService;
	
	public ServiceGuiaRemisionFecha(ConfService confService){
		this.confService = confService; 
	}
	
	public List<String> callFechas(){
		ServicioWendysCall wendysCall = new ServicioWendysCall(
				this.confService.getwSDL(), 
				new Hashtable<String, String>(), 
				confService.getConsultarFechasGuiaRemision());
		
		ServFechas fechas = new ServFechas(wendysCall.Call());
		
		wendysCall = null;
		return fechas.getFechas();
	}
}
