package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServFechas;

public class ServiceNotaCreditoFecha {
	static final Logger log = Logger.getLogger(ServiceNotaCreditoFecha.class);
    private final ConfService confService;
    
    public ServiceNotaCreditoFecha(ConfService confService){
        this.confService = confService;
    }
    
    public List<String> callFechas(){
        ServicioWendysCall wendysCall = new ServicioWendysCall(confService.getwSDL(), 
                new Hashtable<String, String>(), 
                confService.getConsultarFechasNotaCredito());
        
        ServFechas fechas = new ServFechas(wendysCall.Call());
        
        return fechas.getFechas();
    }
}
