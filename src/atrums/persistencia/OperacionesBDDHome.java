package atrums.persistencia;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;

public class OperacionesBDDHome implements OperacionesBDD{
	static final Logger log = Logger.getLogger(OperacionesBDDHome.class);
	private BDDDocumentoBase documentoBase = null;
	private ConfBDDOpenbravo confBDDOpenbravo  = null;
	
	public OperacionesBDDHome(){
		this.confBDDOpenbravo = new ConfBDDOpenbravo();
	}
	
	public OperacionesBDDHome(BDDDocumentoBase documentoBase) {
		this.documentoBase = documentoBase;
		this.confBDDOpenbravo = new ConfBDDOpenbravo();
	}
	
	public void clavesFechas(List<String> claves, String clavesSQL, Connection connection) {
		Statement statement = null;
		String sql;
		
		try{
			statement = connection.createStatement();
			
			ResultSet rs = null;
			sql = "SELECT claveacceso "
					+ "FROM atecfe_documentosbase "
					+ "WHERE claveacceso IN ('" + clavesSQL + "');";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				for(String auxClave: claves){
					if(rs.getString("claveacceso").equals(auxClave)){
						claves.remove(auxClave);
						break;
					}
				}
			}
			
			sql = null;
			rs.close();  log.info("Close BDD o Statement");
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
	}
	
	public boolean loginUser(String nombre, String clave, Connection connection) {
		Statement statement = null;
		String sql;
		boolean login = false;
		
		try{
			statement = connection.createStatement();
			
			ResultSet rs = null;
			sql = "SELECT count(*) AS total "
					+ "FROM ad_user au "
					+ "INNER JOIN ad_user_roles aur ON (aur.ad_user_id = au.ad_user_id) "
					+ "INNER JOIN ad_role ar ON (ar.ad_role_id = aur.ad_role_id) "
					+ "WHERE au.username = '" + nombre + "' "
					+ "AND au.password = '" + clave + "' "
					+ "AND au.isactive = 'Y' "
					+ "AND ar.name ~* 'admin' "
					+ "AND ar.ad_client_id <> '0' "
					+ "AND ar.isactive = 'Y';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				int total = rs.getInt("total");
				
				if (total > 0)
					login = true;
			}
			
			sql = null;
			rs.close();  log.info("Close BDD o Statement");
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		return login;
	}
	
	public void clavesFechasFactura(List<ServDocumentoNroNroEsNroEm> claves, String clavesSQL, Connection connection) {
		Statement statement = null;
		String sql;
		
		try{
			statement = connection.createStatement();
			
			sql = "select pg_terminate_backend(pid) from pg_stat_activity where state = 'idle in transaction';";
			statement.executeQuery(sql);
			connection.commit();
			
			ResultSet rs = null;
			
			sql = "SELECT (nrodocumento || '-' || nroestablecimiento || '-' || nroemision || '-' || tipodocumento) AS claveacceso "
					+ "FROM atecfe_documentosbase "
					+ "WHERE (nrodocumento || '-' || nroestablecimiento || '-' || nroemision || '-' || tipodocumento) IN ('" + clavesSQL + "');";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				for(ServDocumentoNroNroEsNroEm doc: claves){
					String auxDoc = doc.getNroDocumento() + "-" + doc.getNroEstablecimiento() + "-" + doc.getNroEmision() + "-" + doc.getTipoDoc();					
					
					if(rs.getString("claveacceso").equalsIgnoreCase(auxDoc)){
						claves.remove(doc);
						break;
					}
				}
			}
			
			sql = null;
			rs.close();  log.info("Close BDD o Statement");
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
	}
	
	@Override
	public boolean guardarDocumento(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		int total = 0;
		
		try {
			statement = connection.createStatement();
			
			ResultSet rs = null;
			sql = "SELECT count(*) AS TOTAL FROM atecfe_documentosbase WHERE "
					+ "nrodocumento = '" + this.documentoBase.getNrodocumento() + "' AND "
					+ "nroestablecimiento = '" + this.documentoBase.getNroestablecimiento() + "' AND "
					+ "nroemision = '" + this.documentoBase.getNroemision() + "' AND " 
					+ "tipodocumento = '" + (this.documentoBase.getTipodocumento() != null ? this.documentoBase.getTipodocumento() : "FC") + "' AND "
					+ "fechadocumento = '" + this.documentoBase.getFechadocumento() + "';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				if(rs.getString("total") != null){
					total = rs.getInt("total");
				}
			}
			
			rs.close();  log.info("Close BDD o Statement");
			
			if(total == 0){
				
				log.info("Ingresando documento clave acceso: " + this.documentoBase.getNroestablecimiento() 
						+ "-" + this.documentoBase.getNroemision() 
						+ "-" + this.documentoBase.getNrodocumento());

				sql = "INSERT INTO atecfe_documentosbase("
						+ "atecfe_documentosbase_id, ad_client_id, ad_org_id, isactive, "
						+ "created, createdby, updated, updatedby, nrodocumento, nroestablecimiento, nroemision, "
						+ "tipodocumento, fechadocumento, claveacceso, nroautorizacion, "
						+ "fechaautorizacion, usuario, clave, estado) VALUES ("
						+ "'" + this.documentoBase.getDocumentosbaseid() + "', "
						+ "(SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1), "
						+ "(SELECT ao.ad_org_id FROM ad_org AS ao WHERE ao.em_co_nro_estab = '" + this.documentoBase.getNroestablecimiento() + "' AND ao.em_co_punto_emision = '" + this.documentoBase.getNroemision() + "' AND isactive = 'Y' AND ao.ad_client_id = (SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1) LIMIT 1), "
						+ "'Y', NOW(), '100', NOW(), '100', "
						+ "'" + this.documentoBase.getNrodocumento() + "', "
						+ "'" + this.documentoBase.getNroestablecimiento() + "', "
						+ "'" + this.documentoBase.getNroemision() + "', " 
						+ "'" + (this.documentoBase.getTipodocumento() != null ? this.documentoBase.getTipodocumento() : "FC") + "', "
						+ "'" + this.documentoBase.getFechadocumento() + "', "
						+ "null, "
						+ "null, "
						+ "null, "
						+ "null, "
						+ "null, "
						+ "'N');";
				
				registro = statement.executeUpdate(sql);
				
				sql = "INSERT INTO atecfe_wendys_documentos("
						+ "atecfe_wendys_documentos_id, ad_client_id, ad_org_id, isactive, "
						+ "created, createdby, updated, updatedby, nrodocumento, ptoemision, "
						+ "nroestablecimiento, tipodocumento, estado, observacion) "
						+ "VALUES ('" + this.documentoBase.getDocumentosbaseid() + "', "
						+ "(SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1), "
						+ "(SELECT ao.ad_org_id FROM ad_org AS ao WHERE ao.em_co_nro_estab = '" + this.documentoBase.getNroestablecimiento() + "' AND ao.em_co_punto_emision = '" + this.documentoBase.getNroemision() + "' AND isactive = 'Y' AND ao.ad_client_id = (SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1) LIMIT 1), "
						+ "'Y', NOW(), '100', NOW(), '100', "
						+ "'" + this.documentoBase.getNrodocumento() + "', '" + this.documentoBase.getNroemision() + "', "
						+ "'" + this.documentoBase.getNroestablecimiento() + "', '" + (this.documentoBase.getTipodocumento() != null ? this.documentoBase.getTipodocumento() : "FC") +"', '" + this.documentoBase.getEstado() + "', '');";
				
				statement.executeUpdate(sql);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			if(ex.getMessage().indexOf("duplicada") == -1 && ex.getMessage().indexOf("duplicate") == -1){
				log.warn(ex.getMessage());
			}
		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean actualizarEstado(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE atecfe_documentosbase SET fecha = now(), "
					+ "claveacceso = " + (this.documentoBase.getClaveacceso() != null ? ("'" + this.documentoBase.getClaveacceso() + "'") : "null") + ", "
					+ "nroautorizacion = " + (this.documentoBase.getNroautorizacion() != null ? ("'" + this.documentoBase.getNroautorizacion() + "'") : "null") + ", "
					+ "fechaautorizacion = " + (this.documentoBase.getFechaautorizacion() != null ? ("'" + this.documentoBase.getFechaautorizacion() + "'") : "null") + ", "
					+ "usuario = " + (this.documentoBase.getUsuario() != null ? ("'" + this.documentoBase.getUsuario() + "'") : "null") + ", "
					+ "clave = " + (this.documentoBase.getPassword() != null ? ("'" + this.documentoBase.getPassword() + "'") : "null") + ", "
					+ "estado = '" + this.documentoBase.getEstado() + "', "
					+ "mensajeerror = " + (this.documentoBase.getMensaje() != null ? ("'" + this.documentoBase.getMensaje() + "'") : "null") + " "
					+ "WHERE atecfe_documentosbase_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
			
			registro = statement.executeUpdate(sql);
			
			if(this.documentoBase.getEstado().equals("MIG")){
				sql = "DELETE FROM atecfe_wendys_documentos "
						+ "WHERE atecfe_wendys_documentos_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
			}else{
				int auxregistro = 0;
				
				ResultSet rs = null;
				sql = "SELECT count(*) AS TOTAL FROM atecfe_wendys_documentos WHERE "
						+ "atecfe_wendys_documentos_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
				
				rs = statement.executeQuery(sql);
				
				while (rs.next()){
					if(rs.getString("total") != null){
						auxregistro = rs.getInt("total");
					}
				}
				
				rs.close();  log.info("Close BDD o Statement");
				
				if(auxregistro == 0){
					sql = "INSERT INTO atecfe_wendys_documentos("
							+ "atecfe_wendys_documentos_id, ad_client_id, ad_org_id, isactive, "
							+ "created, createdby, updated, updatedby, nrodocumento, ptoemision, "
							+ "nroestablecimiento, tipodocumento, estado, observacion) "
							+ "VALUES ('" + this.documentoBase.getDocumentosbaseid() + "', "
							+ "(SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1), "
							+ "(SELECT ao.ad_org_id FROM ad_org AS ao WHERE ao.em_co_nro_estab = '" + this.documentoBase.getNroestablecimiento() + "' AND ao.em_co_punto_emision = '" + this.documentoBase.getNroemision() + "' AND isactive = 'Y' AND ao.ad_client_id = (SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1) LIMIT 1), "
							+ "'Y', NOW(), '100', NOW(), '100', "
							+ "'" + this.documentoBase.getNrodocumento() + "', '" + this.documentoBase.getNroemision() + "', "
							+ "'" + this.documentoBase.getNroestablecimiento() + "', '" + (this.documentoBase.getTipodocumento() != null ? this.documentoBase.getTipodocumento() : "FC") +"', '" + this.documentoBase.getEstado() + "', '');";
					
					statement.executeUpdate(sql);
				}
				
				if(this.documentoBase.getMensaje() != null){
					if(this.documentoBase.getMensaje().indexOf("No existe el documento en el SRI") != -1){
						sql = "DELETE FROM atecfe_wendys_documentos "
								+ "WHERE atecfe_wendys_documentos_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
					}else{
						sql = "UPDATE atecfe_wendys_documentos SET "
								+ "estado = '" + this.documentoBase.getEstado() + "', fecha = '" + this.documentoBase.getFechadocumento() + "', "
								+ "observacion =  " + (this.documentoBase.getMensaje() != null ? ("'" + this.documentoBase.getMensaje() + "'") : "null") + " "
								+ "WHERE atecfe_wendys_documentos_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
					}
				}else{
					sql = "UPDATE atecfe_wendys_documentos SET "
							+ "estado = '" + this.documentoBase.getEstado() + "', fecha = '" + this.documentoBase.getFechadocumento() + "', "
							+ "observacion =  " + (this.documentoBase.getMensaje() != null ? ("'" + this.documentoBase.getMensaje() + "'") : "null") + " "
							+ "WHERE atecfe_wendys_documentos_id = '" +  this.documentoBase.getDocumentosbaseid() + "';";
				}
			}
			
			statement.executeUpdate(sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			if(ex.getMessage().indexOf("duplicada") == -1){
				log.error(ex.getMessage());
			}
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean getBDDDocumentoBaseProcesar(String tipoDocumento, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.documentoBase.setDocumentosbaseid(null);
			sql = "SELECT dct.atecfe_documentosbase_id,"
					+ "dct.nrodocumento,"
					+ "dct.nroestablecimiento, "
					+ "dct.nroemision, "
					+ "dct.tipodocumento, "
					+ "dct.fechadocumento, "
					+ "dct.fecha, "
					+ "dct.claveacceso, "
					+ "dct.nroautorizacion, "
					+ "dct.fechaautorizacion, "
					+ "dct.usuario, "
					+ "dct.clave, "
					+ "dct.estado "
					+ "FROM atecfe_documentosbase AS dct "
					+ "WHERE dct.estado in ('N','DEV','REC','NOR','NOA') "
					+ "AND dct.tipodocumento in ('" + tipoDocumento + "') "
					+ "ORDER BY fecha ASC "
					+ "LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				this.documentoBase.setDocumentosbaseid(rs.getString("atecfe_documentosbase_id"));
				this.documentoBase.setNrodocumento(rs.getString("nrodocumento"));
				this.documentoBase.setNroestablecimiento(rs.getString("nroestablecimiento"));
				this.documentoBase.setNroemision(rs.getString("nroemision"));
				this.documentoBase.setTipodocumento(rs.getString("tipodocumento"));
				this.documentoBase.setFechadocumento(rs.getString("fechadocumento"));
				this.documentoBase.setClaveacceso(rs.getString("claveacceso"));
				this.documentoBase.setNroautorizacion(rs.getString("nroautorizacion"));
				this.documentoBase.setUsuario(rs.getString("usuario"));
				this.documentoBase.setPassword(rs.getString("clave"));
				this.documentoBase.setEstado(rs.getString("estado"));
				
				//borrar
				this.documentoBase.setEstado("N");
				this.documentoBase.setClaveacceso("");
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.documentoBase.getDocumentosbaseid() != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			
			if(connection.isClosed()){
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} catch (Exception ex) {
			// TODO: handle exception
			log.error(ex.getMessage(), ex);
		}
		
		return connection;
	}
	
	public BDDDocumentoBase getDocumentoBase() {
		return documentoBase;
	}

	public void setDocumentoBase(BDDDocumentoBase documentoBase) {
		this.documentoBase = documentoBase;
	}

	@Override
	public boolean getEmpresaPrincipal(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDDocumentoBaseMigrar(String tipoDocumento, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.documentoBase.setDocumentosbaseid(null);
			sql = "SELECT dct.atecfe_documentosbase_id,"
					+ "dct.nrodocumento,"
					+ "dct.nroestablecimiento, "
					+ "dct.nroemision, "
					+ "dct.tipodocumento, "
					+ "dct.fechadocumento, "
					+ "dct.claveacceso, "
					+ "dct.nroautorizacion, "
					+ "dct.fechaautorizacion, "
					+ "dct.usuario, "
					+ "dct.clave, "
					+ "dct.estado, "
					+ "dct.mensajeerror "
					+ "FROM atecfe_documentosbase AS dct " 
					+ "WHERE dct.estado in ('AUT') AND dct.tipodocumento in ('" + tipoDocumento + "') "
					+ "ORDER BY estado DESC, fecha ASC "
					+ "LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				this.documentoBase.setDocumentosbaseid(rs.getString("atecfe_documentosbase_id"));
				this.documentoBase.setNrodocumento(rs.getString("nrodocumento"));
				this.documentoBase.setNroestablecimiento(rs.getString("nroestablecimiento"));
				this.documentoBase.setNroemision(rs.getString("nroemision"));
				this.documentoBase.setTipodocumento(rs.getString("tipodocumento"));
				this.documentoBase.setFechadocumento(rs.getString("fechadocumento"));
				this.documentoBase.setClaveacceso(rs.getString("claveacceso"));
				this.documentoBase.setNroautorizacion(rs.getString("nroautorizacion"));
				this.documentoBase.setFechaautorizacion(rs.getString("fechaautorizacion"));
				this.documentoBase.setUsuario(rs.getString("usuario"));
				this.documentoBase.setPassword(rs.getString("clave"));
				this.documentoBase.setEstado(rs.getString("estado"));
				this.documentoBase.setMensaje(rs.getString("mensajeerror"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.documentoBase.getDocumentosbaseid() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean existeDocumentoImporFCNC(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOadClient(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOadOrg(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcDoctype(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean existeDato(String sql, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcbPartner(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcbGroup(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadUser(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcLocation(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcbPartnerLocation(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcbPartner(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcInvoice(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcPaymentterm(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOmPricelist(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDmProductCategory(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcTaxcategory(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcTax(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOmProduct(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcInvoiceLine(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadUserPortal(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadRole(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadRoleOrgaccess(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadWindowAccess(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOadUserRoles(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOadUser(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcoRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcoTipoRetencion(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcoTipoRetencion(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcoBpRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcoBpRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcoRetencionCompraLinea(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updateBDDOmProduct(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcPocConfiguration(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public int numeroDocumentos(String fecha, String tipodocumento ,Connection connection){
		Statement statement = null;
		String sql;
		int total = 0;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT count(*) as total "
					+ "FROM atecfe_documentosbase "
					+ "WHERE fechadocumento = '" + fecha + "' AND tipodocumento = '" + tipodocumento + "';";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				if(rs.getString("total") != null){
					total = rs.getInt("total");
				}
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		return total;
	}

	@Override
	public boolean saveBDDNovedad(Connection connection, String mensaje) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveLogServicio(Connection connection, String Mensaje) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "INSERT INTO atecfe_log_seguimiento( "
					+ "atecfe_log_seguimiento_id, ad_client_id, ad_org_id, isactive, "
					+ "created, createdby, updated, updatedby, mensaje) "
					+ "VALUES (get_uuid(), "
					+ "(SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1), "
					+ "'0', 'Y', "
					+ "NOW(), '100', NOW(), '100', "
					+ "'" + Mensaje + "');";
			
			registro = statement.executeUpdate(sql);
			
			sql = "DELETE FROM atecfe_log_seguimiento WHERE (SELECT (count(*) > 80) "
					+ "FROM atecfe_log_seguimiento) AND atecfe_log_seguimiento_id IN (SELECT atecfe_log_seguimiento_id "
					+ "FROM atecfe_log_seguimiento "
					+ "ORDER BY atecfe_log_seguimiento.created ASC "
					+ "LIMIT (SELECT (CASE WHEN (count(*) - 80) <= 0 THEN 0 ELSE (count(*) - 80) END) "
					+ "FROm atecfe_log_seguimiento));";
			statement.executeUpdate(sql);
			
			if(registro > 0){
				connection.commit();
			}else{
				connection.rollback(); log.info("Rollback Proceso");
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} catch (Exception ex) {
			log.warn(ex.getMessage());
		}  finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean procesarFactura(String total, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String buscarPaymentSchedule(Connection connection) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean iniciarPago(String idpaymentSchedule, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean savePago(String idpayment, String metodoPago, String amount,
			Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean savePagolinea(String idpayment, String idpaymentline,
			String amount, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean savePaymentScheduledetail(String idpaymentSchedule,
			String idpaymentline, String amount, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean procesarPago(String idpayment, boolean dato,
			Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean terminarFactura(String ispaid, String amountTotal,
			Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcbPartnerTrans(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDOcbPartnerTrans(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcLocationDes(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOcLocationPar(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOmInout(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean saveBDDOmInoutLine(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean terminarRetencion(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}
}