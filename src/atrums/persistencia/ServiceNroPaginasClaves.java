package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServTotalNroPaginasFacturas;

public class ServiceNroPaginasClaves {
	static final Logger log = Logger.getLogger(ServiceNroPaginasClaves.class);
    private final ConfService confService;
    private String fecha;
    
    public ServiceNroPaginasClaves(ConfService confService, String fecha) {
    	this.confService = confService;
        this.fecha = fecha;
	}
    
    public int callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenTotalClavesPorFecha());
        
        ServTotalNroPaginasFacturas nroPaginasFacturas = new ServTotalNroPaginasFacturas(swendysCall.Call());
        
        return nroPaginasFacturas.getNroPaginas();
    }
}
