package atrums.persistencia;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;

public class ServicioSRICall {
	static final Logger log = Logger.getLogger(ServicioSRICall.class);
	private String keyCert = "./..//CERTSRI/cacerts";
	private ConfService confService = null;
	private String ambiente = null;
	private String fileString = null;
	private String clave = null;
	
	public ServicioSRICall(){
		super();
	}
	
	public ServicioSRICall(ConfService confService, String ambiente, String fileString, String clave){
		this.confService = confService;
		this.ambiente = ambiente;
		this.fileString = fileString;
		this.clave = clave;
		URL url = this.getClass().getClassLoader().getResource(keyCert);
		this.keyCert = url.getPath();
		
		if(this.keyCert.indexOf(":") != -1){
			this.keyCert = keyCert.replaceFirst("/", "");
		}else if(this.keyCert.indexOf("//") != -1){
			this.keyCert = keyCert.replaceFirst("//", "/");
		}
	}
	
	public SOAPMessage RecibidoCall() {
		System.setProperty("javax.net.ssl.keyStore", keyCert);
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		System.setProperty("avax.net.ssl.trustStore", keyCert);
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
	    
	    SOAPMessage soapMes = null;
	    SOAPConnectionFactory soapConF;
		try {
			soapConF = SOAPConnectionFactory.newInstance();
			SOAPConnection soapCon = soapConF.createConnection();
			String wdsl = "";
			
			if(this.ambiente.equals("1")){
				wdsl = this.confService.getPruebasRt();
			}else{
				wdsl = this.confService.getProduccionRt();
			}
			
			URL endPoint = new URL(null, wdsl, new URLStreamHandler() {
				
				@Override
		        protected URLConnection openConnection(URL u) throws IOException {
					URL clone_url = new URL(u.toString());
					HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();
					
					clone_urlconConnection.setConnectTimeout(60000);
					clone_urlconConnection.setReadTimeout(60000);
					return clone_urlconConnection;
		        }
			});
			
			soapMes = soapCon.call(createSoapReqRecXML(confService.getNameSpaceRt(), this.fileString), endPoint);
			soapCon.close();  log.info("Close BDD o Statement");
			soapCon = null;
			soapConF = null;
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (UnsupportedOperationException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (MalformedURLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return soapMes;
	}
	
	public SOAPMessage AutorizadoCall() {
		System.setProperty("javax.net.ssl.keyStore", keyCert);
		System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
		System.setProperty("avax.net.ssl.trustStore", keyCert);
		System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
	    
	    SOAPMessage soapMes = null;
	    SOAPConnectionFactory soapConF;
		try {
			soapConF = SOAPConnectionFactory.newInstance();
			SOAPConnection soapCon = soapConF.createConnection();
			String wdsl = "";
			
			if(this.ambiente.equals("1")){
				wdsl = this.confService.getPruebasAt();
			}else{
				wdsl = this.confService.getProduccionAt();
			}
			
			URL endPoint = new URL(null, wdsl, new URLStreamHandler() {
				
				@Override
		        protected URLConnection openConnection(URL u) throws IOException {
					URL clone_url = new URL(u.toString());
					HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();
					
					clone_urlconConnection.setConnectTimeout(60000);
					clone_urlconConnection.setReadTimeout(60000);
					return clone_urlconConnection;
		        }
			});
			
			soapMes = soapCon.call(createSoapReqAutXML(confService.getNameSpaceAt(), this.clave), endPoint);
			soapCon.close();  log.info("Close BDD o Statement");
			soapCon = null;
			soapConF = null;
		} catch (UnsupportedOperationException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (MalformedURLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return soapMes;
	}	
	
	private static SOAPMessage createSoapReqRecXML(String nameSpace, String fileString) {
		MessageFactory messageFac = null;
		SOAPMessage soapMes = null;
		
		try {
			messageFac = MessageFactory.newInstance();
			soapMes = messageFac.createMessage();
			SOAPPart soapPart = soapMes.getSOAPPart();

			SOAPEnvelope soapEnv = soapPart.getEnvelope();
			soapEnv.addNamespaceDeclaration("ec", nameSpace);

			SOAPBody soapBod = soapEnv.getBody();
			
			SOAPElement soapEle = soapBod.addChildElement("validarComprobante", "ec");
			SOAPElement soapEle1 = soapEle.addChildElement("xml");
			soapEle1.addTextNode(fileString);

			soapMes.saveChanges();
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return soapMes;
	}
	
	private static SOAPMessage createSoapReqAutXML(String nameSpace, String claveAcceso) {
		MessageFactory messageFac = null;
		SOAPMessage soapMes = null;
		
		try {
			messageFac = MessageFactory.newInstance();
			soapMes = messageFac.createMessage();
			SOAPPart soapPart = soapMes.getSOAPPart();

			SOAPEnvelope soapEnv = soapPart.getEnvelope();
			soapEnv.addNamespaceDeclaration("ec", nameSpace);

			SOAPBody soapBod = soapEnv.getBody();
			
			SOAPElement soapEle = soapBod.addChildElement("autorizacionComprobante", "ec");
		    SOAPElement soapEle1 = soapEle.addChildElement("claveAccesoComprobante");
		    soapEle1.addTextNode(claveAcceso);

			soapMes.saveChanges();
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return soapMes;
	}
}
