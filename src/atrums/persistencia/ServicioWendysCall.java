package atrums.persistencia;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.log4j.Logger;

import sun.net.www.protocol.http.HttpURLConnection;

public class ServicioWendysCall {
    static final Logger log = Logger.getLogger(ServicioWendysCall.class);
    private final String urlBrowser;
    private final Hashtable<String, String> parametros;
    private final String operacion;
    
    public ServicioWendysCall(String url, Hashtable<String, String> parametros, String operacion){
        this.urlBrowser = url;
        this.parametros = parametros;
        this.operacion = operacion;
    }
    
    public SOAPMessage Call(){
    	log.info("Utilizando servicio de consultas a Service Web - Operacion: " + this.operacion);
    	
        SOAPMessage soapMes = null;
        
        try {
            SOAPConnectionFactory soapConF = SOAPConnectionFactory.newInstance();
            SOAPConnection soapCon = soapConF.createConnection();
            
            URL endpoint = new URL(null, this.urlBrowser, new URLStreamHandler() {
				
				@Override
				protected URLConnection openConnection(URL u) throws IOException {
					// TODO Auto-generated method stub
					URL target = new URL(u.toString());
					HttpURLConnection connection = (HttpURLConnection) target.openConnection();
					connection.setUseCaches(true);
					connection.setDefaultUseCaches(true);
					
					connection.setConnectTimeout(60000);
					connection.setReadTimeout(60000);
					return(connection);
				}
			});
            
            soapMes = soapCon.call(createSoap(), endpoint);
            
            endpoint = null;
            soapCon.close();  log.info("Close BDD o Statement");
            soapConF = null;
        } catch (SOAPException ex) {
            log.error(ex.getMessage());
        } catch (MalformedURLException ex) {
			// TODO Auto-generated catch block
            log.error(ex.getMessage());
		} 
        
        //log.info("Utilizando servicio de consultas a Service Web - Operacion: " + this.operacion + " - Retornado consulta");\
        this.parametros.clear();
        return soapMes;
    }
    
    private SOAPMessage createSoap(){
        SOAPMessage soapMes = null;
        
        try {
            MessageFactory messageFac =  MessageFactory.newInstance();
            soapMes = messageFac.createMessage();
            soapMes.setProperty(SOAPMessage.WRITE_XML_DECLARATION, "true");
            SOAPPart soapPart = soapMes.getSOAPPart();
            SOAPEnvelope soapEnv = soapPart.getEnvelope();
            SOAPBody soapBod = soapEnv.getBody();
            MimeHeaders mimeHead = soapMes.getMimeHeaders();
            mimeHead.setHeader("SOAPAction", this.operacion);
            SOAPHeader soapHeader = soapEnv.getHeader();
            soapHeader.recycleNode();
            
            if(!parametros.isEmpty()){
                SOAPFactory soapFactory = SOAPFactory.newInstance();
                
                int posicionBuscar = 0;
                int posicionEncontrada = 0;
                while(posicionBuscar != -1){
                    posicionBuscar = this.operacion.indexOf("/",posicionBuscar + 1);
                    if(posicionBuscar != -1){
                        posicionEncontrada = posicionBuscar;
                    }
                }
                
                Name bodyName  = soapFactory.createName(
                        this.operacion.substring(posicionEncontrada + 1, this.operacion.length()), 
                        "",
                        this.operacion.substring(0, posicionEncontrada+1));
                
                SOAPBodyElement soapBodyEle = soapBod.addBodyElement(bodyName);
                
                Enumeration<String> enumeration = parametros.keys();
                while(enumeration.hasMoreElements()){
                    String aux = enumeration.nextElement();
                    
                    //Name name = soapFactory.createName(aux);
                    //SOAPElement soapElement = soapBodyEle.addChildElement(name);
                    SOAPElement soapElement = soapBodyEle.addChildElement(aux, "");
                    soapElement.addTextNode(parametros.get(aux));
                }
            }
            
            soapMes.saveChanges();
        } catch (SOAPException ex) {
            log.error(ex.getMessage());
        }
        return soapMes;
    }
}
