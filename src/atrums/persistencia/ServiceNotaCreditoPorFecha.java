package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.modelo.Servicio.ServDocumentosNroNroEsNroEm;

public class ServiceNotaCreditoPorFecha {
	static final Logger log = Logger.getLogger(ServiceNotaCreditoPorFecha.class);
	private final ConfService confService;
	private String fecha;
    
    public ServiceNotaCreditoPorFecha(ConfService confService, String fecha){
        this.confService = confService;
        this.fecha = fecha;
    }
    
    public List<ServDocumentoNroNroEsNroEm> callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenNotaCreditoPorFecha());
        
        ServDocumentosNroNroEsNroEm documentosNroNroEsNroEm = new ServDocumentosNroNroEsNroEm(swendysCall.Call(), this.fecha);
        
        return documentosNroNroEsNroEm.getDocumentos();
    }
}
