package atrums.persistencia;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.BDDOpenbravo.BDDOadClient;
import atrums.modelo.BDDOpenbravo.BDDOadOrg;
import atrums.modelo.BDDOpenbravo.BDDOadRole;
import atrums.modelo.BDDOpenbravo.BDDOadRoleOrgaccess;
import atrums.modelo.BDDOpenbravo.BDDOadUserEmail;
import atrums.modelo.BDDOpenbravo.BDDOadUserRoles;
import atrums.modelo.BDDOpenbravo.BDDOadWindowAccess;
import atrums.modelo.BDDOpenbravo.BDDOcDoctype;
import atrums.modelo.BDDOpenbravo.BDDOcInvoice;
import atrums.modelo.BDDOpenbravo.BDDOcInvoiceLine;
import atrums.modelo.BDDOpenbravo.BDDOcLocation;
import atrums.modelo.BDDOpenbravo.BDDOcPaymentterm;
import atrums.modelo.BDDOpenbravo.BDDOcPocConfiguration;
import atrums.modelo.BDDOpenbravo.BDDOcTax;
import atrums.modelo.BDDOpenbravo.BDDOcTaxcategory;
import atrums.modelo.BDDOpenbravo.BDDOcbGroup;
import atrums.modelo.BDDOpenbravo.BDDOcbPartner;
import atrums.modelo.BDDOpenbravo.BDDOcbPartnerLocation;
import atrums.modelo.BDDOpenbravo.BDDOcoBpRetencionCompra;
import atrums.modelo.BDDOpenbravo.BDDOcoRetencionCompra;
import atrums.modelo.BDDOpenbravo.BDDOcoRetencionCompraLinea;
import atrums.modelo.BDDOpenbravo.BDDOcoTipoRetencion;
import atrums.modelo.BDDOpenbravo.BDDOmInout;
import atrums.modelo.BDDOpenbravo.BDDOmInoutLine;
import atrums.modelo.BDDOpenbravo.BDDOmPricelist;
import atrums.modelo.BDDOpenbravo.BDDOmProduct;
import atrums.modelo.BDDOpenbravo.BDDOmProductCategory;

public class OperacionesBDDOpenbravo implements OperacionesBDD{
	static final Logger log = Logger.getLogger(OperacionesBDDOpenbravo.class);
	private ConfBDDOpenbravo confBDDOpenbravo;
	private BDDEmpresaPrincipal empresaPrincipal;
	private ConfService confService;
	
	private BDDOadClient oadClient = new BDDOadClient();
	private BDDOadOrg oadOrg = new BDDOadOrg();
	private BDDOcDoctype ocDoctype = new BDDOcDoctype();
	private BDDOcbPartner ocbPartner = new BDDOcbPartner();
	private BDDOcbPartner ocbTransp = new BDDOcbPartner();
	private BDDOcbGroup ocbGroup = new BDDOcbGroup();
	private BDDOadUserEmail oadUserEmail = new BDDOadUserEmail();
	private BDDOcLocation ocLocation = new BDDOcLocation();
	private BDDOcLocation ocLocationPar = new BDDOcLocation();
	private BDDOcLocation ocLocationDes = new BDDOcLocation();
	private BDDOcbPartnerLocation ocbPartnerLocation = new BDDOcbPartnerLocation();
	private BDDOcInvoice ocInvoice = new BDDOcInvoice();
	private BDDOcPaymentterm ocPaymentterm = new BDDOcPaymentterm();
	private BDDOmPricelist omPricelist = new BDDOmPricelist();
	private BDDOmProductCategory omProductCategory = new BDDOmProductCategory();
	private BDDOcTaxcategory ocTaxcategory = new BDDOcTaxcategory();
	private BDDOcTax ocTax = new BDDOcTax();
	private BDDOmProduct omProduct = new BDDOmProduct();
	private BDDOcInvoiceLine ocInvoiceLine = new BDDOcInvoiceLine();
	private BDDOmInoutLine omInoutLine = new BDDOmInoutLine();
	private BDDOadRole oadRole = new BDDOadRole();
	private BDDOadRoleOrgaccess oadRoleOrgaccess = new BDDOadRoleOrgaccess();
	private BDDOadWindowAccess oadWindowAccess = new BDDOadWindowAccess();
	private BDDOadUserRoles oadUserRoles = new BDDOadUserRoles();
	private BDDOcoRetencionCompra ocoRetencionCompra = new BDDOcoRetencionCompra();
	private BDDOcoTipoRetencion ocoTipoRetencion = new BDDOcoTipoRetencion();
	private BDDOcoBpRetencionCompra ocoBpRetencionCompra = new BDDOcoBpRetencionCompra();
	private BDDOcoRetencionCompraLinea ocoRetencionCompraLinea = new BDDOcoRetencionCompraLinea();
	private BDDOcPocConfiguration ocPocConfiguration = new BDDOcPocConfiguration();
	private BDDOmInout omInout = new BDDOmInout();
	private BDDDocumentoBase documentoBase;
	
	public BDDOmInoutLine getOmInoutLine() {
		return omInoutLine;
	}

	public void setOmInoutLine(BDDOmInoutLine omInoutLine) {
		this.omInoutLine = omInoutLine;
	}

	public BDDOmInout getOmInout() {
		return omInout;
	}

	public void setOmInout(BDDOmInout omInout) {
		this.omInout = omInout;
	}
	
	public OperacionesBDDOpenbravo(ConfBDDOpenbravo confBDDOpenbravo){
		this.confBDDOpenbravo = confBDDOpenbravo;
	}
	
	public BDDOcLocation getOcLocationPar() {
		return ocLocationPar;
	}

	public void setOcLocationPar(BDDOcLocation ocLocationPar) {
		this.ocLocationPar = ocLocationPar;
	}

	public BDDOcLocation getOcLocationDes() {
		return ocLocationDes;
	}

	public void setOcLocationDes(BDDOcLocation ocLocationDes) {
		this.ocLocationDes = ocLocationDes;
	}

	public BDDOcbPartner getOcbTransp() {
		return ocbTransp;
	}

	public void setOcbTransp(BDDOcbPartner ocbTransp) {
		this.ocbTransp = ocbTransp;
	}

	public BDDOcPocConfiguration getOcPocConfiguration() {
		return ocPocConfiguration;
	}

	public void setOcPocConfiguration(BDDOcPocConfiguration ocPocConfiguration) {
		this.ocPocConfiguration = ocPocConfiguration;
	}

	public BDDOcoRetencionCompraLinea getOcoRetencionCompraLinea() {
		return ocoRetencionCompraLinea;
	}

	public void setOcoRetencionCompraLinea(
			BDDOcoRetencionCompraLinea ocoRetencionCompraLinea) {
		this.ocoRetencionCompraLinea = ocoRetencionCompraLinea;
	}

	public BDDOcoTipoRetencion getOcoTipoRetencion() {
		return ocoTipoRetencion;
	}

	public void setOcoTipoRetencion(BDDOcoTipoRetencion ocoTipoRetencion) {
		this.ocoTipoRetencion = ocoTipoRetencion;
	}

	public OperacionesBDDOpenbravo(ConfBDDOpenbravo confBDDOpenbravo, 
			BDDEmpresaPrincipal empresaPrincipal, 
			ConfService confService) {
		this.confBDDOpenbravo = confBDDOpenbravo;
		this.empresaPrincipal = empresaPrincipal;
		this.confService = confService;
	}
	
	public BDDOcoBpRetencionCompra getOcoBpRetencionCompra() {
		return ocoBpRetencionCompra;
	}

	public void setOcoBpRetencionCompra(BDDOcoBpRetencionCompra ocoBpRetencionCompra) {
		this.ocoBpRetencionCompra = ocoBpRetencionCompra;
	}

	public BDDOadUserRoles getOadUserRoles() {
		return oadUserRoles;
	}

	public void setOadUserRoles(BDDOadUserRoles oadUserRoles) {
		this.oadUserRoles = oadUserRoles;
	}

	public BDDOadWindowAccess getOadWindowAccess() {
		return oadWindowAccess;
	}

	public void setOadWindowAccess(BDDOadWindowAccess oadWindowAccess) {
		this.oadWindowAccess = oadWindowAccess;
	}

	public BDDOadRoleOrgaccess getOadRoleOrgaccess() {
		return oadRoleOrgaccess;
	}

	public void setOadRoleOrgaccess(BDDOadRoleOrgaccess oadRoleOrgaccess) {
		this.oadRoleOrgaccess = oadRoleOrgaccess;
	}

	public BDDOadRole getOadRole() {
		return oadRole;
	}

	public void setOadRole(BDDOadRole oadRole) {
		this.oadRole = oadRole;
	}

	public BDDOcInvoiceLine getOcInvoiceLine() {
		return ocInvoiceLine;
	}

	public void setOcInvoiceLine(BDDOcInvoiceLine ocInvoiceLine) {
		this.ocInvoiceLine = ocInvoiceLine;
	}

	public BDDOmProduct getOmProduct() {
		return omProduct;
	}

	public void setOmProduct(BDDOmProduct omProduct) {
		this.omProduct = omProduct;
	}

	public BDDOcTax getOcTax() {
		return ocTax;
	}

	public void setOcTax(BDDOcTax ocTax) {
		this.ocTax = ocTax;
	}

	public BDDOcTaxcategory getOcTaxcategory() {
		return ocTaxcategory;
	}

	public void setOcTaxcategory(BDDOcTaxcategory ocTaxcategory) {
		this.ocTaxcategory = ocTaxcategory;
	}

	public BDDOcPaymentterm getOcPaymentterm() {
		return ocPaymentterm;
	}

	public void setOcPaymentterm(BDDOcPaymentterm ocPaymentterm) {
		this.ocPaymentterm = ocPaymentterm;
	}

	public BDDOmPricelist getOmPricelist() {
		return omPricelist;
	}

	public void setOmPricelist(BDDOmPricelist omPricelist) {
		this.omPricelist = omPricelist;
	}

	public BDDOmProductCategory getOmProductCategory() {
		return omProductCategory;
	}

	public void setOmProductCategory(BDDOmProductCategory omProductCategory) {
		this.omProductCategory = omProductCategory;
	}

	public BDDOcInvoice getOcInvoice() {
		return ocInvoice;
	}

	public void setOcInvoice(BDDOcInvoice ocInvoice) {
		this.ocInvoice = ocInvoice;
	}

	public BDDOcbPartnerLocation getOcbPartnerLocation() {
		return ocbPartnerLocation;
	}

	public void setOcbPartnerLocation(BDDOcbPartnerLocation ocbPartnerLocation) {
		this.ocbPartnerLocation = ocbPartnerLocation;
	}

	public BDDOcLocation getOcLocation() {
		return ocLocation;
	}

	public void setOcLocation(BDDOcLocation ocLocation) {
		this.ocLocation = ocLocation;
	}

	public BDDOcbGroup getOcbGroup() {
		return ocbGroup;
	}

	public void setOcbGroup(BDDOcbGroup ocbGroup) {
		this.ocbGroup = ocbGroup;
	}

	public BDDOadUserEmail getOadUserEmail() {
		return oadUserEmail;
	}

	public void setOadUserEmail(BDDOadUserEmail oadUserEmail) {
		this.oadUserEmail = oadUserEmail;
	}

	public BDDOcbPartner getOcbPartner() {
		return ocbPartner;
	}

	public void setOcbPartner(BDDOcbPartner ocbPartner) {
		this.ocbPartner = ocbPartner;
	}
	
	public BDDOcDoctype getOcDoctype() {
		return ocDoctype;
	}

	public void setOcDoctype(BDDOcDoctype ocDoctype) {
		this.ocDoctype = ocDoctype;
	}

	public BDDOadOrg getOadOrg() {
		return oadOrg;
	}

	public void setOadOrg(BDDOadOrg oadOrg) {
		this.oadOrg = oadOrg;
	}

	public BDDDocumentoBase getDocumentoBase() {
		return documentoBase;
	}

	public void setDocumentoBase(BDDDocumentoBase documentoBase) {
		this.documentoBase = documentoBase;
	}

	public BDDOadClient getOadClient() {
		return oadClient;
	}

	public void setOadClient(BDDOadClient oadClient) {
		this.oadClient = oadClient;
	}

	public BDDOcoRetencionCompra getOcoRetencionCompra() {
		return ocoRetencionCompra;
	}

	public void setOcoRetencionCompra(BDDOcoRetencionCompra ocoRetencionCompra) {
		this.ocoRetencionCompra = ocoRetencionCompra;
	}
	
	@Override
	public boolean guardarDocumento(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDDocumentoBaseProcesar(String tipoDocumento, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			
			if(connection.isClosed()){
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
		
		return connection;
	}

	@Override
	public boolean existeDato(String sql, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		int total = 0;
		
		try {
			ResultSet rs;
			statement = connection.createStatement();
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				total = rs.getInt("total");
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex);
			total = 1;
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(total > 0){
			return false;
		}else{
			return true;
		}
	}
	

	@Override
	public boolean getEmpresaPrincipal(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.empresaPrincipal.setId(null);
			sql = "SELECT ao.ad_org_id, "
					+ "(coalesce(l.address1,coalesce(l.address2,'')) || ', ' || coalesce(l.city,'') || '-' || coalesce(c.name,'')) as direccion, "
					+ "(coalesce(ac.em_atecfe_tipoambiente,'1')) as ambiente, "
					+ "(coalesce(ac.em_atecfe_tipoemisi,'1')) as tipoEmision, "
					+ "ac.name, "
					+ "(coalesce(ac.em_atecfe_codinumerico, '1234')) as codNumerico, "
					+ "em_atecfe_numresolsri as numResolucion, "
					+ "(coalesce(ac.value, ac.description)) as nombreComercial, "
					+ "(coalesce(em_atecfe_obligcontabi, 'N')) as oblContabili, "
					+ "oi.taxid as ruc "
					+ "FROM ad_org ao "
					+ "LEFT JOIN ad_orginfo oi ON (ao.ad_org_id = oi.ad_org_id) "
					+ "LEFT JOIN c_location l ON (oi.c_location_id = l.c_location_id) "
					+ "LEFT JOIN c_country c ON (l.c_country_id = c.c_country_id) "
					+ "LEFT JOIN ad_client ac ON (ao.ad_client_id = ac.ad_client_id) "
					+ "WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' "
					+ "AND ao.em_co_nro_estab = '000' "
					+ "AND ao.em_co_punto_emision = '000';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.empresaPrincipal.setId(rs.getString("ad_org_id"));
				this.empresaPrincipal.setDireccion(rs.getString("direccion"));
				this.empresaPrincipal.setAmbiente(rs.getString("ambiente"));
				this.empresaPrincipal.setTipoEmision(rs.getString("tipoEmision"));
				this.empresaPrincipal.setRazonSocial(rs.getString("name"));
				this.empresaPrincipal.setNombreComercial(rs.getString("nombreComercial"));
				this.empresaPrincipal.setRuc(rs.getString("ruc"));
				this.empresaPrincipal.setCodNumerico(rs.getString("codNumerico"));
				this.empresaPrincipal.setNumResolucion(rs.getString("numResolucion"));
				this.empresaPrincipal.setObliContabi(rs.getString("oblContabili").equals("Y") ? true : false);
			}
			
			if(confService.isUsarDatos()){
				this.empresaPrincipal.setDireccion(confService.getDireccion());
				this.empresaPrincipal.setAmbiente(confService.getAmbiente());
				this.empresaPrincipal.setTipoEmision("1");
				this.empresaPrincipal.setRazonSocial(confService.getRazonSocial());
				this.empresaPrincipal.setNombreComercial(confService.getNombreComercial());
				this.empresaPrincipal.setRuc(confService.getRuc());
				this.empresaPrincipal.setCodNumerico(confService.getCodNumerico());
				this.empresaPrincipal.setNumResolucion(confService.getNumResolucion());
				this.empresaPrincipal.setObliContabi(confService.isObliContabi());
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.empresaPrincipal.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	public BDDEmpresaPrincipal getEmpresaPrincipal() {
		return empresaPrincipal;
	}

	public void setEmpresaPrincipal(BDDEmpresaPrincipal empresaPrincipal) {
		this.empresaPrincipal = empresaPrincipal;
	}

	@Override
	public boolean actualizarEstado(Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean getBDDDocumentoBaseMigrar(String tipoDocumento, Connection connection) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean existeDocumentoImporFCNC(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		boolean existe = true;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT ci.em_atecfe_codigo_acc FROM c_invoice AS ci WHERE ci.em_atecfe_codigo_acc = '" + this.documentoBase.getClaveacceso() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				existe = false;
			}
			
			rs = null;
			sql = "SELECT co.em_atecfe_codigo_acc FROM co_retencion_compra AS co WHERE co.em_atecfe_codigo_acc = '" + this.documentoBase.getClaveacceso() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				existe = false;
			}
			
			rs = null;
			sql = "SELECT mi.em_atecfe_codigo_acc FROM m_inout AS mi WHERE mi.em_atecfe_codigo_acc = '" + this.documentoBase.getClaveacceso() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				existe = false;
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		return existe;
	}
	
	@Override
	public boolean getBDDOadClient(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.oadClient.setId(null);
			sql = "SELECT ao.ad_client_id, ao.name "
					+ "FROM ad_client AS ao "
					+ "WHERE upper(ao.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.oadClient.setId(rs.getString("ad_client_id"));
				this.oadClient.setName(rs.getString("name"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.oadClient.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOadOrg(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.oadOrg.setId(null);
			sql = "SELECT ao.ad_org_id, ao.name "
					+ "FROM ad_org AS ao "
					+ "WHERE ao.em_co_nro_estab = '" + this.documentoBase.getNroestablecimiento() + "' "
					+ "AND ao.em_co_punto_emision = '" + this.documentoBase.getNroemision() + "' "
					+ "AND isactive = 'Y' "
					+ "AND ao.ad_client_id = '" + this.oadClient.getId() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.oadOrg.setId(rs.getString("ad_org_id"));
				this.oadOrg.setName(rs.getString("name"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.oadOrg.getId() != null){
			return true;
		}else{
			return false;
		}
	}
	
	
	@Override
	public boolean getBDDOcDoctype(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			this.ocDoctype.setId(null);
			sql = "SELECT cd.c_doctype_id, cd.name "
					+ "FROM c_doctype AS cd "
					+ "WHERE cd.ad_org_id = '" + this.oadOrg.getId() + "' "
					+ "AND cd.ad_client_id = '" + this.oadClient.getId() + "' "
					+ (this.documentoBase.getTipodocumento().equals("FC") ? "AND upper(cd.name) like '%FACTURA%' AND upper(cd.name) like '%VENTA%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("FV") ? "AND upper(cd.name) like '%FACTURA%' AND upper(cd.name) like '%VENTA%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("NC") ? "AND upper(cd.name) like '%NOTA%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("RT") ? "AND upper(cd.name) like '%RETENCIÓN%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("FCP") ? "AND upper(cd.name) like '%FACTURA%' AND upper(cd.name) like '%VENTA%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("NCP") ? "AND upper(cd.name) like '%NOTA%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("RTP") ? "AND upper(cd.name) like '%RETENCIÓN%'" : "")
					+ (this.documentoBase.getTipodocumento().equals("GR") ? "AND upper(cd.name) like '%GUIA%'" : "")
					+ ";";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocDoctype.setId(rs.getString("c_doctype_id"));
				this.ocDoctype.setName(rs.getString("name"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocDoctype.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOcbPartner(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.ocbPartner.setId(null);
		
		String adress = "";
		if(this.ocLocation.getDireccion().length() > 60){
			adress = this.ocLocation.getDireccion().substring(0, 60);
		}else{
			adress = this.ocLocation.getDireccion();
		}
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			sql = "SELECT c_location_id, address1 FROM c_location WHERE address1 = initcap('" + adress + "') LIMIT 1;";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			String locationid = null;
			
			while (rs.next()){
				locationid = rs.getString("c_location_id");
			}
			
			if(locationid == null){
				sql = "INSERT INTO c_location("
						+ "c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, address1, address2, city, postal, postal_add, "
						+ "c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, "
						+ "em_co_barrio) "
						+ "VALUES ("
						+ "'" + this.ocLocation.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
						+ "now(), '100', initcap('" + adress + "'), null, null, null, null, "
						+ "(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, "
						+ "null);";
				statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			}else{
				this.ocLocation.setId(locationid);
			}
			
			rs = null;
			sql = "SELECT cb.c_bpartner_id FROM c_bpartner AS cb WHERE cb.taxid= '" + this.ocbPartner.getIdentificacion() + "' LIMIT 1";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocbPartner.setId(rs.getString("c_bpartner_id"));
			}
			
			sql = "UPDATE c_bpartner_location SET c_location_id = '" + this.ocLocation.getId() + "' WHERE c_bpartner_id = '" + this.ocbPartner.getId() + "';";
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "UPDATE ad_user SET email = " + "lower('" + this.oadUserEmail.getEmail() + "') WHERE c_bpartner_id = '" + this.ocbPartner.getId() + "';";
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			this.ocbPartner.setId(null);
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocbPartner.getId() != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOcLocationPar(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		String adress = "";
		if(this.ocLocationPar.getDireccion().length() > 60){
			adress = this.ocLocationPar.getDireccion().substring(0, 60);
		}else{
			adress = this.ocLocationPar.getDireccion();
		}
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			sql = "SELECT c_location_id, address1 FROM c_location WHERE address1 = initcap('" + adress + "') LIMIT 1;";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			String locationid = null;
			
			while (rs.next()){
				locationid = rs.getString("c_location_id");
			}
			
			if(locationid == null){
				sql = "INSERT INTO c_location("
						+ "c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, address1, address2, city, postal, postal_add, "
						+ "c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, "
						+ "em_co_barrio) "
						+ "VALUES ("
						+ "'" + this.ocLocationPar.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
						+ "now(), '100', initcap('" + adress + "'), null, null, null, null, "
						+ "(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, "
						+ "null);";
				statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			}else{
				this.ocLocationPar.setId(locationid);
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			this.ocLocationPar.setId(null);
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocLocationPar.getId() != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOcLocationDes(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		
		String adress = "";
		if(this.ocLocationDes.getDireccion().length() > 60){
			adress = this.ocLocationDes.getDireccion().substring(0, 60);
		}else{
			adress = this.ocLocationDes.getDireccion();
		}
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			sql = "SELECT c_location_id, address1 FROM c_location WHERE address1 = initcap('" + adress + "') LIMIT 1;";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			String locationid = null;
			
			while (rs.next()){
				locationid = rs.getString("c_location_id");
			}
			
			if(locationid == null){
				sql = "INSERT INTO c_location("
						+ "c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, address1, address2, city, postal, postal_add, "
						+ "c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, "
						+ "em_co_barrio) "
						+ "VALUES ("
						+ "'" + this.ocLocationDes.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
						+ "now(), '100', initcap('" + adress + "'), null, null, null, null, "
						+ "(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, "
						+ "null);";
				statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			}else{
				this.ocLocationDes.setId(locationid);
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			this.ocLocationPar.setId(null);
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocLocationPar.getId() != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean getBDDOcbPartnerTrans(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.ocbTransp.setId(null);
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();

			rs = null;
			sql = "SELECT cb.c_bpartner_id FROM c_bpartner AS cb WHERE cb.taxid= '" + this.ocbTransp.getIdentificacion() + "' LIMIT 1";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocbTransp.setId(rs.getString("c_bpartner_id"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			this.ocbPartner.setId(null);
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocbPartner.getId() != null){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOcbPartner(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			if(this.ocbPartner.getTipo().equals("04")){
				this.ocbPartner.setTipo("01");
			}else if(this.ocbPartner.getTipo().equals("05")){
				this.ocbPartner.setTipo("02");
			}else if(this.ocbPartner.getTipo().equals("06")){
				this.ocbPartner.setTipo("03");
			}else if(this.ocbPartner.getTipo().equals("07")){
				this.ocbPartner.setTipo("07");
			}
			
			
			String nombreComercial = "";
			if(this.ocbPartner.getNombreComercial().length() > 60){
				nombreComercial = this.ocbPartner.getNombreComercial().substring(0, 60);
			}else{
				nombreComercial = this.ocbPartner.getNombreComercial();
			}
			
			String razonSocial = "";
			if(this.ocbPartner.getRazonSocial().length() > 60){
				razonSocial = this.ocbPartner.getRazonSocial().substring(0, 60);
			}else{
				razonSocial = this.ocbPartner.getRazonSocial();
			}
			
			statement = connection.createStatement();
			sql = "INSERT INTO c_bpartner("
					+ "c_bpartner_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, value, name, name2, description, issummary, "
					+ "c_bp_group_id, isonetime, isprospect, isvendor, iscustomer, isemployee, "
					+ "issalesrep, referenceno, duns, url, ad_language, taxid, istaxexempt, "
					+ "c_invoiceschedule_id, rating, salesvolume, numberemployees, naics, "
					+ "firstsale, acqusitioncost, potentiallifetimevalue, actuallifetimevalue, "
					+ "shareofcustomer, paymentrule, so_creditlimit, so_creditused, "
					+ "c_paymentterm_id, m_pricelist_id, isdiscountprinted, so_description, "
					+ "poreference, paymentrulepo, po_pricelist_id, po_paymentterm_id, "
					+ "documentcopies, c_greeting_id, invoicerule, deliveryrule, deliveryviarule, "
					+ "salesrep_id, bpartner_parent_id, socreditstatus, ad_forced_org_id, "
					+ "showpriceinorder, invoicegrouping, fixmonthday, fixmonthday2, "
					+ "fixmonthday3, isworker, upc, c_salary_category_id, invoice_printformat, "
					+ "last_days, po_bankaccount_id, po_bp_taxcategory_id, po_fixmonthday, "
					+ "po_fixmonthday2, po_fixmonthday3, so_bankaccount_id, so_bp_taxcategory_id, "
					+ "fiscalcode, isofiscalcode, po_c_incoterms_id, so_c_incoterms_id, "
					+ "fin_paymentmethod_id, po_paymentmethod_id, fin_financial_account_id, "
					+ "po_financial_account_id, customer_blocking, vendor_blocking, "
					+ "so_payment_blocking, po_payment_blocking, so_invoice_blocking, "
					+ "po_invoice_blocking, so_order_blocking, po_order_blocking, so_goods_blocking, "
					+ "po_goods_blocking, iscashvat, em_co_tipocontrib, em_co_tipo_identificacion, "
					+ "em_co_bp_nro_aut_fc_sri, em_co_bp_fec_venct_aut_fc_sri, em_co_bp_nro_estab, "
					+ "em_co_bp_punto_emision, em_co_bp_nro_aut_rt_sri, em_co_bp_fec_venct_aut_rt_sri, "
					+ "em_co_email, em_co_niv_edu, em_co_vivienda, em_co_anios_res, "
					+ "em_co_meses_res, em_co_total_meses, em_co_fechanac, em_co_ciudadnac, "
					+ "em_co_nacionalidad, em_co_nombres, em_co_apellidos, em_co_com_tarj_cred, "
					+ "em_co_natural_juridico, "
					+ "em_no_fechaingreso, "
					+ "em_no_fechasalida, em_no_estadocivil, em_no_genero, em_no_fechanacimiento, "
					+ "em_no_area_empresa_id, em_no_motivo_salida, em_no_sissalnet, "
					+ "em_no_isdiscapacitado, em_no_istercera_edad)"
					+ "VALUES ("
					+ "'" + this.ocbPartner.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', '" + this.ocbPartner.getIdentificacion() + "', initcap('" + nombreComercial + "'), initcap('" + razonSocial + "'), initcap('" + nombreComercial + "'), 'N', "
					+ "(SELECT c_bp_group_id FROM c_bp_group AS cbg WHERE upper(cbg.name) LIKE '%CLIENTES COMERCIALES%' LIMIT 1), 'N', 'N', 'N', 'Y', 'N', "
					+ "'N', null, null, null, 'es_EC', '" + this.ocbPartner.getIdentificacion() + "', 'N', "
					+ "null, null, null, null, null, "
					+ "null, '0', '0', '0', "
					+ "null, null, '0', '0', "
					+ "null, null, 'N', null, "
					+ "null, null, null, null, "
					+ "null, null, 'I', null, null, "
					+ "null, null, 'O', null, "
					+ "'Y', '000000000000000', null, null, "
					+ "null, 'N', null, null, null, "
					+ "'100', null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "null, 'N', 'N', "
					+ "'N', 'Y', 'Y', "
					+ "'Y', 'Y', 'Y', 'Y', "
					+ "'N', 'N', null, '" + this.ocbPartner.getTipo() + "', "
					+ "null, null, null, "
					+ "null, null, null, "
					+ "lower('" + this.ocbPartner.getEmail() + "'), null, null, null, "
					+ "null, null, null, null, "
					+ "null, initcap('" + nombreComercial + "'), initcap('" + nombreComercial + "'), null, "
					+ "'PN', "
					+ "null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "'N', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage() + "; Error Insertando Cliente en Openbravo: " + this.ocbPartner.getIdentificacion());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOcbPartnerTrans(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			if(this.ocbTransp.getTipo().equals("04")){
				this.ocbTransp.setTipo("01");
			}else if(this.ocbTransp.getTipo().equals("05")){
				this.ocbTransp.setTipo("02");
			}else if(this.ocbTransp.getTipo().equals("06")){
				this.ocbTransp.setTipo("03");
			}else if(this.ocbTransp.getTipo().equals("07")){
				this.ocbTransp.setTipo("07");
			}
			
			
			String nombreComercial = "";
			if(this.ocbTransp.getNombreComercial().length() > 60){
				nombreComercial = this.ocbTransp.getNombreComercial().substring(0, 60);
			}else{
				nombreComercial = this.ocbTransp.getNombreComercial();
			}
			
			String razonSocial = "";
			if(this.ocbTransp.getRazonSocial().length() > 60){
				razonSocial = this.ocbTransp.getRazonSocial().substring(0, 60);
			}else{
				razonSocial = this.ocbTransp.getRazonSocial();
			}
			
			statement = connection.createStatement();
			sql = "INSERT INTO c_bpartner("
					+ "c_bpartner_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, value, name, name2, description, issummary, "
					+ "c_bp_group_id, isonetime, isprospect, isvendor, iscustomer, isemployee, "
					+ "issalesrep, referenceno, duns, url, ad_language, taxid, istaxexempt, "
					+ "c_invoiceschedule_id, rating, salesvolume, numberemployees, naics, "
					+ "firstsale, acqusitioncost, potentiallifetimevalue, actuallifetimevalue, "
					+ "shareofcustomer, paymentrule, so_creditlimit, so_creditused, "
					+ "c_paymentterm_id, m_pricelist_id, isdiscountprinted, so_description, "
					+ "poreference, paymentrulepo, po_pricelist_id, po_paymentterm_id, "
					+ "documentcopies, c_greeting_id, invoicerule, deliveryrule, deliveryviarule, "
					+ "salesrep_id, bpartner_parent_id, socreditstatus, ad_forced_org_id, "
					+ "showpriceinorder, invoicegrouping, fixmonthday, fixmonthday2, "
					+ "fixmonthday3, isworker, upc, c_salary_category_id, invoice_printformat, "
					+ "last_days, po_bankaccount_id, po_bp_taxcategory_id, po_fixmonthday, "
					+ "po_fixmonthday2, po_fixmonthday3, so_bankaccount_id, so_bp_taxcategory_id, "
					+ "fiscalcode, isofiscalcode, po_c_incoterms_id, so_c_incoterms_id, "
					+ "fin_paymentmethod_id, po_paymentmethod_id, fin_financial_account_id, "
					+ "po_financial_account_id, customer_blocking, vendor_blocking, "
					+ "so_payment_blocking, po_payment_blocking, so_invoice_blocking, "
					+ "po_invoice_blocking, so_order_blocking, po_order_blocking, so_goods_blocking, "
					+ "po_goods_blocking, iscashvat, em_co_tipocontrib, em_co_tipo_identificacion, "
					+ "em_co_bp_nro_aut_fc_sri, em_co_bp_fec_venct_aut_fc_sri, em_co_bp_nro_estab, "
					+ "em_co_bp_punto_emision, em_co_bp_nro_aut_rt_sri, em_co_bp_fec_venct_aut_rt_sri, "
					+ "em_co_email, em_co_niv_edu, em_co_vivienda, em_co_anios_res, "
					+ "em_co_meses_res, em_co_total_meses, em_co_fechanac, em_co_ciudadnac, "
					+ "em_co_nacionalidad, em_co_nombres, em_co_apellidos, em_co_com_tarj_cred, "
					+ "em_co_natural_juridico, "
					+ "em_no_fechaingreso, "
					+ "em_no_fechasalida, em_no_estadocivil, em_no_genero, em_no_fechanacimiento, "
					+ "em_no_area_empresa_id, em_no_motivo_salida, em_no_sissalnet, "
					+ "em_no_isdiscapacitado, em_no_istercera_edad)"
					+ "VALUES ("
					+ "'" + this.ocbTransp.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', '" + this.ocbTransp.getIdentificacion() + "', initcap('" + nombreComercial + "'), initcap('" + razonSocial + "'), initcap('" + nombreComercial + "'), 'N', "
					+ "(SELECT c_bp_group_id FROM c_bp_group AS cbg WHERE upper(cbg.name) LIKE '%CLIENTES COMERCIALES%' LIMIT 1), 'N', 'N', 'N', 'Y', 'N', "
					+ "'N', null, null, null, 'es_EC', '" + this.ocbTransp.getIdentificacion() + "', 'N', "
					+ "null, null, null, null, null, "
					+ "null, '0', '0', '0', "
					+ "null, null, '0', '0', "
					+ "null, null, 'N', null, "
					+ "null, null, null, null, "
					+ "null, null, 'I', null, null, "
					+ "null, null, 'O', null, "
					+ "'Y', '000000000000000', null, null, "
					+ "null, 'N', null, null, null, "
					+ "'100', null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "null, 'N', 'N', "
					+ "'N', 'Y', 'Y', "
					+ "'Y', 'Y', 'Y', 'Y', "
					+ "'N', 'N', null, '" + this.ocbTransp.getTipo() + "', "
					+ "null, null, null, "
					+ "null, null, null, "
					+ "lower('" + this.ocbTransp.getEmail() + "'), null, null, null, "
					+ "null, null, null, null, "
					+ "null, initcap('" + nombreComercial + "'), initcap('" + nombreComercial + "'), null, "
					+ "'PN', "
					+ "null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "'N', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage() + "; Error Insertando Transportista en Openbravo: " + this.ocbPartner.getIdentificacion());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcbGroup(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {						
			statement = connection.createStatement();
			sql = "INSERT INTO c_bp_group("
					+ "c_bp_group_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, value, name, description, isdefault)"
					+ "VALUES ("
					+ "'" + this.ocbGroup.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', '" + this.ocbGroup.getNombre() + "', '" + this.ocbGroup.getNombre() + "', '" + this.ocbGroup.getNombre() + "', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOadUser(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		String nombreComercial = "";
		if(this.ocbPartner.getNombreComercial().length() > 60){
			nombreComercial = this.ocbPartner.getNombreComercial().substring(0, 60);
		}else{
			nombreComercial = this.ocbPartner.getNombreComercial();
		}
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_user("
					+ "ad_user_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, name, description, password, email, supervisor_id, "
					+ "c_bpartner_id, processing, emailuser, emailuserpw, c_bpartner_location_id, "
					+ "c_greeting_id, title, comments, phone, phone2, fax, lastcontact, "
					+ "lastresult, birthday, ad_orgtrx_id, firstname, lastname, username, "
					+ "default_ad_client_id, default_ad_language, default_ad_org_id, "
					+ "default_ad_role_id, default_m_warehouse_id, islocked, ad_image_id, "
					+ "grant_portal_access, em_atecfe_check_email)"
					+ "VALUES ("
					+ "'" + this.oadUserEmail.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', initcap('" + nombreComercial + "'), null, null, lower('" + this.oadUserEmail.getEmail() + "'), null, "
					+ "'" + this.ocbPartner.getId() + "', 'N', null, null, null, "
					+ "null, null, null, null, null, null, null, "
					+ "null, null, null, initcap('" + nombreComercial + "'), initcap('" + nombreComercial + "'), null, "
					+ "null, null, null, "
					+ "null, null, 'N', null, "
					+ "'N', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcLocation(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		String adress = "";
		if(this.ocLocation.getDireccion().length() > 60){
			adress = this.ocLocation.getDireccion().substring(0, 60);
		}else{
			adress = this.ocLocation.getDireccion();
		}
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_location("
					+ "c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, address1, address2, city, postal, postal_add, "
					+ "c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, "
					+ "em_co_barrio) "
					+ "VALUES ("
					+ "'" + this.ocLocation.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', initcap('" + adress + "'), null, null, null, null, "
					+ "(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, "
					+ "null);";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcbPartnerLocation(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		String adress = "";
		if(this.ocLocation.getDireccion().length() > 60){
			adress = this.ocLocation.getDireccion().substring(0, 60);
		}else{
			adress = this.ocLocation.getDireccion();
		}
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_bpartner_location("
					+ "c_bpartner_location_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, name, isbillto, isshipto, ispayfrom, "
					+ "isremitto, phone, phone2, fax, c_salesregion_id, c_bpartner_id, "
					+ "c_location_id, istaxlocation, upc, em_no_num_dir) "
					+ "VALUES ("
					+ "'" + this.ocbPartnerLocation.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', initcap('" + adress + "'), 'Y', 'Y', 'Y', "
					+ "'Y', null, null, null, null, '" + this.ocbPartner.getId() + "', "
					+ "'" + this.ocLocation.getId() + "', 'N', null, null);";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcInvoice(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_invoice("
					+ "c_invoice_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, issotrx, documentno, docstatus, docaction, "
					+ "processing, processed, posted, c_doctype_id, c_doctypetarget_id, "
					+ "c_order_id, description, isprinted, salesrep_id, dateinvoiced, "
					+ "dateprinted, dateacct, c_bpartner_id, c_bpartner_location_id, "
					+ "poreference, isdiscountprinted, dateordered, c_currency_id, paymentrule, "
					+ "c_paymentterm_id, c_charge_id, chargeamt, totallines, grandtotal, "
					+ "m_pricelist_id, istaxincluded, c_campaign_id, c_project_id, c_activity_id, "
					+ "createfrom, generateto, ad_user_id, copyfrom, isselfservice, "
					+ "ad_orgtrx_id, user1_id, user2_id, withholdingamount, taxdate, "
					+ "c_withholding_id, ispaid, totalpaid, outstandingamt, daystilldue, "
					+ "dueamt, lastcalculatedondate, updatepaymentmonitor, fin_paymentmethod_id, "
					+ "fin_payment_priority_id, finalsettlement, daysoutstanding, percentageoverdue, "
					+ "c_costcenter_id, calculate_promotions, a_asset_id, iscashvat, "
					+ "em_co_nro_aut_sri, em_co_vencimiento_aut_sri, em_co_nro_estab, "
					+ "em_co_punto_emision, em_co_codsustento, em_co_rise, em_co_addpayment, "
					+ "em_ats_totalexento, em_ats_totalbase0, em_ats_totalbase12, em_ats_totaliva, "
					+ "em_ats_totalice, em_atecfe_docstatus, em_atecfe_docaction, em_atecfe_menobserror_sri, "
					+ "em_atecfe_codigo_acc, em_atecfe_documento_xml, em_atecfe_c_invoice_id, "
					+ "em_atecfe_fecha_autori, em_aprm_addpayment, em_aprm_processinvoice) "
					+ "VALUES ("
					+ "'" + this.ocInvoice.getId() + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', 'Y', to_timestamp('" + this.ocInvoice.getFechaEmision() + "','YYYY-MM-DD'), '100', "
					+ "to_timestamp('" + this.ocInvoice.getFechaEmision() + "','YYYY-MM-DD'), '100', 'Y', '" + this.ocInvoice.getNroDocumento() + "', 'DR', 'CO', "
					+ "'N', 'N', 'N', '" + this.ocDoctype.getId() + "', '" + this.ocDoctype.getId() + "', "
					+ "null, '" + this.ocInvoice.getDescripcion() + "', 'N', null, to_timestamp('" + this.ocInvoice.getFechaEmision() + "','YYYY-MM-DD'), "
					+ "null, to_timestamp('" + this.ocInvoice.getFechaContable()  + "','YYYY-MM-DD'), '" + this.ocbPartner.getId() + "', (SELECT cbl.c_bpartner_location_id FROM c_bpartner_location AS cbl WHERE cbl.c_bpartner_id = '" + this.ocbPartner.getId() + "' LIMIT 1), "
					+ "null, 'N', null, (SELECT cc.c_currency_id FROM c_currency AS cc WHERE upper(cc.iso_code) LIKE 'USD'), null, "
					+ "(SELECT cp.c_paymentterm_id FROM c_paymentterm AS cp WHERE upper(cp.name) like '%CONTADO%' LIMIT 1), null, '0', '0', '0', "
					+ "(SELECT m_pricelist_id FROM m_pricelist AS mp WHERE upper(mp.name) LIKE '%VENTA%' LIMIT 1), 'N', null, null, null, "
					+ "'N', 'N', '" + this.oadUserEmail.getId() + "', 'N', 'N', "
					+ "null, null, null, '0', null, "
					+ "null, 'N', '0', '0', '0', "
					+ "'0', null, 'N', (SELECT fin_paymentmethod_id FROM fin_paymentmethod as fp WHERE upper(fp.name) LIKE '%" + this.ocInvoice.getMetodoPago() + "%' LIMIT 1), "
					+ "null, null, null, '0', "
					+ "null, 'N', null, 'N', "
					+ "'" + this.ocInvoice.getNumeroAutorizacion() + "', to_timestamp('" + this.ocInvoice.getFechaEmision() + "','YYYY-MM-DD'), '" + this.ocInvoice.getNroEstablecimiento() + "', "
					+ "'" + this.ocInvoice.getNroEmision() + "', '06', 'N', 'Y', "
					+ "null, null, null, null, "
					+ "null, 'AP', 'PD', '" + this.ocInvoice.getEstado() + "', "
					+ "'" + this.ocInvoice.getClaveAcceso() + "', decode('" + this.ocInvoice.getFactura() + "','base64'), null, "
					+ "'" + this.ocInvoice.getFechaAutorizacion()+ "', 'Y', 'CO');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOmInout(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO m_inout("
					+ "m_inout_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, issotrx, documentno, docaction, docstatus, "
					+ "posted, processed, c_doctype_id, description, "
					+ "isprinted, movementtype, movementdate, dateacct, "
					+ "c_bpartner_id, c_bpartner_location_id, m_warehouse_id, "
					+ "deliveryrule, freightcostrule, deliveryviarule, "
					+ "priorityrule, "
					+ "islogistic, "
					+ "em_atecfe_docstatus, em_atecfe_docaction, em_atecfe_menobserror_sri, "
					+ "em_atecfe_codigo_acc, em_atecfe_documento_xml, em_atecfe_imp_pdf, "
					+ "em_atecfe_imp_xml, em_atecfe_nro_estab, em_atecfe_punto_emision, "
					+ "em_atecfe_nro_autorizacion, em_atecfe_fecha_autori, em_atecfe_c_bpartner_trans, "
					+ "em_atecfe_location_part, em_atecfe_location_dest, em_atecfe_fecha_ini, "
					+ "em_atecfe_fecha_fin, em_atecfe_placa) "
					+ "VALUES ("
					+ "'" + this.omInout.getId() + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', 'Y', to_timestamp('" + this.omInout.getFechaEmision() + "','YYYY-MM-DD'), '100', "
					+ "to_timestamp('" + this.omInout.getFechaEmision() + "','YYYY-MM-DD'), '100', 'N', '" + this.omInout.getNroDocumento() + "', 'CO', 'BR', "
					+ "'N', 'N', '" + this.ocDoctype.getId() + "', '" + this.omInout.getMotivo() + "', "
					+ "'N', 'C-', to_timestamp('" + this.omInout.getFechaEmision() + "','YYYY-MM-DD'), to_timestamp('" + this.omInout.getFechaEmision() + "','YYYY-MM-DD'), "
					+ "'" + this.ocbPartner.getId() + "', (SELECT cbl.c_bpartner_location_id FROM c_bpartner_location AS cbl WHERE cbl.c_bpartner_id = '" + this.ocbPartner.getId() + "' LIMIT 1), (SELECT m_warehouse_id FROM m_warehouse WHERE upper(name) LIKE '%BODEGA%' LIMIT 1), "
					+ "'N', 'N', 'N', "
					+ "'N', "
					+ "'N', "
					+ "'AP', 'AP', null, "
					+ "'" + this.omInout.getClaveAcceso() + "', decode('" + this.omInout.getGuia() + "','base64'), 'N', "
					+ "'N', '" + this.omInout.getEstablecimiento() + "', '" + this.omInout.getEmision() + "', "
					+ "'" + this.omInout.getNumeroAutorizacion() + "', date('" + this.omInout.getFechaAutorizacion() + "'), '" + this.ocbTransp.getId() + "', "
					+ "'" + this.ocLocationPar.getId() + "', '" + this.ocLocationDes.getId() + "', to_timestamp('" + this.omInout.getFechaIni() + "','YYYY-MM-DD'), "
					+ "to_timestamp('" + this.omInout.getFechaFin() + "','YYYY-MM-DD'), '" + this.omInout.getPlaca() + "');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcPaymentterm(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_paymentterm("
					+ "c_paymentterm_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, name, description, documentnote, "
					+ "isduefixed, netdays, fixmonthday, fixmonthoffset, isnextbusinessday, "
					+ "isdefault, value, netday, isvalid, fixmonthday2, fixmonthday3) "
					+ "VALUES ("
					+ "'" + this.ocPaymentterm.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', 'CONTADO', null, null, "
					+ "'N', '0', null, '0', 'N', "
					+ "'N', 'CONTADO', null, 'N', null, '0');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOmPricelist(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO m_pricelist("
					+ "m_pricelist_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, name, description, basepricelist_id, istaxincluded, "
					+ "issopricelist, isdefault, c_currency_id, enforcepricelimit, costbased) "
					+ "VALUES ("
					+ "'" + this.omPricelist.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', 'VENTA', null, null, 'N', "
					+ "'Y', 'N', '100', 'N', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDmProductCategory(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO m_product_category("
					+ "m_product_category_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, value, name, description, isdefault, "
					+ "plannedmargin, a_asset_group_id, ad_image_id, issummary)"
					+ "VALUES ("
					+ "'" + this.omProductCategory.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', '" + this.omProductCategory.getName() + "', '" + this.omProductCategory.getName() + "', null, 'N', "
					+ "'0', null, null, 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcTaxcategory(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_taxcategory("
					+ "c_taxcategory_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, name, description, isdefault, "
					+ "em_co_es_ice, em_co_tp_base_imponible, em_atecfe_codtax) "
					+ "VALUES ("
					+ "'" + this.ocTaxcategory.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', '" + this.ocTaxcategory.getNombre() + "', 'Impuesto al valor agregado', 'N', "
					+ "'N', '" + this.ocTaxcategory.getCodigo() + "', null);";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcTax(Connection connection) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Statement statement = null;
				String sql;
				int registro = 0;
				
				try {
					statement = connection.createStatement();
					sql = "INSERT INTO c_tax("
							+ "c_tax_id, ad_client_id, ad_org_id, isactive, created, createdby, "
							+ "updated, name, updatedby, description, taxindicator, validfrom, "
							+ "issummary, rate, parent_tax_id, c_country_id, c_region_id, to_country_id, "
							+ "to_region_id, c_taxcategory_id, isdefault, istaxexempt, sopotype, "
							+ "cascade, c_bp_taxcategory_id, line, iswithholdingtax, isnotaxable, "
							+ "deducpercent, originalrate, istaxundeductable, istaxdeductable, "
							+ "isnovat, baseamount, c_taxbase_id, doctaxamount, iscashvat, em_atecfe_tartax) "
							+ "VALUES ("
							+ "'" + this.ocTax.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
							+ "now(), '" + this.ocTaxcategory.getNombre() + "', '100', null, null, '2013-01-01', "
							+ "'N', '" + this.ocTax.getRate()  + "', null, '171', null, null, "
							+ "null, (SELECT c_taxcategory_id FROM c_taxcategory WHERE upper(name) like '" + this.ocTaxcategory.getNombre() + "' LIMIT 1), 'N', 'N', 'B', "
							+ "'N', null, '10', 'N', 'N', "
							+ "null, null, 'N', 'N', "
							+ "'N', 'LNA', null, 'D', 'N', null);";
					
					//AND em_co_tp_base_imponible = '" + this.ocTaxcategory.getCodigo() + "'
					
					registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					log.error(ex.getMessage());
				} finally {
					try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
				}
				
				if(registro > 0){
					return true;
				}else{
					return false;
				}
	}

	@Override
	public boolean saveBDDOmProduct(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO m_product("
					+ "m_product_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, value, name, description, documentnote, help, "
					+ "upc, sku, c_uom_id, salesrep_id, issummary, isstocked, ispurchased, "
					+ "issold, isbom, isinvoiceprintdetails, ispicklistprintdetails, "
					+ "isverified, m_product_category_id, classification, volume, weight, "
					+ "shelfwidth, shelfheight, shelfdepth, unitsperpallet, c_taxcategory_id, "
					+ "s_resource_id, discontinued, discontinuedby, processing, s_expensetype_id, "
					+ "producttype, imageurl, descriptionurl, guaranteedays, versionno, "
					+ "m_attributeset_id, m_attributesetinstance_id, downloadurl, m_freightcategory_id, "
					+ "m_locator_id, ad_image_id, c_bpartner_id, ispriceprinted, name2, "
					+ "costtype, coststd, stock_min, enforce_attribute, calculated, "
					+ "ma_processplan_id, production, capacity, delaymin, mrp_planner_id, "
					+ "mrp_planningmethod_id, qtymax, qtymin, qtystd, qtytype, stockmin, "
					+ "attrsetvaluetype, isquantityvariable, isdeferredrevenue, revplantype, "
					+ "periodnumber, isdeferredexpense, expplantype, periodnumber_exp, "
					+ "defaultperiod, defaultperiod_exp, bookusingpoprice, c_uom_weight_id, "
					+ "m_brand_id, isgeneric, generic_product_id, createvariants, characteristic_desc, "
					+ "updateinvariants, managevariants"
					+ ")"
					+ "VALUES ("
					+ "'" + this.omProduct.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', '" + this.omProduct.getCodigo() + "', initcap('" + this.omProduct.getName() + "'), initcap('" + this.omProduct.getName() + "'), null, null, "
					+ "null, null, (SELECT c_uom_id FROM c_uom AS cu WHERE upper(name) LIKE '%UNIT%' LIMIT 1), null, 'N', 'Y', 'Y', "
					+ "'N', 'N', 'N', 'N', "
					+ "'N', (SELECT mp.m_product_category_id FROM m_product_category AS mp WHERE upper(mp.name) LIKE '%" + this.omProductCategory.getName().toUpperCase() + "%' LIMIT 1), null, '0', '0', "
					+ "null, null, null, null, (SELECT c_taxcategory_id FROM c_taxcategory WHERE upper(name) like '" + this.ocTaxcategory.getNombre() + "' LIMIT 1), "
					+ "null, 'N', null, 'N', null, "
					+ "'I', null, null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, 'Y', null, "
					+ "null, null, null, 'N', 'N', "
					+ "null, 'N', null, null, null, "
					+ "null, null, null, null, 'N', null, "
					+ "null, 'N', 'N', null, "
					+ "null, 'N', null, null, "
					+ "null, null, 'N', null, "
					+ "null, 'N', null, 'N', null, "
					+ "'N', 'N'"
					+ ");";
			
			// AND em_co_tp_base_imponible = '" + this.ocTaxcategory.getCodigo() + "'
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcInvoiceLine(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO c_invoiceline("
					+ "c_invoiceline_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, c_invoice_id, c_orderline_id, "
					+ "m_inoutline_id, line, description, financial_invoice_line, account_id, "
					+ "m_product_id, qtyinvoiced, pricelist, priceactual, pricelimit, "
					+ "linenetamt, c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id, "
					+ "taxamt, m_attributesetinstance_id, isdescription, quantityorder, "
					+ "m_product_uom_id, c_invoice_discount_id, c_projectline_id, m_offer_id, "
					+ "pricestd, excludeforwithholding, iseditlinenetamt, taxbaseamt, "
					+ "line_gross_amount, gross_unit_price, c_bpartner_id, periodnumber, "
					+ "grosspricestd, a_asset_id, defplantype, grosspricelist, c_project_id, "
					+ "isdeferred, c_period_id, c_costcenter_id, user1_id, user2_id, "
					+ "explode, bom_parent_id, em_atecfe_descuento) "
					+ "VALUES "
					+ "('" + this.ocInvoiceLine.getId() + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', 'Y', now(), "
					+ "'100', now(), '100', '" + this.ocInvoice.getId() + "', null, "
					+ "null, '" + this.ocInvoiceLine.getLinea() + "', null, 'N', null, "
					+ "(SELECT m_product_id FROM m_product AS mp WHERE mp.value = '" + this.omProduct.getCodigo() + "'), '" + this.ocInvoiceLine.getCantidad() + "', '" + this.ocInvoiceLine.getPrecioUnitario() + "', '" +  this.ocInvoiceLine.getPrecioUnitario() + "', '0', "
					+ "'" + this.ocInvoiceLine.getPrecioTotal() + "', null, '0', (SELECT c_uom_id FROM c_uom AS cu WHERE upper(name) LIKE '%UNIT%' LIMIT 1), (SELECT ct.c_tax_id FROM c_tax AS ct WHERE upper(ct.name) LIKE '" + this.ocTaxcategory.getNombre() + "' AND ct.rate = '" + this.ocTax.getRate() + "' LIMIT 1) , null, "
					+ "'" + this.ocInvoiceLine.getValor() + "', null, 'N', null, "
					+ "null, null, null, null, "
					+ "'" + this.ocInvoiceLine.getPrecioUnitario() + "', 'N', 'N', '" + this.ocInvoiceLine.getPrecioTotal() + "', "
					+ "'0', '0', null, null, "
					+ "'0', null, null, '0', null, "
					+ "'N', null, null, null, null, "
					+ "'N', null, '" + this.ocInvoiceLine.getDescuento() + "');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOmInoutLine(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "INSERT INTO m_inoutline("
					+ "m_inoutline_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, line, description, m_inout_id, "
					+ "m_product_id, c_uom_id, movementqty, isinvoiced, "
					+ "isdescription, "
					+ "explode) "
					+ "VALUES ("
					+ "'" + this.omInoutLine.getId() + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', 'Y', NOW(), '100', "
					+ "NOW(), '100', '10', '" + this.omInoutLine.getDescripcion() + "', '" + this.omInout.getId() + "', "
					+ "(SELECT m_product_id FROM m_product AS mp WHERE mp.value = '" + this.omProduct.getCodigo() + "'), (SELECT c_uom_id FROM c_uom AS cu WHERE upper(name) LIKE '%UNIT%' LIMIT 1), '" + this.omInoutLine.getCantidad() + "', 'N', "
					+ "'N', "
					+ "'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean saveBDDOadUserPortal(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
		
		String nombreComercial = "";
		if(this.ocbPartner.getNombreComercial().length() > 60){
			nombreComercial = this.ocbPartner.getNombreComercial().substring(0, 60);
		}else{
			nombreComercial = this.ocbPartner.getNombreComercial();
		}
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_user("
					+ "ad_user_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, name, description, password, email, supervisor_id, "
					+ "c_bpartner_id, processing, emailuser, emailuserpw, c_bpartner_location_id, "
					+ "c_greeting_id, title, comments, phone, phone2, fax, lastcontact, "
					+ "lastresult, birthday, ad_orgtrx_id, firstname, lastname, username, "
					+ "default_ad_client_id, default_ad_language, default_ad_org_id, "
					+ "default_ad_role_id, default_m_warehouse_id, islocked, ad_image_id, "
					+ "grant_portal_access, em_atecfe_check_email)"
					+ "VALUES ("
					+ "'" + this.oadUserEmail.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', initcap('" + nombreComercial + "'), null, '" + auxiliares.encryptSh1(this.ocbPartner.getIdentificacion()) + "', null, null, "
					+ "'" + this.ocbPartner.getId() + "', 'N', null, null, null, "
					+ "null, null, null, null, null, null, null, "
					+ "null, null, null, initcap('" + nombreComercial + "'), initcap('" + nombreComercial + "'), '" + this.ocbPartner.getIdentificacion() + "', "
					+ "null, 'es_EC', null, "
					+ "null, null, 'N', null, "
					+ "'N', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOadRole(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_role("
					+ "ad_role_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, name, updatedby, description, userlevel, clientlist, "
					+ "orglist, c_currency_id, amtapproval, ad_tree_menu_id, ismanual, "
					+ "processing, is_client_admin, isadvanced, isrestrictbackend, isportal, "
					+ "isportaladmin) "
					+ "VALUES ("
					+ "'" + this.oadRole.getId() + "', '" + this.oadClient.getId() + "', '0', 'Y', now(), '100', "
					+ "now(), 'CONSULTOR DOCUMENTOS ELECTRÓNICOS', '100', 'CONSULTOR DOCUMENTOS ELECTRÓNICOS', 'CO', '" + this.oadClient.getId() + "', "
					+ "'0', null, '0', null, 'Y', "
					+ "'N', 'N', 'N', 'N', 'N', "
					+ "'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOadRoleOrgaccess(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_role_orgaccess("
					+ "ad_role_orgaccess_id, ad_role_id, ad_org_id, ad_client_id, isactive, "
					+ "created, createdby, updated, updatedby, is_org_admin) "
					+ "VALUES ("
					+ "'" + this.oadRoleOrgaccess.getId() + "', (SELECT ad_role_id FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%' LIMIT 1), '0', '" + this.oadClient.getId() + "', 'Y', "
					+ "now(), '100', now(), '100', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOadWindowAccess(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_window_access("
					+ "ad_window_access_id, ad_window_id, ad_role_id, ad_client_id, "
					+ "ad_org_id, isactive, created, createdby, updated, updatedby, "
					+ "isreadwrite) VALUES ("
					+ "'" + this.oadWindowAccess.getId() + "', (SELECT ad_window_id FROM ad_window WHERE name like '%Facturas y Notas de Crédito Electrónicas%' LIMIT 1), (SELECT ad_role_id FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%' LIMIT 1), '" + this.oadClient.getId() + "', "
					+ "'0', 'Y', now(), '100', now(), '100', "
					+ "'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			if(registro > 0){
				this.oadWindowAccess = new BDDOadWindowAccess();
				
				sql = "INSERT INTO ad_window_access("
						+ "ad_window_access_id, ad_window_id, ad_role_id, ad_client_id, "
						+ "ad_org_id, isactive, created, createdby, updated, updatedby, "
						+ "isreadwrite) VALUES ("
						+ "'" + this.oadWindowAccess.getId() + "', (SELECT ad_window_id FROM ad_window WHERE name like '%Retenciones Electrónicas%' LIMIT 1), (SELECT ad_role_id FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%' LIMIT 1), '" + this.oadClient.getId() + "', "
						+ "'0', 'Y', now(), '100', now(), '100', "
						+ "'N');";
				
				registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			}
			
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOadUserRoles(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO ad_user_roles("
					+ "ad_user_roles_id, ad_user_id, ad_role_id, ad_client_id, ad_org_id, "
					+ "isactive, created, createdby, updated, updatedby, is_role_admin) "
					+ "VALUES ("
					+ "'" + this.oadUserRoles.getId() + "', '" + this.oadUserEmail.getId() + "', (SELECT ad_role_id FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%' LIMIT 1), '" + this.oadClient.getId() + "', '0', "
					+ "'Y', now(), '100', now(), '100', 'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOadUser(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.oadUserEmail.setId(null);
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT ad_user_id FROM ad_user WHERE c_bpartner_id IN (SELECT c_bpartner_id FROM c_bpartner WHERE taxid = '" + this.ocbPartner.getIdentificacion() + "') AND password IS NOT NULL;";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.oadUserEmail.setId(rs.getString("ad_user_id"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.oadUserEmail.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcoRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO co_retencion_compra("
					+ "co_retencion_compra_id, ad_client_id, ad_org_id, created, createdby, "
					+ "updated, updatedby, isactive, c_invoice_id, documentno, no_autorizacion, "
					+ "c_bpartner_id, ruc_bp, c_bpartner_location_id, fecha_emision, "
					+ "tipo_comprobante_venta, no_comprobante_venta, total_retencion, "
					+ "processed, c_period_id, docstatus, c_doctype_id, dateacct, processing, "
					+ "posted, docactionre, c_doctypetarget_id, em_atecfe_docstatus, "
					+ "em_atecfe_docaction, em_atecfe_menobserror_sri, em_atecfe_codigo_acc, "
					+ "em_atecfe_documento_xml, em_atecfe_fecha_autori, em_atecfe_user_id, "
					+ "em_atecfe_imp_pdf, em_atecfe_imp_xml, em_atecfe_nro_estab, em_atecfe_punto_emision, "
					+ "em_atecfe_email_clie, em_atecfe_ad_client)"
					+ "VALUES ('" + this.ocoRetencionCompra.getId() + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', now(), '100', "
					+ "now(), '100', 'Y', null, '" + this.ocoRetencionCompra.getNroDocumento() + "', '" + this.ocoRetencionCompra.getNumeroAutorizacion() + "', "
					+ "'" + this.ocbPartner.getId() + "', '" + this.ocoRetencionCompra.getIdentificacion() + "', (SELECT cbl.c_bpartner_location_id FROM c_bpartner_location AS cbl WHERE cbl.c_bpartner_id = '" + this.ocbPartner.getId() + "' LIMIT 1), to_timestamp('" + this.ocoRetencionCompra.getFechaEmision().substring(0, 10) + "','YYYY-MM-DD'), "
					+ "'" + this.ocoRetencionCompra.getTipoDocumento() + "', '" + this.ocoRetencionCompra.getNrofactura() + "', '0', "
					+ "'N', null, 'BR', '" + this.ocDoctype.getId() + "', to_timestamp('" + this.ocoRetencionCompra.getFechaContable().substring(0, 10)  + "','YYYY-MM-DD'), 'N', "
					+ "'N', 'CO', null, 'AP', "
					+ "'PD', '" + this.ocoRetencionCompra.getEstado() + "', '" + this.ocoRetencionCompra.getClaveAcceso() + "', "
					+ "decode('" + this.ocoRetencionCompra.getRetencion() + "','base64'), '" + this.ocoRetencionCompra.getFechaAutorizacion()+ "', '" + this.oadUserEmail.getId() + "', "
					+ "'N', 'N', '" + this.ocoRetencionCompra.getNroEstablecimiento() + "', '" + this.ocoRetencionCompra.getNroEmision() +"', "
					+ "null, '" + this.oadClient.getId() + "');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOcoTipoRetencion(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.ocoTipoRetencion.setId(null);
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT co_tipo_retencion_id FROM co_tipo_retencion WHERE name = '" + this.ocoTipoRetencion.getCodigo() + "' AND tipo = '" + this.ocoTipoRetencion.getTipo() + "' AND porcentaje = " + this.ocoTipoRetencion.getPorcentaje() + ";";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocoTipoRetencion.setId(rs.getString("co_tipo_retencion_id"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocoTipoRetencion.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcoTipoRetencion(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO co_tipo_retencion("
					+ "co_tipo_retencion_id, ad_client_id, ad_org_id, created, createdby, "
					+ "updated, updatedby, name, tipo, porcentaje, isactive, tiporetencioniva, "
					+ "tiporetencionfuente) "
					+ "VALUES ("
					+ "'" + this.ocoTipoRetencion.getId() + "', '0', '0', now(), '100', "
					+ "now(), '100', '" + this.ocoTipoRetencion.getCodigo() + "', '" + this.ocoTipoRetencion.getTipo() +"', '" + this.ocoTipoRetencion.getPorcentaje() + "', 'Y', null, "
					+ "'" + this.ocoTipoRetencion.getCodigo() + "');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOcoBpRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.ocoBpRetencionCompra.setId(null);
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT co_bp_retencion_compra_id FROM co_bp_retencion_compra WHERE c_bpartner_id = '" + this.ocbPartner.getId() + "' AND co_tipo_retencion_id = '" + this.ocoTipoRetencion.getId() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocoBpRetencionCompra.setId(rs.getString("co_bp_retencion_compra_id"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocoBpRetencionCompra.getId() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcoBpRetencionCompra(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO co_bp_retencion_compra("
					+ "co_bp_retencion_compra_id, ad_client_id, ad_org_id, created, "
					+ "createdby, updated, updatedby, descripcion, c_bpartner_id, co_tipo_retencion_id, "
					+ "isactive) "
					+ "VALUES ("
					+ "'" + this.ocoBpRetencionCompra.getId() + "', '0', '0', now(), "
					+ "'100', now(), '100', '" + this.ocoBpRetencionCompra.getDescription() + "', '" + this.ocbPartner.getId() + "', '" + this.ocoTipoRetencion.getId() + "', "
					+ "'Y');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDOcoRetencionCompraLinea(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO co_retencion_compra_linea("
					+ "co_retencion_compra_linea_id, ad_client_id, ad_org_id, created, "
					+ "createdby, updated, updatedby, co_retencion_compra_id, line, "
					+ "tipo, no_cheque, base_imp_retencion, co_bp_retencion_compra_id, "
					+ "valor_retencion, isactive) "
					+ "VALUES ('" + this.ocoRetencionCompraLinea.getId() + "', '0', '0', now(), "
					+ "'100', now(), '100', '" + this.ocoRetencionCompra.getId() + "', '" + this.ocoRetencionCompraLinea.getLinea() + "', "
					+ "'" + this.ocoRetencionCompraLinea.getTipo() + "', null, '" + this.ocoRetencionCompraLinea.getBaseimponible() + "', '" + this.ocoBpRetencionCompra.getId() + "', "
					+ "'" + this.ocoRetencionCompraLinea.getValor() + "', 'Y');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean updateBDDOmProduct(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE m_product SET name = '" + this.omProduct.getName() + "', description = '" + this.omProduct.getName() + "'  WHERE value = '" + this.omProduct.getCodigo() + "';";
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean getBDDOcPocConfiguration(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		this.ocbPartner.setId(null);
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT cpc.smtpserver, "
					+ "cpc.smtpserveraccount, "
					+ "cpc.smtpserverpassword, "
					+ "cpc.issmtpauthorization, "
					+ "cpc.smtpconnectionsecurity, "
					+ "cpc.smtpport "
					+ "FROM c_poc_configuration AS cpc "
					+ "WHERE cpc.ad_client_id = '" + this.oadClient.getId() + "'";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				this.ocPocConfiguration.setSmtpserver(rs.getString("smtpserver"));
				this.ocPocConfiguration.setSmtpserveracount(rs.getString("smtpserveraccount"));
				this.ocPocConfiguration.setSmtppassword(rs.getString("smtpserverpassword"));
				this.ocPocConfiguration.setIssmtpautorization(rs.getString("issmtpauthorization").equals("Y") ? true: false);
				this.ocPocConfiguration.setSmtpsegurity(rs.getString("smtpconnectionsecurity"));
				this.ocPocConfiguration.setSmtpport(rs.getString("smtpport"));
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(this.ocPocConfiguration.getSmtpserver() != null && 
				this.ocPocConfiguration.getSmtpserveracount() != null && 
				this.ocPocConfiguration.getSmtppassword() != null && 
				this.ocPocConfiguration.getSmtpport() != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveBDDNovedad(Connection connection, String mensaje) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO atecfe_log_seguimiento("
					+ "atecfe_log_seguimiento_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, mensaje) "
					+ "VALUES (getuuid(), "
					+ "(SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1), "
					+ "(SELECT ao.ad_org_id FROM ad_org AS ao WHERE ao.em_co_nro_estab = '" + this.documentoBase.getNroestablecimiento() + "' AND ao.em_co_punto_emision = '" + this.documentoBase.getNroemision() + "' AND isactive = 'Y' AND ao.ad_client_id = (SELECT ad_client_id FROM ad_client AS ac WHERE upper(ac.name) LIKE '%" + this.confBDDOpenbravo.getEmpresa().toUpperCase() + "%' LIMIT 1) LIMIT 1), "
					+ "'Y', NOW(), '100', NOW(), '100', "
					+ "'" + mensaje + "');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public boolean saveLogServicio(Connection connection, String Mensaje) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean procesarFactura(String total, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			String idInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();
			String idProcess = "";
			ResultSet rs = null;
			
			statement = connection.createStatement();
			
		    sql = "UPDATE c_invoicetax "
		    		+ "SET taxamt = (SELECT  round(sum(taxamt),2) FROM c_invoiceline  WHERE c_invoice_id = '" + this.ocInvoice.getId() +"') "
		    		+ "WHERE c_invoice_id = '" + this.ocInvoice.getId() +"';";
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "SELECT round(sum(taxbaseamt) + sum(taxamt),2) as total "
					+ "FROM c_invoiceline "
					+ "WHERE c_invoice_id = '" + this.ocInvoice.getId() +"'";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			String auxamount = total;
			while (rs.next()){
				total = rs.getString("total");
			}
			
			if(!total.equals("0.00")){
				sql = "SELECT ad_process_id "
						+ "FROM ad_process "
						+ "WHERE upper(procedurename) = upper('c_invoice_post0') AND upper(name) = upper('Process Invoice');"; 
				rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
				
				while (rs.next()){
					idProcess = rs.getString("ad_process_id");
				}

				rs.close();  log.info("Close BDD o Statement");
				
				sql = "INSERT INTO ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
						+ "ad_user_id, updated, result, errormsg, ad_client_id, "
						+ "ad_org_id, createdby, updatedby, isactive) "
						+ "VALUES ('" + idInstance + "', '" + idProcess + "', '" + this.ocInvoice.getId() +"', 'N', now(), "
						+ "'0', now(),'0','', (SELECT ad_client_id FROM c_invoice as ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "' LIMIT 1), "
						+ "(SELECT ad_org_id FROM c_invoice as ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "' LIMIT 1), '0', '0', 'Y');";
				
				registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
				
				if(registro > 0){
					sql = "select c_invoice_post('" + idInstance + "','" + this.ocInvoice.getId() + "') as dato1 from dual d where d.dummy <> '" + this.ocInvoice.getId() + "'";
				    statement.executeQuery(sql);log.info("Consultando: " + sql);
				    
				    rs = null;
				    registro = 0;
				    sql = "SELECT round(amount,2) AS amount, count(*) AS total FROM fin_payment_schedule WHERE c_invoice_id = '" + this.ocInvoice.getId() + "' GROUP BY amount;";
				    rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
				    
				    String aux2amount = "0";
				    while (rs.next()){
				    	registro = Integer.valueOf(rs.getString("total"));
				    	aux2amount = rs.getString("amount");
					}
				    
				    rs.close();  log.info("Close BDD o Statement");
				    rs = null;
				    
				    if(registro > 0){
				    	Float auxtotal = Float.valueOf(auxamount);
					    Float aux2total = Float.valueOf(aux2amount);
					    Float auxmonto = (float)0;
					    
					    if(aux2total < 0){
					    	auxmonto = auxtotal * -1;
					    	aux2total = Math.abs(aux2total);
					    }else{
					    	auxmonto = auxtotal;
					    }
					    
					    Float aux2totalmas = ((float) (aux2total + 0.01)); 
					    Float aux2totalmenos = ((float) (aux2total - 0.01)); 
					    
					    Float aux22totalmas = ((float) (aux2total + 0.02)); 
					    Float aux22totalmenos = ((float) (aux2total - 0.02)); 
					    
					    aux2totalmas = (float) (Math.round(aux2totalmas*100.0)/100.0);
					    aux2totalmenos = (float) (Math.round(aux2totalmenos*100.0)/100.0);
					    
					    aux22totalmas = (float) (Math.round(aux22totalmas*100.0)/100.0);
					    aux22totalmenos = (float) (Math.round(aux22totalmenos*100.0)/100.0);
					    
					    if(aux2totalmas.compareTo(auxtotal) == 0 || aux2totalmenos.compareTo(auxtotal) == 0 || 
					    		aux22totalmas.compareTo(auxtotal) == 0 || aux22totalmenos.compareTo(auxtotal) == 0){
					    	sql = "UPDATE c_invoice SET docaction = 'BR', processed = 'N' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'"; 
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	rs = null;
					    	sql = "SELECT c_invoicetax_id FROM c_invoicetax WHERE c_invoice_id = '" + this.ocInvoice.getId() + "' LIMIT 1;";
						    rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
						    String auxctaxid = null;
						    
						    while(rs.next()){
						    	auxctaxid = rs.getString("c_invoicetax_id");
						    }
						    
						    rs.close();  log.info("Close BDD o Statement");
					    	
					    	if(aux2totalmas.compareTo(auxtotal) == 0 && auxctaxid != null){
					    		sql = "UPDATE c_invoicetax SET taxamt = taxamt + 0.01 WHERE c_invoicetax_id = '" + auxctaxid + "';"; 
						    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	}else if(aux2totalmenos.compareTo(auxtotal) == 0 && auxctaxid != null){
					    		sql = "UPDATE c_invoicetax SET taxamt = taxamt - 0.01 WHERE c_invoicetax_id = '" + auxctaxid + "';"; 
						    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	}else if(aux22totalmas.compareTo(auxtotal) == 0 && auxctaxid != null){
					    		sql = "UPDATE c_invoicetax SET taxamt = taxamt + 0.02 WHERE c_invoicetax_id = '" + auxctaxid + "';"; 
						    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	}else if(aux22totalmenos.compareTo(auxtotal) == 0 && auxctaxid != null){
					    		sql = "UPDATE c_invoicetax SET taxamt = taxamt - 0.02 WHERE c_invoicetax_id = '" + auxctaxid + "';"; 
						    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	}
					    	
					    	sql = "UPDATE c_invoice SET grandtotal = '" + Math.abs(auxmonto) + "' , outstandingamt = '" + Math.abs(auxmonto) + "' , dueamt = '" + Math.abs(auxmonto) + "' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'"; 
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	sql = "UPDATE fin_payment_schedule SET amount = '" + auxmonto.toString() + "', outstandingamt = '" + auxmonto.toString() + "', paidamt = 0.00 WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'";
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	sql = "UPDATE c_invoice SET docaction = 'RE', processed = 'Y' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'";
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    }else if(!(auxtotal.compareTo(aux2total) == 0)){
					    	sql = "UPDATE c_invoice SET docaction = 'BR', processed = 'N' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'"; 
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	sql = "UPDATE c_invoice SET grandtotal = '" + Math.abs(auxmonto) + "' , outstandingamt = '" + Math.abs(auxmonto) + "' , dueamt = '" + Math.abs(auxmonto) + "' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'"; 
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	sql = "UPDATE fin_payment_schedule SET amount = '" + auxmonto.toString() + "', outstandingamt = '" + auxmonto.toString() + "', paidamt = 0.00 WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'";
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    	
					    	sql = "UPDATE c_invoice SET docaction = 'RE', processed = 'Y' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "'";
					    	statement.executeUpdate(sql);log.info("Actualizando: " + sql);
					    }
				    }else{
					    sql = "SELECT errormsg FROM ad_pinstance WHERE ad_pinstance_id = '" + idInstance + "';";
					    rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
					    String auxmens = "";
					    
					    while (rs.next()){
					    	auxmens = rs.getString("errormsg");
						}
					    
					    log.warn(auxmens);
					    
					    rs.close();  log.info("Close BDD o Statement");
					    rs = null;
				    }
				}
			}else{				
				if(total.equals("0.00")){
					registro = 1;
				}
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public String buscarPaymentSchedule(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		String idPaymentShedule = null;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			sql = "SELECT fin_payment_schedule_id FROM fin_payment_schedule WHERE c_invoice_id = '" + this.ocInvoice.getId() + "';";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				idPaymentShedule = rs.getString("fin_payment_schedule_id");
			}
			
			rs.close();  log.info("Close BDD o Statement");
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		return idPaymentShedule;
	}
	
	@Override
	public boolean iniciarPago(String idpaymentSchedule, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE fin_payment_scheduledetail SET amount = 0 WHERE fin_payment_detail_id is null AND fin_payment_schedule_invoice = '" + idpaymentSchedule + "';";
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean savePago(String idpayment, String metodoPago, String amount, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "INSERT INTO fin_payment("
					+ "fin_payment_id, ad_client_id, ad_org_id, created, createdby, "
					+ "updated, updatedby, isactive, isreceipt, c_bpartner_id, paymentdate, "
					+ "c_currency_id, amount, writeoffamt, fin_paymentmethod_id, documentno, "
					+ "referenceno, status, processed, processing, posted, description, "
					+ "fin_financial_account_id, c_doctype_id, c_project_id, c_campaign_id, "
					+ "c_activity_id, user1_id, user2_id, generated_credit, used_credit, "
					+ "createdbyalgorithm, finacc_txn_convert_rate, finacc_txn_amount, "
					+ "fin_rev_payment_id, c_costcenter_id, em_atecdp_deposito, em_co_nro_cheque, "
					+ "em_co_nombre_banco, em_aprm_process_payment, "
					+ "em_aprm_reconcile_payment, em_aprm_add_scheduledpayments, em_aprm_executepayment, "
					+ "em_aprm_reversepayment)"
					+ "VALUES ("
					+ "'" + idpayment + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', now(), '100', "
					+ "now(), '100', 'Y', 'Y', (SELECT ci.c_bpartner_id FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), "
					+ "'100', '" + amount + "', '0', (SELECT fin_paymentmethod_id FROM fin_paymentmethod as fp WHERE upper(fp.name) LIKE '%" + metodoPago + "%' AND fp.isactive = 'Y' AND fp.ad_client_id = '" + this.oadClient.getId() + "' LIMIT 1), (SELECT ci.documentno FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), "
					+ "null, 'RPR', 'N', 'N', 'N', 'Factura N� : ' || (SELECT ci.documentno FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), "
					+ "(SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%BANCO%' AND ffa.ad_client_id = '" + this.oadClient.getId() + "' LIMIT 1), (SELECT c_doctype_id FROM c_doctype AS cd WHERE upper(cd.name) LIKE '%COMPROBANTE DE INGRESO%' AND cd.ad_client_id = '" + this.oadClient.getId() + "' LIMIT 1), null, null, "
					+ "null, null, null, '0', '0', "
					+ "'N', '1', '" + amount + "', "
					+ "null, null, null, null, "
					+ "null, 'RE', "
					+ "'N', 'N', 'N', "
					+ "'N');";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "UPDATE fin_payment_schedule SET fin_paymentmethod_id = (SELECT fin_paymentmethod_id FROM fin_paymentmethod as fp WHERE upper(fp.name) LIKE '%" + metodoPago + "%' AND fp.isactive = 'Y' AND fp.ad_client_id = '" + this.oadClient.getId() + "' LIMIT 1) WHERE c_invoice_id = '" + this.ocInvoice.getId() + "';";

			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "UPDATE c_invoice SET fin_paymentmethod_id = (SELECT fin_paymentmethod_id FROM fin_paymentmethod as fp WHERE upper(fp.name) LIKE '%" + metodoPago + "%' AND fp.isactive = 'Y' AND fp.ad_client_id = '" + this.oadClient.getId() + "' LIMIT 1) WHERE c_invoice_id = '" + this.ocInvoice.getId() + "';";
			
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean savePagolinea(String idpayment, String idpaymentline, String amount, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "INSERT INTO fin_payment_detail("
					+ "fin_payment_detail_id, ad_client_id, ad_org_id, created, createdby, "
					+ "updated, updatedby, fin_payment_id, amount, refund, isactive, "
					+ "writeoffamt, c_glitem_id, isprepayment, em_no_pago_line_id) "
					+ "VALUES ("
					+ "'" + idpaymentline + "', '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', now(), '100', "
					+ "now(), '100', '" + idpayment + "', '" + amount + "', 'N', 'Y', "
					+ "'0', null, 'N', null);";
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "UPDATE fin_payment SET amount = (SELECT sum(amount) FROM fin_payment_detail WHERE fin_payment_id = '" + idpayment + "'), finacc_txn_amount = (SELECT sum(amount) FROM fin_payment_detail WHERE fin_payment_id = '" + idpayment + "') WHERE fin_payment_id = '" + idpayment + "'";
			
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean savePaymentScheduledetail(String idpaymentSchedule, String idpaymentline, String amount, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		ResultSet rs;
		boolean ingresarLinea = false;
		
		try {
			statement = connection.createStatement();
			
			sql = "SELECT fin_payment_detail_id is not null AS validacion FROM fin_payment_scheduledetail WHERE fin_payment_schedule_invoice = '" + idpaymentSchedule + "'";
			rs = statement.executeQuery(sql);log.info("Consultando: " + sql);
			
			while (rs.next()){
				if(rs.getBoolean("validacion")){
					ingresarLinea = true;
				}
			}
			
			if(ingresarLinea){
				sql = "INSERT INTO fin_payment_scheduledetail(" 
						+ "fin_payment_scheduledetail_id, ad_client_id, ad_org_id, created, " 
						+ "createdby, updated, updatedby, fin_payment_detail_id, fin_payment_schedule_order, "
						+ "fin_payment_schedule_invoice, amount, isactive, writeoffamt, "
						+ "iscanceled, c_bpartner_id, c_activity_id, m_product_id, c_campaign_id, "
						+ "c_project_id, c_salesregion_id, c_costcenter_id, user1_id, user2_id, "
						+ "doubtfuldebt_amount) "
						+ "VALUES ("
						+ "get_uuid(), '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', now(), "
						+ "'100', now(), '100', '" + idpaymentline + "', null, "
						+ "'" + idpaymentSchedule + "', '" + amount + "', 'Y', '0', "
						+ "'N', null, null, null, null, "
						+ "null, null, null, null, null, "
						+ "'0');";
			}else{
				sql = "UPDATE fin_payment_scheduledetail SET amount = '" + amount + "' , fin_payment_detail_id = '" + idpaymentline + "' WHERE fin_payment_schedule_invoice = '" + idpaymentSchedule + "'";
			}
			
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			sql = "UPDATE fin_payment_schedule SET paidamt = (SELECT sum(amount) FROM fin_payment_scheduledetail as fps WHERE fps.fin_payment_schedule_invoice = '" + idpaymentSchedule + "'), outstandingamt = amount - (SELECT sum(amount) FROM fin_payment_scheduledetail as fps WHERE fps.fin_payment_schedule_invoice = '" + idpaymentSchedule + "') WHERE fin_payment_schedule_id = '" + idpaymentSchedule + "';";
			
			statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
		    try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean procesarPago(String idpayment, boolean dato, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "UPDATE fin_payment SET status = 'RDNC', processed = 'Y', em_aprm_process_payment = 'RE' WHERE fin_payment_id = '" + idpayment + "';";
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			
			if(registro > 0 && dato){
				sql = "INSERT INTO fin_finacc_transaction("
						+ "fin_finacc_transaction_id, ad_client_id, ad_org_id, created, "
						+ "createdby, updated, updatedby, isactive, c_currency_id, fin_financial_account_id, "
						+ "line, fin_payment_id, dateacct, c_glitem_id, status, paymentamt, "
						+ "depositamt, processed, processing, posted, c_project_id, c_campaign_id, "
						+ "c_activity_id, user1_id, user2_id, trxtype, statementdate, description, "
						+ "fin_reconciliation_id, createdbyalgorithm, foreign_currency_id, "
						+ "foreign_convert_rate, foreign_amount, c_bpartner_id, m_product_id, "
						+ "c_salesregion_id, c_costcenter_id, em_aprm_delete, em_aprm_modify) "
						+ "VALUES (get_uuid(), '" + this.oadClient.getId() + "', '" + this.oadOrg.getId() + "', now(), "
						+ "'100', now(), '100', 'Y', '100', (SELECT fin_financial_account_id "
						+ "FROM fin_financial_account AS ffa "
						+ "WHERE upper(ffa.name) LIKE '%BANCO%' LIMIT 1), "
						+ "(SELECT COALESCE((max(line) + 10),10) "
						+ "FROM fin_finacc_transaction "
						+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id "
						+ "FROM fin_financial_account AS ffa "
						+ "WHERE upper(ffa.name) LIKE '%BANCO%' LIMIT 1)), '" + idpayment + "', (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), null, 'RDNC', 0, "
						+ "(SELECT amount FROM fin_payment WHERE fin_payment_id = '" + idpayment + "'), 'Y', 'N', 'N', null, null, "
						+ "null, null, null, null, (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.ocInvoice.getId() + "'), (SELECT description FROM fin_payment WHERE fin_payment_id = '" + idpayment + "'), "
						+ "null, 'N', null, "
						+ "null, null, (SELECT c_bpartner_id FROM fin_payment WHERE fin_payment_id = '" + idpayment + "'), null, "
						+ "null, null, 'N', 'N');";
				statement.executeUpdate(sql);log.info("Actualizando: " + sql);
				
				sql = "UPDATE fin_financial_account SET currentbalance = (SELECT sum(depositamt) - sum(paymentamt) "
						+ "FROM fin_finacc_transaction "
						+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%BANCO%' LIMIT 1)) " 
						+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%BANCO%' LIMIT 1);";
				statement.executeUpdate(sql);log.info("Actualizando: " + sql);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			registro = 0;
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean terminarFactura(String ispaid, String amountTotal, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE c_invoice SET ispaid = '" + ispaid + "', totalpaid = round('" + amountTotal + "',2), outstandingamt = round(grandtotal - round('" + amountTotal + "',2),2) , dueamt = round(grandtotal - round('" + amountTotal + "',2),2), finalsettlement = now(), daysoutstanding = '12', percentageoverdue = '100' WHERE c_invoice_id = '" + this.ocInvoice.getId() + "';";
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public boolean terminarRetencion(Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE co_retencion_compra SET docstatus = 'CO', docactionre = 'RE' WHERE co_retencion_compra_id = '" + this.ocoRetencionCompra.getId() + "';";
			registro = statement.executeUpdate(sql);log.info("Actualizando: " + sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close();  log.info("Close BDD o Statement"); } catch (Exception e) {};
		}
		
		if(registro > 0){
			return true;
		}else{
			return false;
		}
	}
}
