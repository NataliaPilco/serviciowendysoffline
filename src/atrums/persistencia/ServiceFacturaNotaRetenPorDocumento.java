package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServDocumentosLineas;

public class ServiceFacturaNotaRetenPorDocumento {
	static final Logger log = Logger.getLogger(ServiceFacturaPorFecha.class);
	private final ConfService confService;
	private String nroDocumento;
	private String nroEstablecimiento;
	private String nroEmision;
	private String tipoDoc;
    private String fecha;
	
    public ServiceFacturaNotaRetenPorDocumento(ConfService confService, 
    		String nroDocumento, 
    		String nroEstablecimiento, 
    		String nroEmision, 
    		String tipoDoc){
        this.confService = confService;
        this.nroDocumento = nroDocumento;
        this.nroEstablecimiento = nroEstablecimiento;
        this.nroEmision = nroEmision;
        this.tipoDoc = tipoDoc;
    }
    
    public List<ServDocumentoLinea> callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("documento", this.nroDocumento);
    	auxParametros.put("establecimiento", this.nroEstablecimiento);
    	auxParametros.put("pto_emision", this.nroEmision);
    	auxParametros.put("tipo", this.tipoDoc);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(this.confService.getwSDL(), 
                auxParametros, 
                (this.tipoDoc.equals("FV") ? this.confService.getConsultarFacturaPorDocumento() : "") +
                (this.tipoDoc.equals("FC") ? this.confService.getConsultarFacturaPorDocumento() : "") + 
                (this.tipoDoc.equals("NC") ? this.confService.getConsultarNotaCreditoPorDocumento() : "") + 
                (this.tipoDoc.equals("RT") ? this.confService.getConsultarRetencionPorDocumento() : "") + 
                (this.tipoDoc.equals("GR") ? this.confService.getConsultarGuiaRemisionPorDocumento() : ""));
        
        ServDocumentosLineas documentosLineas = new ServDocumentosLineas(this.fecha ,swendysCall.Call());
        
        return documentosLineas.getLineas();
    }

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
