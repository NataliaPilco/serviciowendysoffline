package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServClaves;

public class ServiceClavePorFechaPagina {
	static final Logger log = Logger.getLogger(ServiceClavePorFechaPagina.class);
	private final ConfService confService;
	private String fecha;
	private String pagina;
    
    public ServiceClavePorFechaPagina(ConfService confService, String fecha, String pagina){
        this.confService = confService;
        this.fecha = fecha;
        this.pagina = pagina;
    }
    
    public List<String> callClaves(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	auxParametros.put("numPagina", this.pagina);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenClavesPaginadasPorFecha());
        
        ServClaves claves = new ServClaves(swendysCall.Call());
        
        swendysCall = null;
        return claves.getClaves();
    }
}
