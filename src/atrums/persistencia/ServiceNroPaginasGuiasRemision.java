package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServTotalNroPaginasGuiasRemision;

public class ServiceNroPaginasGuiasRemision {
	static final Logger log = Logger.getLogger(ServiceNroPaginasGuiasRemision.class);
    private final ConfService confService;
    private String fecha;
    
    public ServiceNroPaginasGuiasRemision(ConfService confService, String fecha) {
    	this.confService = confService;
        this.fecha = fecha;
	}
    
    public int callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenTotalGuiaRemisionPorFecha());
        
        ServTotalNroPaginasGuiasRemision nroPaginasGuiasRemision = new ServTotalNroPaginasGuiasRemision(swendysCall.Call());
        
        return nroPaginasGuiasRemision.getNroPaginas();
    }
}
