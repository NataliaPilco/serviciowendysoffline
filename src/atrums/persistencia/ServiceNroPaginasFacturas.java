package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServTotalNroPaginasFacturas;

public class ServiceNroPaginasFacturas {
	static final Logger log = Logger.getLogger(ServiceNroPaginasFacturas.class);
    private final ConfService confService;
    private String fecha;
    
    public ServiceNroPaginasFacturas(ConfService confService, String fecha) {
    	this.confService = confService;
        this.fecha = fecha;
	}
    
    public int callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenTotalFacturaPorFecha());
        
        ServTotalNroPaginasFacturas nroPaginasFacturas = new ServTotalNroPaginasFacturas(swendysCall.Call());
        
        return nroPaginasFacturas.getNroPaginas();
    }
}
