package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServTotalNroPaginasNotas;

public class ServiceNroPaginasNotas {
	static final Logger log = Logger.getLogger(ServiceNroPaginasFacturas.class);
    private final ConfService confService;
    private String fecha;
    
    public ServiceNroPaginasNotas(ConfService confService, String fecha) {
    	this.confService = confService;
        this.fecha = fecha;
	}
    
    public int callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenTotalNotaCreditoPorFecha());
        
        ServTotalNroPaginasNotas nroPaginasNotas = new ServTotalNroPaginasNotas(swendysCall.Call());
        
        return nroPaginasNotas.getNroPaginas();
    }
}
